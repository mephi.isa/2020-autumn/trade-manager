package ru.trade_manager.companies;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class CompanyTest {

    private static final String NAME = "name";
    private static final String PHONE = "phone";
    private static final String ADDRESS = "address";
    private static final String EMAIL = "email";
    private static final String INN = "inn";
    HashSet<Company.Department> departments = new HashSet<>();
    LegalEntity legalEntity = new LegalEntity(1, "name", "phone", "email", "address", "inn", LegalEntityRole.CUSTOMER);
    PriceList priceList = new PriceList(1, "name", PriceListType.RETAIL);
    Company company;
    Company.Department department;
    Company.Department newDepartment;

    @Before
    public void init() {
        company = new Company(1, NAME, PHONE, ADDRESS, EMAIL, INN, CompanyType.INDIVIDUAL_ENTREPRENEUR);
        department = company.createDepartment("name", "address");
        newDepartment = company.createDepartment("newName", "address");
        departments.add(department);
        company.addDepartment(department);
    }

    @Test
    public void constructor() {
        Company company = new Company(1, NAME, PHONE, ADDRESS, EMAIL, INN, CompanyType.INDIVIDUAL_ENTREPRENEUR);
        assertTrue(company instanceof Company);
    }

    @Test
    public void getName() {
        assertEquals(NAME, company.getName());
    }

    @Test
    public void getPhone() {
        assertEquals(PHONE, company.getPhone());
    }

    @Test
    public void getAddress() {
        assertEquals(ADDRESS, company.getAddress());
    }

    @Test
    public void getEmail() {
        assertEquals(EMAIL, company.getEmail());
    }

    @Test
    public void setEmail() {
        String newEmail = "newEmail";
        company.setEmail(newEmail);
        assertEquals(newEmail, company.getEmail());
    }

    @Test
    public void getInn() {
        assertEquals(INN, company.getInn());
    }

    @Test
    public void getCompanyType() {
        assertEquals(CompanyType.INDIVIDUAL_ENTREPRENEUR, company.getCompanyType());
    }

    @Test
    public void getDepartments() {
        assertEquals(departments, company.getDepartments());
    }

    @Test
    public void addDepartment() {
        company.addDepartment(newDepartment);
        assertEquals(2, company.getDepartments().size());
    }

    @Test
    public void deleteDepartment() {
        company.addDepartment(newDepartment);
        company.deleteDepartment(newDepartment);
        assertEquals(1, company.getDepartments().size());
    }

    @Test
    public void getLegalEntities() {
        assertTrue(company.getLegalEntities().isEmpty());
    }

    @Test
    public void addLegalEntity() {
        company.addLegalEntity(legalEntity);
        assertEquals(1, company.getLegalEntities().size());
    }

    @Test
    public void deleteLegalEntity() {
        company.addLegalEntity(legalEntity);
        company.deleteLegalEntity(legalEntity);
        assertTrue(company.getLegalEntities().isEmpty());
    }

    @Test
    public void getPriceLists() {
        assertTrue(company.getPriceLists().isEmpty());
    }

    @Test
    public void addPriceList() {
        company.addPriceList(priceList);
        assertEquals(1, company.getPriceLists().size());
    }

    @Test
    public void deletePriceList() {
        company.addPriceList(priceList);
        company.deletePriceList(priceList);
        assertTrue(company.getPriceLists().isEmpty());
    }

    @Test
    public void testPositiveEquals() {
        Company newCompany = new Company(1, NAME, PHONE, ADDRESS, EMAIL, INN, CompanyType.INDIVIDUAL_ENTREPRENEUR);
        assertEquals(company, newCompany);
    }

    @Test
    public void testNegativeEquals() {
        Company newCompany = new Company(1,"newCompany", "phone", "address", "email", "inn", CompanyType.INDIVIDUAL_ENTREPRENEUR);
        assertNotEquals(company, newCompany);
    }

    @Test
    public void testHashCode() {
        assertEquals(161278013, company.hashCode());
    }
}