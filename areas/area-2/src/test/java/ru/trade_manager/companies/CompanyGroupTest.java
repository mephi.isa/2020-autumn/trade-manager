package ru.trade_manager.companies;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CompanyGroupTest {

    private static final String NAME = "name";
    CompanyGroup companyGroup = new CompanyGroup(1, NAME);
    HashSet<Company> companies = new HashSet<>();
    Company company = new Company(1,"company", "phone", "address", "email", "inn", CompanyType.INDIVIDUAL_ENTREPRENEUR);
    Company newCompany = new Company(2, "newCompany", "phone", "address", "email", "inn", CompanyType.INDIVIDUAL_ENTREPRENEUR);

    @Before
    public void init() {
        companies.add(company);
        companyGroup = new CompanyGroup(1, NAME, companies);
    }

    @Test
    public void getName() {
        assertEquals(NAME, companyGroup.getName());
    }

    @Test
    public void getCompanies() {
        assertEquals(companies, companyGroup.getCompanies());
    }

    @Test
    public void setCompanies() {
        HashSet<Company> newCompanies = new HashSet<>();
        newCompanies.add(newCompany);
        companyGroup.setCompanies(newCompanies);
        assertEquals(newCompanies, companyGroup.getCompanies());
    }

    @Test
    public void addCompany() {
        companyGroup.addCompany(newCompany);
        assertEquals(2, companyGroup.getCompanies().size());
    }

    @Test
    public void deleteCompany() {
        companyGroup.addCompany(newCompany);
        companyGroup.deleteCompany(newCompany);
        assertEquals(1, companyGroup.getCompanies().size());
    }

    @Test
    public void clearCompanies() {
        companyGroup.addCompany(newCompany);
        companyGroup.clearCompanies();
        assertTrue(companyGroup.getCompanies().isEmpty());
    }
}