package ru.trade_manager.products;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class ProductCategoryTest {

    private static final String NAME = "name";
    ProductCategory productCategory;
    Product product = new Product(1, "product", 1, "barcode");
    HashSet<Product> products;

    @Before
    public void init() {
        products = new HashSet<>();
        products.add(product);
        productCategory = new ProductCategory(1, NAME, products);
    }

    @Test
    public void getName() {
        assertEquals(NAME, productCategory.getName());
    }

    @Test
    public void getProducts() {
        assertEquals(products, productCategory.getProducts());
    }

    @Test
    public void setProducts() {
        Product newProduct = new Product(1, "newProduct", 1, "barcode");
        HashSet<Product> newProducts = new HashSet<>();
        newProducts.add(newProduct);
        productCategory.setProducts(newProducts);
        assertEquals(newProducts, productCategory.getProducts());
    }

    @Test
    public void addProduct() {
        Product newProduct = new Product(1, "newProduct", 1, "barcode");
        productCategory.addProduct(newProduct);
        assertEquals(2, productCategory.getProducts().size());
    }

    @Test
    public void deleteProduct() {
        Product newProduct = new Product(1, "newProduct", 1, "barcode");
        productCategory.addProduct(newProduct);
        productCategory.deleteProduct(newProduct);
        assertEquals(1, productCategory.getProducts().size());
    }

    @Test
    public void clearProducts() {
        productCategory.clearProducts();
        assertTrue(productCategory.getProducts().isEmpty());
    }

    @Test
    public void testPositiveEquals() {
        ProductCategory newProductCategory = new ProductCategory(1, NAME);
        assertEquals(newProductCategory, productCategory);
    }

    @Test
    public void testNegativeEquals() {
        String newName = "newName";
        ProductCategory newProductCategory = new ProductCategory(1, newName);
        assertNotEquals(newProductCategory, productCategory);
    }

    @Test
    public void testHashCode() {
        assertEquals(3373738, productCategory.hashCode());
    }
}