package ru.trade_manager.products;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class ProductCatalogTest {

    private static final String NAME = "name";
    ProductCatalog productCatalog;
    ProductCategory productCategory;
    HashSet<ProductCategory> categories = new HashSet<>();

    @Before
    public void init() {
        productCategory = new ProductCategory(1, "category");
        categories.add(productCategory);
        productCatalog = new ProductCatalog(1, NAME, categories);
    }

    @Test
    public void getName() {
        assertEquals(NAME, productCatalog.getName());
    }

    @Test
    public void getCategories() {
        assertEquals(categories, productCatalog.getCategories());
    }

    @Test
    public void setCategories() {
        ProductCategory newProductCategory = new ProductCategory(1, "newCategory");
        categories.add(newProductCategory);
        productCatalog.setCategories(categories);
        assertEquals(categories, productCatalog.getCategories());
    }

    @Test
    public void addProductCategory() {
        ProductCategory newProductCategory = new ProductCategory(1, "newCategory");
        productCatalog.addProductCategory(newProductCategory);
        assertEquals(2, productCatalog.getCategories().size());
    }

    @Test
    public void deleteProductCategory() {
        ProductCategory newProductCategory = new ProductCategory(1, "newCategory");
        productCatalog.addProductCategory(newProductCategory);
        productCatalog.deleteProductCategory(newProductCategory);
        assertEquals(1, productCatalog.getCategories().size());
    }

    @Test
    public void clearCategories() {
        productCatalog.clearCategories();
        assertTrue(productCatalog.getCategories().isEmpty());
    }

    @Test
    public void testPositiveEquals() {
        ProductCatalog newProductCatalog = new ProductCatalog(1, NAME);
        assertEquals(newProductCatalog, productCatalog);
    }

    @Test
    public void testNegativeEquals() {
        String newName = "newName";
        ProductCatalog newProductCatalog = new ProductCatalog(1, newName);
        assertNotEquals(newProductCatalog, productCatalog);
    }

    @Test
    public void testHashCode() {
        assertEquals(3373738, productCatalog.hashCode());
    }
}