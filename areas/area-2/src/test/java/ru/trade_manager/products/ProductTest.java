package ru.trade_manager.products;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ProductTest {

    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final int PRODUCT_CODE = 1;
    private static final String BARCODE = "barcode";
    Product product;

    @Before
    public void init() {
        product = new Product(1, NAME, DESCRIPTION, PRODUCT_CODE, BARCODE);
    }

    @Test
    public void getName() {
        assertEquals(NAME, product.getName());
    }

    @Test
    public void setName() {
        String newName = "newName";
        product.setName(newName);
        assertEquals(newName, product.getName());
    }

    @Test
    public void getDescription() {
        assertEquals(DESCRIPTION, product.getDescription());
    }

    @Test
    public void setDescription() {
        String newDescription = "newDescription";
        product.setDescription(newDescription);
        assertEquals(newDescription, product.getDescription());
    }

    @Test
    public void getProductCode() {
        assertEquals(PRODUCT_CODE, product.getProductCode());
    }

    @Test
    public void setProductCode() {
        int newProductCode = 2;
        product.setProductCode(newProductCode);
        assertEquals(newProductCode, product.getProductCode());
    }

    @Test
    public void getBarcode() {
        assertEquals(BARCODE, product.getBarcode());
    }

    @Test
    public void setBarcode() {
        String newBarcode = "newBarcode";
        product.setBarcode(newBarcode);
        assertEquals(newBarcode, product.getBarcode());
    }

    @Test
    public void positiveEquals() {
        Product newProduct = new Product(1, NAME, PRODUCT_CODE, BARCODE);
        assertEquals(newProduct, product);
    }

    @Test
    public void negativeEquals() {
        String newName = "newName";
        Product newProduct = new Product(1, newName, PRODUCT_CODE, BARCODE);
        assertNotEquals(newProduct, product);
    }

    @Test
    public void testHashCode() {
        assertEquals(-1386389303, product.hashCode());
    }
}