package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductData;
import ru.trade_manager.UseCases.CreateProduct;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.products.Product;
import ru.trade_manager.user.UserRole;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CreateProductTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private CreateProduct usecase;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new CreateProduct(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulCreate() {
        ProductData productData = new ProductData("Beer", "Baltika", 1227, "some@email.me");
        int id = this.usecase.run(1, productData);
        Product product = this.storage.getProduct(id);

        assertEquals(product.getBarcode(), productData.barcode);
        assertEquals(product.getDescription(), productData.description);
        assertEquals(product.getProductCode(), productData.productCode);
        assertEquals(product.getName(), productData.name);
    }

    @Test
    public void testDuplicateProductCode() {
        ProductData ProductData = new ProductData("Meat", "New", 27, "some@email.me");
        assertThrows(RuntimeException.class, () -> this.usecase.run(1, ProductData));
    }

    @Test
    public void testWrongRole() {
        ProductData productData = new ProductData("Beer", "Baltika", 27, "some@email.me");
        assertThrows(RuntimeException.class, () -> this.usecase.run(2, productData),
                "Wrong role");
    }
}
