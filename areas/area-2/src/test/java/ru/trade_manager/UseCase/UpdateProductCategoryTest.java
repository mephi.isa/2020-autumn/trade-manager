package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductCategoryData;
import ru.trade_manager.UseCases.UpdateProductCategory;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.products.ProductCategory;
import ru.trade_manager.user.UserRole;

import java.util.AbstractMap;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UpdateProductCategoryTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private UpdateProductCategory usecase;
    int productID = 7;
    int productCategoryID = 5;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new UpdateProductCategory(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulUpdate() {
        ProductCategoryData productCategoryData = this.usecase.getProductCategory(1, productCategoryID);
        ArrayList<Integer> products = new ArrayList<>();
        products.add(productID);
        productCategoryData.products = products;

        this.usecase.run(1, new AbstractMap.SimpleEntry<>(productCategoryID, productCategoryData));
        ProductCategory productCategory = this.storage.getProductCategory(productCategoryID);

        assertEquals(productCategory.getName(), productCategoryData.name);
        assertEquals(productCategory.getProducts().size(), 1);
        assertTrue(productCategory.getProducts().contains(this.storage.getProduct(productID)));

    }

    @Test
    public void testWrongProductID() {
        ProductCategoryData productCategoryData = this.usecase.getProductCategory(1, productCategoryID);
        ArrayList<Integer> products = new ArrayList<>();
        products.add(-1);
        productCategoryData.products = products;
        assertThrows(RuntimeException.class, () -> this.usecase.run(1, new AbstractMap.SimpleEntry<>(productCategoryID, productCategoryData)), "No productID!");

        products = new ArrayList<>();
        products.add(productID);
        productCategoryData.products = products;
        this.usecase.run(1, new AbstractMap.SimpleEntry<>(productCategoryID, productCategoryData));
        ProductCategory productCategory = this.storage.getProductCategory(productCategoryID);

        assertEquals(productCategory.getName(), productCategoryData.name);
        assertEquals(productCategory.getProducts().size(), 1);
        assertTrue(productCategory.getProducts().contains(this.storage.getProduct(productID)));

    }

    @Test
    public void testWrongAction() {
        ProductCategoryData productCategoryData = this.usecase.getProductCategory(1, productCategoryID);
        ArrayList<Integer> products = new ArrayList<>();
        products.add(productID);
        productCategoryData.products = products;

        assertThrows(RuntimeException.class, () -> this.usecase.continueRun(1, productCategoryData));
    }

    @Test
    public void testUpdateCompanyGroupWrongRole() {
        ProductCategoryData productCategoryData = this.usecase.getProductCategory(1, productCategoryID);
        productCategoryData.name = "test";
        assertThrows(RuntimeException.class, () -> usecase.run(3, new AbstractMap.SimpleEntry<>(0, productCategoryData)), "Wrong role");
    }
}
