package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductReturnData;
import ru.trade_manager.UseCases.GetAllProducts;
import ru.trade_manager.mock.Storage;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class GetAllProductsTest {
    private Storage storage;
    private GetAllProducts usecase;

    @Before
    public void init() {
        this.storage = new Storage();
        this.usecase = new GetAllProducts(this.storage);
    }

    @Test
    public void testSuccessfulGetAll() {
        List<ProductReturnData> curProducts = this.usecase.run();
        assertTrue(curProducts.size() >= 4);
    }
}
