package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.UseCases.DeleteProductCategory;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.user.UserRole;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DeleteProductCategoryTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private DeleteProductCategory usecase;
    int firstProductCategory = 3;
    int secondProductCategory = 4;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new DeleteProductCategory(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulDelete() {
        this.usecase.run(1, firstProductCategory);
        assertNull(this.storage.getProductCategory(firstProductCategory));
    }

    @Test(expected = RuntimeException.class)
    public void testDeleteForNonExistingProductCategory() {
        this.usecase.run(1, secondProductCategory);
        this.usecase.run(1, secondProductCategory);
    }

    @Test
    public void testDeleteProductCategoryWrongRole() {
        assertThrows(RuntimeException.class, () -> this.usecase.run(2, secondProductCategory),
                "Wrong role");
    }
}
