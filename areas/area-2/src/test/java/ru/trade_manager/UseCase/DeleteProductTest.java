package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.UseCases.DeleteProduct;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.user.UserRole;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DeleteProductTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private DeleteProduct usecase;
    int firstProductID = 4;
    int secondProductID = 5;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new DeleteProduct(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulDelete() {
        this.usecase.run(1, firstProductID);
        assertNull(this.storage.getProduct(firstProductID));
    }

    @Test(expected = RuntimeException.class)
    public void testDeleteForNonExistingProduct() {
        this.usecase.run(1, secondProductID);
        this.usecase.run(1, secondProductID);
    }

    @Test
    public void testDeleteProductWrongRole() {
        assertThrows(RuntimeException.class, () -> this.usecase.run(2, secondProductID),
                "Wrong role");
    }
}
