package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyData;
import ru.trade_manager.UseCases.UpdateCompany;
import ru.trade_manager.companies.Company;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.user.UserRole;

import java.util.AbstractMap;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UpdateCompanyTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private UpdateCompany usecase;
    int companyID = 4;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new UpdateCompany(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulUpdate() {
        CompanyData companyData = this.usecase.getCompany(1, companyID);
        companyData.email = "some@email.me";
        this.usecase.run(1, new AbstractMap.SimpleEntry<>(companyID, companyData));
        Company company = this.storage.getCompany(companyID);
        assertEquals(company.getEmail(), companyData.email);
    }

    @Test(expected = RuntimeException.class)
    public void testUpdateForNonExistingCompany() {
        CompanyData companyData = this.usecase.getCompany(1, companyID);
        companyData.email = "some@email.me";
        this.usecase.run(1, new AbstractMap.SimpleEntry<>(-1, companyData));
    }

    @Test
    public void testUpdateWithInvalidEmail() {
        CompanyData companyData = this.usecase.getCompany(1, companyID);
        companyData.email = "someemail.me";
        assertThrows(RuntimeException.class, () -> this.usecase.run(1, new AbstractMap.SimpleEntry<>(companyID, companyData)),
                "Invalid Email!");
        companyData.email = "test@me.ru";
        this.usecase.continueRun(1, companyData);
        Company company = this.storage.getCompany(companyID);
        assertEquals(company.getEmail(), companyData.email);

    }

    @Test
    public void testUpdateCompanyWrongRole() {
        CompanyData companyData = this.usecase.getCompany(1, companyID);
        companyData.email = "some@email.me";
        assertThrows(RuntimeException.class, () -> this.usecase.run(3, new AbstractMap.SimpleEntry<>(companyID, companyData)),
                "Wrong role");
    }
}
