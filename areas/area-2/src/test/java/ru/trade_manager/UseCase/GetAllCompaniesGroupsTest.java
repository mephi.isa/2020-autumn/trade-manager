package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyGroupReturnData;
import ru.trade_manager.UseCases.GetAllCompaniesGroups;
import ru.trade_manager.mock.Storage;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class GetAllCompaniesGroupsTest {
    private Storage storage;
    private GetAllCompaniesGroups usecase;

    @Before
    public void init() {
        this.storage = new Storage();
        this.usecase = new GetAllCompaniesGroups(this.storage);
    }

    @Test
    public void testSuccessfulGetAll() {
        List<CompanyGroupReturnData> curCompanies = this.usecase.run();
        assertTrue(curCompanies.size() >= 2);
    }
}
