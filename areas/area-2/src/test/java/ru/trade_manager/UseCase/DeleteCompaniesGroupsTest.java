package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.UseCases.DeleteCompanyGroup;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.user.UserRole;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DeleteCompaniesGroupsTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private DeleteCompanyGroup usecase;
    public int firstCompanyGroupID = 0;
    public int secondCompanyGroupID = 1;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new DeleteCompanyGroup(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulDelete() {
        this.usecase.run(1, firstCompanyGroupID);
        assertNull(this.storage.getCompanyGroups(firstCompanyGroupID));
    }

    @Test(expected = RuntimeException.class)
    public void testDeleteForNonExistingCompanyGroup() {
        this.usecase.run(1, secondCompanyGroupID);
        this.usecase.run(1, secondCompanyGroupID);
    }

    @Test
    public void testDeleteCompanyWrongRole() {
        assertThrows(RuntimeException.class, () -> this.usecase.run(2, secondCompanyGroupID),
                "Wrong role");
    }
}
