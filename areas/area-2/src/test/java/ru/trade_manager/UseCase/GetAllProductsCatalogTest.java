package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductCatalogReturnData;
import ru.trade_manager.UseCases.GetAllProductCatalog;
import ru.trade_manager.mock.Storage;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class GetAllProductsCatalogTest {
    private Storage storage;
    private GetAllProductCatalog usecase;

    @Before
    public void init() {
        this.storage = new Storage();
        this.usecase = new GetAllProductCatalog(this.storage);
    }

    @Test
    public void testSuccessfulGetAll() {
        List<ProductCatalogReturnData> curProductsCatalog = this.usecase.run();
        assertTrue(curProductsCatalog.size() >= 2);
    }
}
