package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyGroupData;
import ru.trade_manager.UseCases.UpdateCompanyGroup;
import ru.trade_manager.companies.CompanyGroup;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.user.UserRole;

import java.util.AbstractMap;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UpdateCompanyGroupTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private UpdateCompanyGroup usecase;
    int companyGroupID = 2;
    int companyID = 3;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new UpdateCompanyGroup(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulUpdate() {
        CompanyGroupData companyGroupData = this.usecase.getCompanyGroup(1, companyGroupID);
        ArrayList<Integer> companies = new ArrayList<>();
        companies.add(companyID);
        companyGroupData.companies = companies;

        this.usecase.run(1, new AbstractMap.SimpleEntry<>(companyGroupID, companyGroupData));
        CompanyGroup companyGroup = this.storage.getCompanyGroups(companyGroupID);
        assertEquals(companyGroup.getCompanies().size(), 1);
        assertTrue(companyGroup.getCompanies().contains(this.storage.getCompany(companyID)));
    }

    @Test
    public void testInvalidCompanyID() {
        CompanyGroupData companyGroupData = this.usecase.getCompanyGroup(1, companyGroupID);
        ArrayList<Integer> companies = new ArrayList<>();
        companies.add(-1);
        companyGroupData.companies = companies;
        assertThrows(RuntimeException.class, () -> this.usecase.run(1, new AbstractMap.SimpleEntry<>(companyGroupID, companyGroupData)), "No CompanyID!");

        companies = new ArrayList<>();
        companies.add(companyID);
        companyGroupData.companies = companies;
        this.usecase.ContinueRun(1, companyGroupData);

        CompanyGroup companyGroup = this.storage.getCompanyGroups(companyGroupID);
        assertEquals(companyGroup.getCompanies().size(), 1);
        assertTrue(companyGroup.getCompanies().contains(this.storage.getCompany(companyID)));
    }

    @Test(expected = RuntimeException.class)
    public void testUpdateForNonExistingProduct() {
        CompanyGroupData companyGroupData = this.usecase.getCompanyGroup(1, companyGroupID);
        companyGroupData.name = "some@email.me";
        this.usecase.run(1, new AbstractMap.SimpleEntry<>(-1, companyGroupData));
    }

    @Test
    public void testUpdateCompanyGroupWrongRole() {
        CompanyGroupData companyGroupData = this.usecase.getCompanyGroup(1, companyGroupID);
        companyGroupData.name = "some@email.me";
        assertThrows(RuntimeException.class, () -> usecase.run(3, new AbstractMap.SimpleEntry<>(0, companyGroupData)), "Wrong role");
    }
}
