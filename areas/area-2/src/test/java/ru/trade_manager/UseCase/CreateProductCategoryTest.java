package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductCategoryData;
import ru.trade_manager.UseCases.CreateProductCategory;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.products.ProductCategory;
import ru.trade_manager.user.UserRole;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CreateProductCategoryTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private CreateProductCategory usecase;
    int productID = 0;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new CreateProductCategory(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulCreate() {
        ArrayList<Integer> products = new ArrayList<>();
        products.add(productID);
        ProductCategoryData productCategoryData = new ProductCategoryData("Meat", products);
        int id = this.usecase.run(1, productCategoryData);
        ProductCategory productCategory = this.storage.getProductCategory(id);

        assertEquals(productCategory.getName(), productCategoryData.name);
        assertEquals(productCategory.getProducts().size(), 1);
        assertTrue(productCategory.getProducts().contains(this.storage.getProduct(this.productID)));
    }

    @Test
    public void testWrongProductCreate() {
        ArrayList<Integer> products = new ArrayList<>();
        products.add(-1);
        ProductCategoryData productCatalogData = new ProductCategoryData("Meat", products);
        assertThrows(RuntimeException.class, () -> this.usecase.run(1, productCatalogData));
    }

    @Test
    public void testCreateCompanyWrongRole() {
        ProductCategoryData productCategoryData = new ProductCategoryData("Beer");
        assertThrows(RuntimeException.class, () -> usecase.run(3, productCategoryData), "Wrong role");
    }
}
