package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductData;
import ru.trade_manager.UseCases.UpdateProduct;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.products.Product;
import ru.trade_manager.user.UserRole;

import java.util.AbstractMap;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UpdateProductTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private UpdateProduct usecase;
    int productID = 8;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new UpdateProduct(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulUpdate() {
        ProductData productData = this.usecase.getProduct(1, productID);
        productData.name = "Vine";
        productData.productCode = 2765;
        productData.barcode = "some@email.me";
        this.usecase.run(1, new AbstractMap.SimpleEntry<>(productID, productData));
        Product product = this.storage.getProduct(productID);

        assertEquals(product.getBarcode(), productData.barcode);
        assertEquals(product.getDescription(), productData.description);
        assertEquals(product.getProductCode(), productData.productCode);
        assertEquals(product.getName(), productData.name);
    }

    @Test
    public void testWrongProductCode() {
        ProductData productData = this.usecase.getProduct(1, productID);
        productData.name = "Vine";
        productData.productCode = -1;
        productData.barcode = "some@email.me";
        assertThrows(RuntimeException.class, () -> this.usecase.run(1, new AbstractMap.SimpleEntry<>(productID, productData)),
                "Wrong role");
        productData.productCode = 99;
        this.usecase.run(1, new AbstractMap.SimpleEntry<>(productID, productData));
        Product product = this.storage.getProduct(productID);

        assertEquals(product.getBarcode(), productData.barcode);
        assertEquals(product.getDescription(), productData.description);
        assertEquals(product.getProductCode(), productData.productCode);
        assertEquals(product.getName(), productData.name);
    }

    @Test
    public void testNotUniqueProductCode() {
        ProductData productData = this.usecase.getProduct(1, productID);
        productData.name = "Vine";
        productData.productCode = 27;
        productData.barcode = "some@email.me";
        assertThrows(RuntimeException.class, () -> this.usecase.run(1, new AbstractMap.SimpleEntry<>(productID, productData)));
    }

    @Test
    public void testWrongAction() {
        ProductData productData = this.usecase.getProduct(1, productID);
        productData.name = "Vine";
        productData.productCode = 27;
        productData.barcode = "some@email.me";
        assertThrows(RuntimeException.class, () -> this.usecase.ContinueRun(1, productData));
    }

    @Test
    public void testWrongRole() {
        ProductData productData = this.usecase.getProduct(1, productID);
        productData.name = "Vine";
        assertThrows(RuntimeException.class, () -> this.usecase.run(2, new AbstractMap.SimpleEntry<>(0, productData)),
                "Wrong role");
    }
}
