package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyData;
import ru.trade_manager.UseCases.DeleteCompany;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.user.UserRole;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DeleteCompaniesTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private DeleteCompany usecase;
    public int firstCompanyID = 1;
    public int secondCompanyID = 2;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new DeleteCompany(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulDelete() {
        this.usecase.run(1, firstCompanyID);
        assertNull(this.storage.getCompany(firstCompanyID));
    }

    @Test(expected = RuntimeException.class)
    public void testDeleteForNonExistingCompany() {
        this.usecase.run(1, secondCompanyID);
        this.usecase.run(1, secondCompanyID);
    }

    @Test
    public void testDeleteCompanyWrongRole() {
        CompanyData companyData = new CompanyData("some@email.me");
        assertThrows(RuntimeException.class, () -> this.usecase.run(2, secondCompanyID),
                "Wrong role");
    }
}
