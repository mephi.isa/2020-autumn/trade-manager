package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyReturnData;
import ru.trade_manager.UseCases.GetAllCompanies;
import ru.trade_manager.mock.Storage;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class GetAllCompaniesTest {
    private Storage storage;
    private GetAllCompanies usecase;

    @Before
    public void init() {
        this.storage = new Storage();
        this.usecase = new GetAllCompanies(this.storage);
    }

    @Test
    public void testSuccessfulGetAll() {
        List<CompanyReturnData> curCompanies = this.usecase.run();
        assertTrue(curCompanies.size() >= 3);
    }
}
