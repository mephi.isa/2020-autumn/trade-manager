package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductCategoryReturnData;
import ru.trade_manager.UseCases.GetAllProductCategory;
import ru.trade_manager.mock.Storage;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class GetAllProductsCategoryTest {
    private Storage storage;
    private GetAllProductCategory usecase;

    @Before
    public void init() {
        this.storage = new Storage();
        this.usecase = new GetAllProductCategory(this.storage);
    }

    @Test
    public void testSuccessfulGetAll() {
        List<ProductCategoryReturnData> curProductsCategory = this.usecase.run();
        assertTrue(curProductsCategory.size() >= 5);
    }
}
