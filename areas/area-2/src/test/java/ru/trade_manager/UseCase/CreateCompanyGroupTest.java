package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyGroupData;
import ru.trade_manager.UseCases.CreateCompanyGroup;
import ru.trade_manager.companies.CompanyGroup;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.user.UserRole;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CreateCompanyGroupTest {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private CreateCompanyGroup usecase;
    private final int companyID = 0;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new CreateCompanyGroup(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulCreate() {
        ArrayList<Integer> companies = new ArrayList<>();
        companies.add(companyID);
        CompanyGroupData companyData = new CompanyGroupData("test", companies);
        int id = this.usecase.run(1, companyData);
        CompanyGroup curCompany = this.storage.getCompanyGroups(id);

        assertEquals(curCompany.getCompanies().size(), 1);
        assertTrue(curCompany.getCompanies().contains(this.storage.getCompany(companyID)));
        assertEquals(curCompany.getName(), companyData.name);
    }

    @Test
    public void testWrongCompanyID() {
        ArrayList<Integer> companies = new ArrayList<>();
        companies.add(-1);
        CompanyGroupData companyData = new CompanyGroupData("test", companies);
        assertThrows(RuntimeException.class, () -> this.usecase.run(1, companyData), "No CompanyID!");
        companies = new ArrayList<>();
        companies.add(companyID);
        companyData.companies = companies;
        int id = this.usecase.run(1, companyData);
        CompanyGroup curCompany = this.storage.getCompanyGroups(id);

        assertEquals(curCompany.getCompanies().size(), 1);
        assertTrue(curCompany.getCompanies().contains(this.storage.getCompany(companyID)));
        assertEquals(curCompany.getName(), companyData.name);
    }

    @Test
    public void testCreateCompanyGroupWrongRole() {
        CompanyGroupData companyData = new CompanyGroupData("test");
        assertThrows(RuntimeException.class, () -> usecase.run(3, companyData), "Wrong role");
    }
}
