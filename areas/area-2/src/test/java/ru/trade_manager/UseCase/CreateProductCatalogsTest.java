package ru.trade_manager.UseCase;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductCatalogData;
import ru.trade_manager.UseCases.CreateProductCatalog;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.products.ProductCatalog;
import ru.trade_manager.user.UserRole;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CreateProductCatalogsTest {


    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private Storage storage;
    private CreateProductCatalog usecase;
    private final int firstProductCatalog = 1;
    private final int secondProductCatalog = 2;
    int productID = 2;

    @Before
    public void init() {
        this.storage = new Storage();
        UserAuthRepository userAuthRepository = new UserAuthRepository();
        this.usecase = new CreateProductCatalog(this.storage, userAuthRepository);
    }

    @Test
    public void testSuccessfulCreate() {
        ArrayList<Integer> products = new ArrayList<>();
        products.add(productID);
        ProductCatalogData productCatalogData = new ProductCatalogData("Meat", products);
        int id = this.usecase.run(1, productCatalogData);
        ProductCatalog productCatalog = this.storage.getProductCatalog(id);

        assertEquals(productCatalog.getName(), productCatalogData.name);
        assertEquals(productCatalog.getCategories().size(), 1);
        assertTrue(productCatalog.getCategories().contains(this.storage.getProductCategory(this.productID)));
    }

    @Test
    public void testWrongProductCategoryCreate() {
        ArrayList<Integer> products = new ArrayList<>();

        products.add(-1);
        ProductCatalogData productCatalogData = new ProductCatalogData("Meat", products);
        assertThrows(RuntimeException.class, () -> this.usecase.run(1, productCatalogData));
    }

    @Test
    public void testCreateCompanyWrongRole() {
        ProductCatalogData productCatalogData = new ProductCatalogData("Beer");
        assertThrows(RuntimeException.class, () -> usecase.run(3, productCatalogData), "Wrong role");
    }
}
