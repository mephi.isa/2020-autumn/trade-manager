package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyGroupData;
import ru.trade_manager.companies.CompanyGroup;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CreateCompanyGroupTest {
    private Network storage;
    private final int companyID = 0;

    @Before
    public void setUp() {
        storage = new Network();
    }


    @Test
    public void testSuccessfulCreate() {
        ArrayList<Integer> companies = new ArrayList<>();
        companies.add(companyID);
        CompanyGroupData companyData = new CompanyGroupData("test", companies);
        int id = this.storage.createCompanyGroup(companyData);
        CompanyGroup curCompany = this.storage.getCompanyGroup(id);

        assertEquals(curCompany.getCompanies().size(), 1);
        assertTrue(curCompany.getCompanies().contains(this.storage.getCompany(companyID)));
        assertEquals(curCompany.getName(), companyData.name);
    }

    @Test
    public void testWrongCompanyID() {
        ArrayList<Integer> companies = new ArrayList<>();
        companies.add(-1);
        CompanyGroupData companyData = new CompanyGroupData("test", companies);
        assertThrows(RuntimeException.class, () -> this.storage.createCompanyGroup(companyData), "No CompanyID!");
        companies = new ArrayList<>();
        companies.add(companyID);
        companyData.companies = companies;
        int id = this.storage.createCompanyGroup(companyData);
        CompanyGroup curCompany = this.storage.getCompanyGroup(id);

        assertEquals(curCompany.getCompanies().size(), 1);
        assertTrue(curCompany.getCompanies().contains(this.storage.getCompany(companyID)));
        assertEquals(curCompany.getName(), companyData.name);
    }
}
