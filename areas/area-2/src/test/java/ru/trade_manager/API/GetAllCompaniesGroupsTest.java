package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyGroupReturnData;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class GetAllCompaniesGroupsTest {
    private Network storage;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulGetAll() {
        List<CompanyGroupReturnData> curCompanies = this.storage.getCompanyGroups();
        assertTrue(curCompanies.size() >= 2);
    }
}
