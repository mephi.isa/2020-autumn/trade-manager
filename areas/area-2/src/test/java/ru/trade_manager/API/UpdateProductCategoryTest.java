package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductCategoryData;
import ru.trade_manager.products.ProductCategory;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UpdateProductCategoryTest {
    private Network storage;
    int productID = 7;
    int productCategoryID = 5;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulUpdate() {
        ProductCategoryData productCategoryData = this.storage.getProductCategoryData(productCategoryID);
        ArrayList<Integer> products = new ArrayList<>();
        products.add(productID);
        productCategoryData.products = products;

        this.storage.updateProductCategory(productCategoryID, productCategoryData);
        ProductCategory productCategory = this.storage.getProductCategory(productCategoryID);

        assertEquals(productCategory.getName(), productCategoryData.name);
        assertEquals(productCategory.getProducts().size(), 1);
        assertTrue(productCategory.getProducts().contains(this.storage.getProduct(productID)));

    }

    @Test
    public void testWrongProductID() {
        ProductCategoryData productCategoryData = this.storage.getProductCategoryData(productCategoryID);
        ArrayList<Integer> products = new ArrayList<>();
        products.add(-1);
        productCategoryData.products = products;
        assertThrows(RuntimeException.class, () -> this.storage.updateProductCategory(productCategoryID, productCategoryData), "No productID!");

        products = new ArrayList<>();
        products.add(productID);
        productCategoryData.products = products;
        this.storage.updateProductCategory(productCategoryID, productCategoryData);
        ProductCategory productCategory = this.storage.getProductCategory(productCategoryID);

        assertEquals(productCategory.getName(), productCategoryData.name);
        assertEquals(productCategory.getProducts().size(), 1);
        assertTrue(productCategory.getProducts().contains(this.storage.getProduct(productID)));

    }
}
