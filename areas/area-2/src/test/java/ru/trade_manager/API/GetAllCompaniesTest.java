package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyReturnData;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class GetAllCompaniesTest {
    private Network storage;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulGetAll() {
        List<CompanyReturnData> curCompanies = this.storage.getCompanies();
        assertTrue(curCompanies.size() >= 3);
    }
}
