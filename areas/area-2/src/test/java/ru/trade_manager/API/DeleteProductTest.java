package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;

public class DeleteProductTest {
    private Network storage;
    int firstProductID = 4;
    int secondProductID = 5;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulDelete() {
        this.storage.deleteProduct(firstProductID);
        assertNull(this.storage.getProduct(firstProductID));
    }

    @Test(expected = RuntimeException.class)
    public void testDeleteForNonExistingProduct() {
        this.storage.deleteProduct(secondProductID);
        this.storage.deleteProduct(secondProductID);
    }
}
