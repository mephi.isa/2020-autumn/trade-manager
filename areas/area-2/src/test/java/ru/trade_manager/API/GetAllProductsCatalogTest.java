package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductCatalogReturnData;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class GetAllProductsCatalogTest {
    private Network storage;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulGetAll() {
        List<ProductCatalogReturnData> curProductsCatalog = this.storage.getProductsCatalogs();
        assertTrue(curProductsCatalog.size() >= 2);
    }
}
