package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyData;
import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.CompanyType;

import java.util.ArrayList;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CreateCompanyTest {
    private Network storage;
    public int departmentID = 0;
    public int legalEntityID = 0;
    public int priceListID = 0;


    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulCreate() {
        HashSet<Integer> departments = new HashSet<>();
        departments.add(departmentID);

        ArrayList<Integer> legalEntities = new ArrayList<>();
        legalEntities.add(legalEntityID);

        ArrayList<Integer> priceLists = new ArrayList<>();
        priceLists.add(priceListID);
        CompanyData companyData = new CompanyData("name", "8800", "Moscow", "test@test.ru",
                "123456789012", CompanyType.INDIVIDUAL_ENTREPRENEUR, departments, legalEntities, priceLists);

        int id = this.storage.createCompany(companyData);
        Company company = this.storage.getCompany(id);

        assertEquals(company.getAddress(), companyData.address);
        assertEquals(company.getCompanyType(), companyData.companyType);
        assertEquals(company.getDepartments().size(), 0);
//        assertTrue(company.getDepartments().contains(this.storage.getDepartment(this.departmentID)));
        assertEquals(company.getEmail(), companyData.email);
        assertEquals(company.getInn(), companyData.inn);
        assertEquals(company.getLegalEntities().size(), 1);
        assertTrue(company.getLegalEntities().contains(this.storage.getLegalEntity(this.legalEntityID)));
        assertEquals(company.getName(), companyData.name);
        assertEquals(company.getPhone(), companyData.phone);
        assertEquals(company.getPriceLists().size(), 1);
        assertTrue(company.getPriceLists().contains(this.storage.getPriceList(this.priceListID)));
    }

    @Test
    public void testWrongInnLength() {
        ArrayList<Integer> legalEntities = new ArrayList<>();
        legalEntities.add(legalEntityID);

        ArrayList<Integer> priceLists = new ArrayList<>();
        priceLists.add(priceListID);
        CompanyData companyData = new CompanyData("name", "8800", "Moscow", "test@test.ru",
                "123456789", CompanyType.INDIVIDUAL_ENTREPRENEUR, new HashSet<>(), legalEntities, priceLists);

        assertThrows(RuntimeException.class, () -> this.storage.createCompany(companyData), "Inn length must be equal 12!");
        companyData.inn = "123456789019";
        int id = this.storage.createCompany(companyData);
        Company company = this.storage.getCompany(id);

        assertEquals(company.getAddress(), companyData.address);
        assertEquals(company.getCompanyType(), companyData.companyType);
        assertEquals(company.getDepartments().size(), 0);
        assertEquals(company.getEmail(), companyData.email);
        assertEquals(company.getInn(), companyData.inn);
        assertEquals(company.getLegalEntities().size(), 1);
        assertTrue(company.getLegalEntities().contains(this.storage.getLegalEntity(this.legalEntityID)));
        assertEquals(company.getName(), companyData.name);
        assertEquals(company.getPhone(), companyData.phone);
        assertEquals(company.getPriceLists().size(), 1);
        assertTrue(company.getPriceLists().contains(this.storage.getPriceList(this.priceListID)));
    }

    @Test
    public void testDuplicateInn() {
        ArrayList<Integer> legalEntities = new ArrayList<>();
        legalEntities.add(legalEntityID);

        ArrayList<Integer> priceLists = new ArrayList<>();
        priceLists.add(priceListID);
        CompanyData companyData = new CompanyData("name", "8800", "Moscow", "test@test.ru",
                "123456789012", CompanyType.INDIVIDUAL_ENTREPRENEUR, new HashSet<>(), legalEntities, priceLists);
        this.storage.createCompany(companyData);
        companyData.address = "123 Moscow";
        assertThrows(RuntimeException.class, () -> this.storage.createCompany(companyData), "Inn must be UNIQUE!");
        companyData.inn = "123456789089";
        int id = this.storage.createCompany(companyData);
        Company company = this.storage.getCompany(id);

        assertEquals(company.getAddress(), companyData.address);
        assertEquals(company.getCompanyType(), companyData.companyType);
        assertEquals(company.getDepartments().size(), 0);
        assertEquals(company.getEmail(), companyData.email);
        assertEquals(company.getInn(), companyData.inn);
        assertEquals(company.getLegalEntities().size(), 1);
        assertTrue(company.getLegalEntities().contains(this.storage.getLegalEntity(this.legalEntityID)));
        assertEquals(company.getName(), companyData.name);
        assertEquals(company.getPhone(), companyData.phone);
        assertEquals(company.getPriceLists().size(), 1);
        assertTrue(company.getPriceLists().contains(this.storage.getPriceList(this.priceListID)));
    }

    @Test
    public void testCreateCompanyWrongRole() {
        CompanyData companyData = new CompanyData("name", "8800", "Moscow", "test@test.ru",
                "123", CompanyType.INDIVIDUAL_ENTREPRENEUR);
        assertThrows(RuntimeException.class, () -> this.storage.createCompany(companyData), "Wrong role");
    }
}
