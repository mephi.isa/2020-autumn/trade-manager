package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductReturnData;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class GetAllProductsTest {
    private Network storage;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulGetAll() {
        List<ProductReturnData> curProducts = this.storage.getProducts();
        assertTrue(curProducts.size() >= 4);
    }
}
