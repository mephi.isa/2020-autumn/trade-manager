package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductCategoryData;
import ru.trade_manager.products.ProductCategory;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CreateProductCategoryTest {
    private Network storage;
    int productID = 0;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulCreate() {
        ArrayList<Integer> products = new ArrayList<>();
        products.add(productID);
        ProductCategoryData productCategoryData = new ProductCategoryData("Meat", products);
        int id = this.storage.createProductCategory(productCategoryData);
        ProductCategory productCategory = this.storage.getProductCategory(id);

        assertEquals(productCategory.getName(), productCategoryData.name);
        assertEquals(productCategory.getProducts().size(), 1);
        assertTrue(productCategory.getProducts().contains(this.storage.getProduct(this.productID)));
    }

    @Test
    public void testWrongProductCreate() {
        ArrayList<Integer> products = new ArrayList<>();
        products.add(-1);
        ProductCategoryData productCatalogData = new ProductCategoryData("Meat", products);
        assertThrows(RuntimeException.class, () -> this.storage.createProductCategory(productCatalogData));
    }
}
