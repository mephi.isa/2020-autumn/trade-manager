package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductData;
import ru.trade_manager.products.Product;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CreateProductTest {
    private Network storage;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulCreate() {
        ProductData productData = new ProductData("Beer", "Baltika", 1227, "some@email.me");
        int id = this.storage.createProduct(productData);
        Product product = this.storage.getProduct(id);

        assertEquals(product.getBarcode(), productData.barcode);
        assertEquals(product.getDescription(), productData.description);
        assertEquals(product.getProductCode(), productData.productCode);
        assertEquals(product.getName(), productData.name);
    }

    @Test
    public void testDuplicateProductCode() {
        ProductData ProductData = new ProductData("Meat", "New", 27, "some@email.me");
        assertThrows(RuntimeException.class, () -> this.storage.createProduct(ProductData));
    }
}
