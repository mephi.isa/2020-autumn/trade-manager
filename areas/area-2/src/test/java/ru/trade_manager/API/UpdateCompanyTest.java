package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyData;
import ru.trade_manager.companies.Company;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UpdateCompanyTest {
    private Network storage;
    int companyID = 4;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulUpdate() {
        CompanyData companyData = this.storage.getCompanyData(companyID);
        companyData.email = "some@email.me";
        this.storage.updateCompany(companyID, companyData);
        Company company = this.storage.getCompany(companyID);
        assertEquals(company.getEmail(), companyData.email);
    }

    @Test(expected = RuntimeException.class)
    public void testUpdateForNonExistingCompany() {
        CompanyData companyData = this.storage.getCompanyData(companyID);
        companyData.email = "some@email.me";
        this.storage.updateCompany(-1, companyData);
    }

    @Test
    public void testUpdateWithInvalidEmail() {
        CompanyData companyData = this.storage.getCompanyData(companyID);
        companyData.email = "someemail.me";
        assertThrows(RuntimeException.class, () -> this.storage.updateCompany(companyID, companyData),
                "Invalid Email!");
        companyData.email = "test@me.ru";
        this.storage.updateCompany(companyID, companyData);
        Company company = this.storage.getCompany(companyID);
        assertEquals(company.getEmail(), companyData.email);

    }
}
