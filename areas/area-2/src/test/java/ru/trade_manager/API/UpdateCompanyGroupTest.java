package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.CompanyGroupData;
import ru.trade_manager.companies.CompanyGroup;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UpdateCompanyGroupTest {
    private Network storage;
    int companyGroupID = 2;
    int companyID = 3;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulUpdate() {
        CompanyGroupData companyGroupData = this.storage.getCompanyGroupData(companyGroupID);
        ArrayList<Integer> companies = new ArrayList<>();
        companies.add(companyID);
        companyGroupData.companies = companies;

        this.storage.updateCompanyGroup(companyGroupID, companyGroupData);
        CompanyGroup companyGroup = this.storage.getCompanyGroup(companyGroupID);
        assertEquals(companyGroup.getCompanies().size(), 1);
        assertTrue(companyGroup.getCompanies().contains(this.storage.getCompany(companyID)));
    }

    @Test
    public void testInvalidCompanyID() {
        CompanyGroupData companyGroupData = this.storage.getCompanyGroupData(companyGroupID);
        ArrayList<Integer> companies = new ArrayList<>();
        companies.add(-1);
        companyGroupData.companies = companies;
        assertThrows(RuntimeException.class, () -> this.storage.updateCompanyGroup(companyGroupID, companyGroupData), "No CompanyID!");

        companies = new ArrayList<>();
        companies.add(companyID);
        companyGroupData.companies = companies;
        this.storage.updateCompanyGroup(companyGroupID, companyGroupData);

        CompanyGroup companyGroup = this.storage.getCompanyGroup(companyGroupID);
        assertEquals(companyGroup.getCompanies().size(), 1);
        assertTrue(companyGroup.getCompanies().contains(this.storage.getCompany(companyID)));
    }

    @Test(expected = RuntimeException.class)
    public void testUpdateForNonExistingProduct() {
        CompanyGroupData companyGroupData = this.storage.getCompanyGroupData(companyGroupID);
        companyGroupData.name = "some@email.me";
        this.storage.updateCompanyGroup(-1, companyGroupData);
    }
}

