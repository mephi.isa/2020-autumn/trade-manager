package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;

public class DeleteCompaniesTest {
    private Network storage;
    public int firstCompanyID = 1;
    public int secondCompanyID = 2;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulDelete() {
        this.storage.deleteCompany(firstCompanyID);
        assertNull(this.storage.getCompany(firstCompanyID));
    }

    @Test(expected = RuntimeException.class)
    public void testDeleteForNonExistingCompany() {
        this.storage.deleteCompany(secondCompanyID);
        this.storage.deleteCompany(secondCompanyID);
    }
}
