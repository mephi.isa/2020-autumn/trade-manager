package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductCategoryReturnData;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class GetAllProductsCategoryTest {
    private Network storage;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulGetAll() {
        List<ProductCategoryReturnData> curProductsCategory = this.storage.getProductsCategories();
        assertTrue(curProductsCategory.size() >= 5);
    }
}
