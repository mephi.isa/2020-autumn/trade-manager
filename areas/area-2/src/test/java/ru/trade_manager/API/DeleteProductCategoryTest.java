package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;

public class DeleteProductCategoryTest {
    private Network storage;
    int firstProductCategory = 3;
    int secondProductCategory = 4;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulDelete() {
        this.storage.deleteProductCategory(firstProductCategory);
        assertNull(this.storage.getProductCategory(firstProductCategory));
    }

    @Test(expected = RuntimeException.class)
    public void testDeleteForNonExistingProductCategory() {
        this.storage.deleteProductCategory(secondProductCategory);
        this.storage.deleteProductCategory(secondProductCategory);
    }
}
