package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductCatalogData;
import ru.trade_manager.products.ProductCatalog;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class CreateProductCatalogsTest {
    private Network storage;
    int productID = 2;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulCreate() {
        ArrayList<Integer> products = new ArrayList<>();
        products.add(productID);
        ProductCatalogData productCatalogData = new ProductCatalogData("Meat", products);
        int id = this.storage.createProductCatalog(productCatalogData);
        ProductCatalog productCatalog = this.storage.getProductCatalog(id);

        assertEquals(productCatalog.getName(), productCatalogData.name);
        assertEquals(productCatalog.getCategories().size(), 1);
        assertTrue(productCatalog.getCategories().contains(this.storage.getProductCategory(this.productID)));
    }

    @Test
    public void testWrongProductCategoryCreate() {
        ArrayList<Integer> products = new ArrayList<>();
        products.add(-1);
        ProductCatalogData productCatalogData = new ProductCatalogData("Meat", products);
        assertThrows(RuntimeException.class, () -> this.storage.createProductCatalog(productCatalogData));
    }
}
