package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;

public class DeleteCompaniesGroupsTest {
    private Network storage;
    public int firstCompanyGroupID = 0;
    public int secondCompanyGroupID = 1;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulDelete() {
        this.storage.deleteCompanyGroup(firstCompanyGroupID);
        assertNull(this.storage.getCompanyGroup(firstCompanyGroupID));
    }

    @Test(expected = RuntimeException.class)
    public void testDeleteForNonExistingCompanyGroup() {
        this.storage.deleteCompanyGroup(secondCompanyGroupID);
        this.storage.deleteCompanyGroup(secondCompanyGroupID);
    }
}
