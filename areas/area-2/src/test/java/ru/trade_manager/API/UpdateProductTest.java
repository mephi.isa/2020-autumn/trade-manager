package ru.trade_manager.API;

import org.junit.Before;
import org.junit.Test;
import ru.trade_manager.ObjectData.ProductData;
import ru.trade_manager.products.Product;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UpdateProductTest {
    private Network storage;
    int productID = 8;

    @Before
    public void init() {
        this.storage = new Network();
    }

    @Test
    public void testSuccessfulUpdate() {
        ProductData productData = this.storage.getProductData(productID);
        productData.name = "Vine";
        productData.productCode = 2765;
        productData.barcode = "some@email.me";
        this.storage.updateProduct(productID, productData);
        Product product = this.storage.getProduct(productID);

        assertEquals(product.getBarcode(), productData.barcode);
        assertEquals(product.getDescription(), productData.description);
        assertEquals(product.getProductCode(), productData.productCode);
        assertEquals(product.getName(), productData.name);
    }

    @Test
    public void testWrongProductCode() {
        ProductData productData = this.storage.getProductData(productID);
        productData.name = "Vine";
        productData.productCode = -1;
        productData.barcode = "some@email.me";
        assertThrows(RuntimeException.class, () -> this.storage.updateProduct(productID, productData),
                "Wrong role");
        productData.productCode = 99;
        this.storage.updateProduct(productID, productData);
        Product product = this.storage.getProduct(productID);

        assertEquals(product.getBarcode(), productData.barcode);
        assertEquals(product.getDescription(), productData.description);
        assertEquals(product.getProductCode(), productData.productCode);
        assertEquals(product.getName(), productData.name);
    }

    @Test
    public void testNotUniqueProductCode() {
        ProductData productData = this.storage.getProductData(productID);
        productData.name = "Vine";
        productData.productCode = 27;
        productData.barcode = "some@email.me";
        assertThrows(RuntimeException.class, () -> this.storage.updateProduct(productID, productData));
    }
}
