package ru.trade_manager.interfaces;

import ru.trade_manager.user.UserRole;

public interface UserAuthRepositoryI {
    void assertRole(long id, UserRole... role) throws RuntimeException;
}
