package ru.trade_manager.interfaces;

import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.CompanyGroup;
import ru.trade_manager.companies.LegalEntity;
import ru.trade_manager.companies.PriceList;
import ru.trade_manager.products.Product;
import ru.trade_manager.products.ProductCatalog;
import ru.trade_manager.products.ProductCategory;

import java.util.List;

public interface StorageI {
    int createCompany(Company company);

    Company getCompany(int companyId);

    List<Company> getCompanies();

    void updateCompany(Company company);

    void deleteCompany(Company company);

    int getInnCount(String inn);

    int createCompanyGroup(CompanyGroup company);

    CompanyGroup getCompanyGroups(int companyGroupId);

    List<CompanyGroup> getCompanyGroup();

    void updateCompanyGroup(CompanyGroup company);

    void deleteCompanyGroup(CompanyGroup company);

    int createProduct(Product product);

    Product getProduct(int productId);

    List<Product> getProducts();

    void updateProduct(Product product);

    void deleteProduct(Product product);

    int getProductCodeCount(int productCode);

    int createProductCategory(ProductCategory productCategory);

    ProductCategory getProductCategory(int productCategoryId);

    List<ProductCategory> getProductsCategories();

    void updateProductCategory(ProductCategory productCategory);

    void deleteProductCategory(ProductCategory productCategory);

    int createProductCatalog(ProductCatalog productCatalog);

    ProductCatalog getProductCatalog(int productCategoryId);

    List<ProductCatalog> getProductsCatalogs();

    void updateProductCatalog(ProductCatalog productCatalog);

    void deleteProductCatalog(ProductCatalog productCatalog);

//    public int createDepartment(Department department);
//    public Department getDepartment(int departmentId);

    int createPriceList(PriceList priceList);

    PriceList getPriceList(int priceListId);

    int createLegalEntity(LegalEntity legalEntity);

    LegalEntity getLegalEntity(int legalEntityId);
}
