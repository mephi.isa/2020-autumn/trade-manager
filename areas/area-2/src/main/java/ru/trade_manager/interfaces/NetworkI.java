package ru.trade_manager.interfaces;

import ru.trade_manager.ObjectData.*;
import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.CompanyGroup;
import ru.trade_manager.companies.LegalEntity;
import ru.trade_manager.companies.PriceList;
import ru.trade_manager.products.Product;
import ru.trade_manager.products.ProductCatalog;
import ru.trade_manager.products.ProductCategory;

import java.util.List;

public interface NetworkI {
    int createCompany(CompanyData company);

    Company getCompany(int companyId);

    CompanyData getCompanyData(int companyId);

    List<CompanyReturnData> getCompanies();

    void updateCompany(int companyId, CompanyData company);

    void deleteCompany(int companyId);

    int createCompanyGroup(CompanyGroupData company);

    CompanyGroup getCompanyGroup(int companyGroupId);

    CompanyGroupData getCompanyGroupData(int companyId);

    List<CompanyGroupReturnData> getCompanyGroups();

    void updateCompanyGroup(int companyId, CompanyGroupData company);

    void deleteCompanyGroup(int company_id);

    int createProduct(ProductData product);

    Product getProduct(int productId);

    ProductData getProductData(int productId);

    List<ProductReturnData> getProducts();

    void updateProduct(int productId, ProductData product);

    void deleteProduct(int product_id);

    int createProductCategory(ProductCategoryData productCategory);

    ProductCategory getProductCategory(int productCategoryId);

    ProductCategoryData getProductCategoryData(int productCategoryId);


    List<ProductCategoryReturnData> getProductsCategories();

    void updateProductCategory(int productCategoryId, ProductCategoryData productCategory);

    void deleteProductCategory(int productCategory_id);

    int createProductCatalog(ProductCatalogData productCatalog);

    ProductCatalog getProductCatalog(int productCategoryId);

    List<ProductCatalogReturnData> getProductsCatalogs();

    PriceList getPriceList(int priceListId);

    LegalEntity getLegalEntity(int legalEntityId);
}
