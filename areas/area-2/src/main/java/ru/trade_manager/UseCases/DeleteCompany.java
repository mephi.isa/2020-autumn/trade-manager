package ru.trade_manager.UseCases;

import ru.trade_manager.companies.Company;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.user.UserRole;


public class DeleteCompany {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;

    public DeleteCompany(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }


    public void run(int id, int companyID) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        Company company = this.storage.getCompany(companyID);
        if (company == null) {
            throw new RuntimeException("Wrong Company ID");
        }
        this.storage.deleteCompany(company);
    }
}
