package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.ProductData;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.products.Product;
import ru.trade_manager.user.UserRole;

public class CreateProduct {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;

    public CreateProduct(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public int run(int id, ProductData productData) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        if (this.storage.getProductCodeCount(productData.productCode) != 0) {
            throw new RuntimeException("productCode must be UNIQUE!");
        }
        int productId = this.storage.getProducts().size();
        Product product = new Product(productId, productData.name, productData.description, productData.productCode, productData.barcode);
        return this.storage.createProduct(product);
    }
}
