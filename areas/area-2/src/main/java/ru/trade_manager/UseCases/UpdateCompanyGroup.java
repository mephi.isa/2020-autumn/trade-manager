package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.CompanyGroupData;
import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.CompanyGroup;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.user.UserRole;

import java.util.AbstractMap;
import java.util.HashSet;

public class UpdateCompanyGroup {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;
    int companyGroupId = -1;

    public UpdateCompanyGroup(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public void run(int id, AbstractMap.SimpleEntry<Integer, CompanyGroupData> input) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        companyGroupId = input.getKey();
        this.ContinueRun(id, input.getValue());
    }

    public CompanyGroupData getCompanyGroup(int id, int CompanyGroupID) {
        this.authRepository.assertRole(id, UserRole.Admin);
        CompanyGroup companyGroup = this.storage.getCompanyGroups(CompanyGroupID);
        return new CompanyGroupData(companyGroup);
    }

    public void ContinueRun(int id, CompanyGroupData input) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        if (companyGroupId == -1) {
            throw new RuntimeException("Wrong Action!");
        }
        HashSet<Company> companies = new HashSet<>();
        for (Integer companyID : input.companies) {
            if (companyID == null) {
                continue;
            }
            Company company = this.storage.getCompany(companyID);
            if (company == null) {
                throw new RuntimeException("No CompanyID!");
            }
            companies.add(company);
        }
        CompanyGroup companyGroup = this.storage.getCompanyGroups(companyGroupId);
        companyGroup.setCompanies(companies);
        this.storage.updateCompanyGroup(companyGroup);
        companyGroupId = -1;
    }
}
