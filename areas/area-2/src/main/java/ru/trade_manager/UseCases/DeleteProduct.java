package ru.trade_manager.UseCases;

import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.products.Product;
import ru.trade_manager.user.UserRole;


public class DeleteProduct {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;

    public DeleteProduct(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public void run(int id, int productID) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        Product product = this.storage.getProduct(productID);
        if (product == null) {
            throw new RuntimeException("Wrong product ID");
        }
        this.storage.deleteProduct(product);
    }
}
