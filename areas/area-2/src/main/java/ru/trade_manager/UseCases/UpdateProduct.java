package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.ProductData;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.products.Product;
import ru.trade_manager.user.UserRole;

import java.util.AbstractMap;

public class UpdateProduct {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;
    private int productID = -1;

    public UpdateProduct(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public void run(int id, AbstractMap.SimpleEntry<Integer, ProductData> input) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        this.productID = input.getKey();
        ProductData productData = input.getValue();
        this.ContinueRun(id, productData);
    }

    public ProductData getProduct(int id, int ProductID) {
        this.authRepository.assertRole(id, UserRole.Admin);
        Product product = this.storage.getProduct(ProductID);
        return new ProductData(product);
    }

    public void ContinueRun(int id, ProductData productData) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        if (this.productID == -1) {
            throw new RuntimeException("Wrong Action!");
        }
        if (productData.productCode > 0) {
            if (this.storage.getProductCodeCount(productData.productCode) != 0 && this.storage.getProduct(this.productID).getProductCode() != productData.productCode) {
                throw new RuntimeException("productCode must be UNIQUE!");
            }
        } else {
            throw new RuntimeException("productCode must be > 0!");
        }
        Product product = this.storage.getProduct(this.productID);
        product.setBarcode(productData.barcode);
        product.setDescription(productData.description);
        product.setName(productData.name);
        product.setProductCode(productData.productCode);
        this.storage.updateProduct(product);
        this.productID = -1;
    }
}
