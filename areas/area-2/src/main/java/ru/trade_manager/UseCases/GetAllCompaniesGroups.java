package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.CompanyGroupReturnData;
import ru.trade_manager.companies.CompanyGroup;
import ru.trade_manager.interfaces.StorageI;

import java.util.ArrayList;
import java.util.List;

public class GetAllCompaniesGroups {
    private final StorageI storage;

    public GetAllCompaniesGroups(StorageI storage) {
        this.storage = storage;
    }

    public List<CompanyGroupReturnData> run() {
        List<CompanyGroupReturnData> companiesGroups = new ArrayList<>();
        for (CompanyGroup companyGroup : this.storage.getCompanyGroup()) {
            companiesGroups.add(new CompanyGroupReturnData(companyGroup));
        }
        return companiesGroups;
    }
}
