package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.ProductCatalogData;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.products.ProductCatalog;
import ru.trade_manager.products.ProductCategory;
import ru.trade_manager.user.UserRole;

public class CreateProductCatalog {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;

    public CreateProductCatalog(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public int run(int id, ProductCatalogData productData) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        int productCatalogId = this.storage.getProductsCatalogs().size();
        ProductCatalog productCatalog = new ProductCatalog(productCatalogId, productData.name);
        for (Integer productID : productData.categories) {
            ProductCategory productCategory = this.storage.getProductCategory(productID);
            if (productCategory == null) {
                throw new RuntimeException("No productCategory!");
            }
            productCatalog.addProductCategory(productCategory);
        }
        return this.storage.createProductCatalog(productCatalog);
    }
}
