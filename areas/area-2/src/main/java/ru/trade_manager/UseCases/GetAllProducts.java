package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.ProductReturnData;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.products.Product;

import java.util.ArrayList;
import java.util.List;

public class GetAllProducts {
    private final StorageI storage;

    public GetAllProducts(StorageI storage) {
        this.storage = storage;
    }

    public List<ProductReturnData> run() {
        List<ProductReturnData> products = new ArrayList<>();
        for (Product product : this.storage.getProducts()) {
            products.add(new ProductReturnData(product));
        }
        return products;
    }
}
