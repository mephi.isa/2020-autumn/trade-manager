package ru.trade_manager.UseCases;

import ru.trade_manager.companies.CompanyGroup;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.user.UserRole;


public class DeleteCompanyGroup {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;

    public DeleteCompanyGroup(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public void run(int id, int CompanyGroupID) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        CompanyGroup companyGroup = this.storage.getCompanyGroups(CompanyGroupID);
        if (companyGroup == null) {
            throw new RuntimeException("Wrong CompanyGroup ID");
        }
        this.storage.deleteCompanyGroup(companyGroup);
    }
}
