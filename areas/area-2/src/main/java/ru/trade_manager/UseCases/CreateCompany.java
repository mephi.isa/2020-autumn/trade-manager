package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.CompanyData;
import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.LegalEntity;
import ru.trade_manager.companies.PriceList;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.user.UserRole;

public class CreateCompany {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;

    public CreateCompany(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public int run(int id, CompanyData companyData) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        if (companyData.inn.length() != 12) {
            throw new RuntimeException("Inn length must be equal 12!");
        }

        if (this.storage.getInnCount(companyData.inn) != 0) {
            throw new RuntimeException("Inn must be UNIQUE!");
        }
        int companyId = this.storage.getCompanies().size();
        Company company = new Company(companyId, companyData.name, companyData.phone, companyData.address, companyData.email, companyData.inn, companyData.companyType);
//        for (Integer departmentID : companyData.departments) {
//            company.addDepartment(this.storage.getDepartment(departmentID));
//        }
        for (Integer legalEntityID : companyData.legalEntities) {
            LegalEntity legalEntity = this.storage.getLegalEntity(legalEntityID);
            if (legalEntity == null) {
                throw new RuntimeException(String.format("Wrong legal entity id %d", legalEntityID));
            }
            company.addLegalEntity(legalEntity);
        }
        for (Integer priceListID : companyData.priceLists) {
            PriceList priceList = this.storage.getPriceList(priceListID);
            if (priceList == null) {
                throw new RuntimeException(String.format("Wrong price list id %d", priceListID));
            }
            company.addPriceList(priceList);
        }
        return this.storage.createCompany(company);
    }
}
