package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.CompanyData;
import ru.trade_manager.companies.Company;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.user.UserRole;

import java.util.AbstractMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UpdateCompany {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;
    private int companyID = -1;
    private static final String regex = "^(.+)@(.+)$";

    public UpdateCompany(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public void run(int id, AbstractMap.SimpleEntry<Integer, CompanyData> input) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        companyID = input.getKey();
        this.continueRun(id, input.getValue());
    }

    public CompanyData getCompany(int id, int CompanyID) {
        this.authRepository.assertRole(id, UserRole.Admin);
        Company company = this.storage.getCompany(CompanyID);
        return new CompanyData(company);
    }

    public void continueRun(int id, CompanyData companyData) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        if (companyID == -1) {
            throw new RuntimeException("Wrong Action!");
        }
        if (!this.checkEmail(companyData.email)) {
            throw new RuntimeException("Invalid Email!");
        }
        Company company = this.storage.getCompany(companyID);
        company.setEmail(companyData.email);
        company.setAddress(companyData.address);
        company.setCompanyType(companyData.companyType);
        company.setInn(companyData.inn);
        company.setName(companyData.name);
        company.setPhone(companyData.phone);
        this.storage.updateCompany(company);
        companyID = -1;
    }

    private boolean checkEmail(String email) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
