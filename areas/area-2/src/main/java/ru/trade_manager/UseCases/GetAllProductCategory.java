package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.ProductCategoryReturnData;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.products.ProductCategory;

import java.util.ArrayList;
import java.util.List;

public class GetAllProductCategory {
    private final StorageI storage;

    public GetAllProductCategory(StorageI storage) {
        this.storage = storage;
    }

    public List<ProductCategoryReturnData> run() {
        List<ProductCategoryReturnData> productCategories = new ArrayList<>();
        for (ProductCategory productCategory : this.storage.getProductsCategories()) {
            productCategories.add(new ProductCategoryReturnData(productCategory));
        }
        return productCategories;
    }
}
