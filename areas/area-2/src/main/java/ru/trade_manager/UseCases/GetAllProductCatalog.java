package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.ProductCatalogReturnData;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.products.ProductCatalog;

import java.util.ArrayList;
import java.util.List;

public class GetAllProductCatalog {
    private final StorageI storage;

    public GetAllProductCatalog(StorageI storage) {
        this.storage = storage;
    }

    public List<ProductCatalogReturnData> run() {
        List<ProductCatalogReturnData> productCatalogs = new ArrayList<>();
        for (ProductCatalog productCatalog : this.storage.getProductsCatalogs()) {
            productCatalogs.add(new ProductCatalogReturnData(productCatalog));
        }
        return productCatalogs;
    }
}
