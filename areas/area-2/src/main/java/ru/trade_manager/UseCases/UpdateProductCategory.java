package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.ProductCategoryData;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.products.Product;
import ru.trade_manager.products.ProductCategory;
import ru.trade_manager.user.UserRole;

import java.util.AbstractMap;
import java.util.HashSet;

public class UpdateProductCategory {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;
    private int productCategoryID = -1;

    public UpdateProductCategory(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public void run(int id, AbstractMap.SimpleEntry<Integer, ProductCategoryData> input) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        this.productCategoryID = input.getKey();
        ProductCategoryData productCategoryData = input.getValue();
        this.continueRun(id, productCategoryData);
    }

    public ProductCategoryData getProductCategory(int id, int ProductCategoryID) {
        this.authRepository.assertRole(id, UserRole.Admin);
        ProductCategory productCategory = this.storage.getProductCategory(ProductCategoryID);
        return new ProductCategoryData(productCategory);
    }

    public void continueRun(int id, ProductCategoryData input) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        if (this.productCategoryID == -1) {
            throw new RuntimeException("Wrong Action!");
        }

        HashSet<Product> products = new HashSet<>();
        for (Integer productID : input.products) {
            if (productID == null) {
                continue;
            }
            Product product = this.storage.getProduct(productID);
            if (product == null) {
                throw new RuntimeException("No productID!");
            }
            products.add(product);
        }

        ProductCategory productCategory = this.storage.getProductCategory(this.productCategoryID);
        productCategory.setProducts(products);
        productCategory.setName(input.name);
        this.storage.updateProductCategory(productCategory);
        this.productCategoryID = -1;
    }
}
