package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.ProductCategoryData;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.products.Product;
import ru.trade_manager.products.ProductCategory;
import ru.trade_manager.user.UserRole;

public class CreateProductCategory {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;

    public CreateProductCategory(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public int run(int id, ProductCategoryData productData) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        int productCategoryId = this.storage.getProductsCategories().size();
        ProductCategory productCategory = new ProductCategory(productCategoryId, productData.name);
        for (Integer productID : productData.products) {
            Product product = this.storage.getProduct(productID);
            if (product == null) {
                throw new RuntimeException("No productID!");
            }
            productCategory.addProduct(product);
        }
        return this.storage.createProductCategory(productCategory);
    }
}
