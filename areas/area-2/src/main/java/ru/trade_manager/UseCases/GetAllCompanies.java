package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.CompanyReturnData;
import ru.trade_manager.companies.Company;
import ru.trade_manager.interfaces.StorageI;

import java.util.ArrayList;
import java.util.List;

public class GetAllCompanies {
    private final StorageI storage;

    public GetAllCompanies(StorageI storage) {
        this.storage = storage;
    }

    public List<CompanyReturnData> run() {
        List<CompanyReturnData> companies = new ArrayList<>();
        for (Company company : this.storage.getCompanies()) {
            companies.add(new CompanyReturnData(company));
        }
        return companies;
    }
}
