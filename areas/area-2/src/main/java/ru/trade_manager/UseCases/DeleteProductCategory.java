package ru.trade_manager.UseCases;

import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.products.ProductCategory;
import ru.trade_manager.user.UserRole;


public class DeleteProductCategory {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;


    public DeleteProductCategory(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public void run(int id, int productCategoryID) {
        this.authRepository.assertRole(id, UserRole.Admin);
        ProductCategory productCategory = this.storage.getProductCategory(productCategoryID);
        if (productCategory == null) {
            throw new RuntimeException("Wrong productCategory ID");
        }
        this.storage.deleteProductCategory(productCategory);
    }
}
