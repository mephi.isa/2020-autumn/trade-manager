package ru.trade_manager.UseCases;

import ru.trade_manager.ObjectData.CompanyGroupData;
import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.CompanyGroup;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.user.UserRole;

public class CreateCompanyGroup {
    private final StorageI storage;
    private final UserAuthRepositoryI authRepository;

    public CreateCompanyGroup(StorageI storage, UserAuthRepositoryI authRepository) {
        this.storage = storage;
        this.authRepository = authRepository;
    }

    public int run(int id, CompanyGroupData companyGroupData) throws RuntimeException {
        this.authRepository.assertRole(id, UserRole.Admin);
        int companyGroupId = this.storage.getCompanyGroup().size();
        CompanyGroup companyGroup = new CompanyGroup(companyGroupId, companyGroupData.name);
        for (Integer companyID : companyGroupData.companies) {
            Company company = this.storage.getCompany(companyID);
            if (company == null) {
                throw new RuntimeException("No CompanyID!");
            }
            companyGroup.addCompany(company);
        }
        return this.storage.createCompanyGroup(companyGroup);
    }
}
