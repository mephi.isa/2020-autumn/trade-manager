package ru.trade_manager.companies;

public enum CompanyType {
    INDIVIDUAL_ENTREPRENEUR,
    LEGAL_ENTITY
}
