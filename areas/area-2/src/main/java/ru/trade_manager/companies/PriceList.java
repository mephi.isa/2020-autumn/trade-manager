package ru.trade_manager.companies;

import ru.trade_manager.products.Product;

import java.util.HashMap;

public class PriceList {
    private int id;
    private String name;
    private HashMap<Product, Float> prices = new HashMap<>();
    private PriceListType type;

    public PriceList(int id, String name, PriceListType type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
