package ru.trade_manager.companies;

import java.util.HashSet;

public class CompanyGroup {
    private int id;
    private String name;
    private HashSet<Company> companies = new HashSet<>();

    public CompanyGroup(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public CompanyGroup(int id, String name, HashSet<Company> companies) {
        this.id = id;
        this.name = name;
        this.companies = companies;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public HashSet<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(HashSet<Company> companies) {
        this.companies = companies;
    }

    public void addCompany(Company company) {
        companies.add(company);
    }

    public void deleteCompany(Company company) {
        companies.remove(company);
    }

    public void clearCompanies() {
        companies.clear();
    }

    public void setName(String name) {
        this.name = name;
    }
}
