package ru.trade_manager.companies;

public enum PriceListType {
    RETAIL,
    WHOLESALE,
    PROCUREMENT
}
