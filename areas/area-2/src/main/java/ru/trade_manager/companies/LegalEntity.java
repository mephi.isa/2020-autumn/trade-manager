package ru.trade_manager.companies;

import java.util.Objects;

public class LegalEntity {
    private int id;
    private String name;
    private String phone;
    private String email;
    private String address;
    private String inn;
    public LegalEntityRole legalEntityRole;

    public LegalEntity(int id, String name, String phone, String email, String address, String inn, LegalEntityRole legalEntityRole) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.inn = inn;
        this.legalEntityRole = legalEntityRole;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LegalEntity that = (LegalEntity) o;
        return name.equals(that.name) &&
                phone.equals(that.phone) &&
                email.equals(that.email) &&
                address.equals(that.address) &&
                inn.equals(that.inn) &&
                legalEntityRole == that.legalEntityRole;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, phone, email, address, inn, legalEntityRole);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }
}

