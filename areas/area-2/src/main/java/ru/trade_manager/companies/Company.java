package ru.trade_manager.companies;

import java.util.HashSet;
import java.util.Objects;

public class Company {

    class Department {
        private int id;
        private String name;
        private String address;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Department(String name, String address) {
            this.name = name;
            this.address = address;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Department that = (Department) o;
            return id == that.id &&
                    Objects.equals(name, that.name) &&
                    Objects.equals(address, that.address);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, name, address);
        }
    }

    Department createDepartment(String name, String address) {
        return new Department(name, address);
    }
    private int id;
    private String name;
    private String phone;
    private String address;
    private String email;
    private String inn;
    private CompanyType companyType;
    private HashSet<Department> departments = new HashSet<>();
    private HashSet<LegalEntity> legalEntities = new HashSet<>();
    private HashSet<PriceList> priceLists = new HashSet<>();

    public Company(int id, String name, String phone, String address, String email, String inn, CompanyType companyType) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.inn = inn;
        this.companyType = companyType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return name.equals(company.name) &&
                phone.equals(company.phone) &&
                address.equals(company.address) &&
                email.equals(company.email) &&
                inn.equals(company.inn) &&
                companyType == company.companyType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, phone, address, email, inn);
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public void setCompanyType(CompanyType companyType) {
        this.companyType = companyType;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInn() {
        return inn;
    }

    public CompanyType getCompanyType() {
        return companyType;
    }

    public HashSet<Department> getDepartments() {
        return departments;
    }

    public void addDepartment(Department department) {
        departments.add(department);
    }

    public void deleteDepartment(Department department) {
        departments.remove(department);
    }

    public HashSet<LegalEntity> getLegalEntities() {
        return legalEntities;
    }

    public void addLegalEntity(LegalEntity legalEntity) {
        legalEntities.add(legalEntity);
    }

    public void deleteLegalEntity(LegalEntity legalEntity) {
        legalEntities.remove(legalEntity);
    }

    public HashSet<PriceList> getPriceLists() {
        return priceLists;
    }

    public void addPriceList(PriceList priceList) {
        priceLists.add(priceList);
    }

    public void deletePriceList(PriceList priceList) {
        priceLists.remove(priceList);
    }
}
