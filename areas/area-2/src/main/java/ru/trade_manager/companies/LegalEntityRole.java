package ru.trade_manager.companies;

public enum LegalEntityRole {
    SUPPLIER,
    CUSTOMER
}
