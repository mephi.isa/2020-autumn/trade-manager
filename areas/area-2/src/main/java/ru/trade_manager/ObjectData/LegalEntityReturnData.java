package ru.trade_manager.ObjectData;

import ru.trade_manager.companies.LegalEntity;
import ru.trade_manager.companies.LegalEntityRole;

public class LegalEntityReturnData {
    public String name;
    public String phone;
    public String email;
    public String address;
    public String inn;
    public LegalEntityRole legalEntityRole;

    public LegalEntityReturnData(LegalEntity legalEntity) {
        this.address = legalEntity.getAddress();
        this.phone = legalEntity.getPhone();
        this.name = legalEntity.getName();
        this.inn = legalEntity.getInn();
        this.email = legalEntity.getEmail();
        this.legalEntityRole = legalEntity.legalEntityRole;
    }
}
