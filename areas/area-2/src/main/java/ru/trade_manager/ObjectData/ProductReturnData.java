package ru.trade_manager.ObjectData;

import ru.trade_manager.products.Product;

public class ProductReturnData {
    public String name;
    public String description;
    public int productCode;
    public String barcode;

    public ProductReturnData(Product product) {
        this.name = product.getName();
        this.description = product.getDescription();
        this.productCode = product.getProductCode();
        this.barcode = product.getBarcode();
    }
}
