package ru.trade_manager.ObjectData;

import java.util.ArrayList;
import java.util.List;

public class ProductCatalogData {
    public String name;
    public List<Integer> categories = new ArrayList<>();

    public ProductCatalogData(String name, ArrayList<Integer> productCategories) {
        this.name = name;
        this.categories = productCategories;
    }

    public ProductCatalogData(String name) {
        this.name = name;
    }

    public ProductCatalogData() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Integer> categories) {
        this.categories = categories;
    }
}
