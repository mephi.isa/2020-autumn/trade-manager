package ru.trade_manager.ObjectData;

import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.CompanyType;
import ru.trade_manager.companies.LegalEntity;
import ru.trade_manager.companies.PriceList;

import java.util.HashSet;

public class CompanyReturnData {
    public String name;
    public String phone;
    public String address;
    public String email;
    public String inn;
    public CompanyType companyType;
    public HashSet<LegalEntityReturnData> legalEntities = new HashSet<>();
    public HashSet<PriceListReturnData> priceLists = new HashSet<>();

    public CompanyReturnData(Company company) {
        this.name = company.getName();
        this.phone = company.getPhone();
        this.address = company.getAddress();
        this.email = company.getEmail();
        this.inn = company.getInn();
        this.companyType = company.getCompanyType();

        for (LegalEntity legalEntity : company.getLegalEntities()) {
            this.legalEntities.add(new LegalEntityReturnData(legalEntity));
        }

        for (PriceList priceList : company.getPriceLists()) {
            this.priceLists.add(new PriceListReturnData(priceList));
        }
    }
}
