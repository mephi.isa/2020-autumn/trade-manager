package ru.trade_manager.ObjectData;

import ru.trade_manager.products.Product;
import ru.trade_manager.products.ProductCategory;

import java.util.HashSet;

public class ProductCategoryReturnData {
    public String name;
    public HashSet<ProductReturnData> products = new HashSet<>();

    public ProductCategoryReturnData(ProductCategory productCategory) {
        this.name = productCategory.getName();

        for (Product product : productCategory.getProducts()) {
            this.products.add(new ProductReturnData(product));
        }
    }

}
