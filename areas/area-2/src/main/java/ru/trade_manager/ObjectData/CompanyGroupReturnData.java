package ru.trade_manager.ObjectData;

import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.CompanyGroup;

import java.util.HashSet;


public class CompanyGroupReturnData {
    public String name;
    public HashSet<CompanyReturnData> companies = new HashSet<>();


    public CompanyGroupReturnData(CompanyGroup companyGroup) {
        this.name = companyGroup.getName();
        for (Company company : companyGroup.getCompanies()) {
            this.companies.add(new CompanyReturnData(company));
        }
    }
}
