package ru.trade_manager.ObjectData;

import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.CompanyType;
import ru.trade_manager.companies.LegalEntity;
import ru.trade_manager.companies.PriceList;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


public class CompanyData {
    public String name;
    public String phone;
    public String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public CompanyType getCompanyType() {
        return companyType;
    }

    public void setCompanyType(CompanyType companyType) {
        this.companyType = companyType;
    }

    public HashSet<Integer> getDepartments() {
        return departments;
    }

    public void setDepartments(HashSet<Integer> departments) {
        this.departments = departments;
    }

    public List<Integer> getLegalEntities() {
        return legalEntities;
    }

    public void setLegalEntities(ArrayList<Integer> legalEntities) {
        this.legalEntities = legalEntities;
    }

    public List<Integer> getPriceLists() {
        return priceLists;
    }

    public void setPriceLists(ArrayList<Integer> priceLists) {
        this.priceLists = priceLists;
    }

    public String email;
    public String inn;
    public CompanyType companyType;
    public HashSet<Integer> departments = new HashSet<>();
    public List<Integer> legalEntities = new ArrayList<>();
    public List<Integer> priceLists = new ArrayList<>();


    public CompanyData(String name, String phone, String address, String email, String inn, CompanyType companyType) {
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.inn = inn;
        this.companyType = companyType;
    }

    public CompanyData(String email) {
        this.email = email;
    }

    public CompanyData(String name, String phone, String address, String email, String inn, CompanyType companyType,
                       HashSet<Integer> departments, ArrayList<Integer> legalEntities, ArrayList<Integer> priceLists) {
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.inn = inn;
        this.companyType = companyType;
        this.priceLists = priceLists;
        this.departments = departments;
        this.legalEntities = legalEntities;
    }

    public CompanyData(Company company) {
        this.name = company.getName();
        this.phone = company.getPhone();
        this.address = company.getAddress();
        this.email = company.getEmail();
        this.inn = company.getInn();
        this.companyType = company.getCompanyType();


        for (LegalEntity legalEntity : company.getLegalEntities()) {
            this.legalEntities.add(legalEntity.getId());
        }

        for (PriceList priceList : company.getPriceLists()) {
            this.priceLists.add(priceList.getId());
        }
    }

    public CompanyData() {

    }
}
