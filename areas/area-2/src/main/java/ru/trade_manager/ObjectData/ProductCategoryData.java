package ru.trade_manager.ObjectData;

import ru.trade_manager.products.Product;
import ru.trade_manager.products.ProductCategory;

import java.util.ArrayList;
import java.util.List;

public class ProductCategoryData {
    public String name;
    public List<Integer> products = new ArrayList<>();

    public ProductCategoryData(String name) {
        this.name = name;
    }

    public ProductCategoryData(String name, ArrayList<Integer> products) {
        this.name = name;
        this.products = products;
    }

    public ProductCategoryData(ProductCategory productCategory) {
        this.name = productCategory.getName();

        for (Product product : productCategory.getProducts()) {
            this.products.add(product.getId());
        }
    }

    public ProductCategoryData(ArrayList<Integer> products) {
        this.products = products;
    }

    public ProductCategoryData() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Integer> products) {
        this.products = products;
    }
}
