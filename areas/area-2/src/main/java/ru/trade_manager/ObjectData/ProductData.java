package ru.trade_manager.ObjectData;

import ru.trade_manager.products.Product;

public class ProductData {
    public String name;
    public String description = "";
    public int productCode;
    public String barcode;

    public ProductData(String name, int productCode, String barcode) {
        this.name = name;
        this.productCode = productCode;
        this.barcode = barcode;
    }

    public ProductData(String name, String description, int productCode, String barcode) {
        this.name = name;
        this.description = description;
        this.productCode = productCode;
        this.barcode = barcode;
    }

    public ProductData(Product product) {
        this.name = product.getName();
        this.description = product.getDescription();
        this.productCode = product.getProductCode();
        this.barcode = product.getBarcode();
    }

    public ProductData() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
