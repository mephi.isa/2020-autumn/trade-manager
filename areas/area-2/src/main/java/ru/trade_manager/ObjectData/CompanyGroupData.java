package ru.trade_manager.ObjectData;

import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.CompanyGroup;

import java.util.ArrayList;
import java.util.List;


public class CompanyGroupData {
    public String name;
    public List<Integer> companies = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Integer> companies) {
        this.companies = companies;
    }

    public CompanyGroupData(String name, ArrayList<Integer> companies) {
        this.name = name;
        this.companies = companies;
    }

    public CompanyGroupData(CompanyGroup companyGroup) {
        this.name = companyGroup.getName();
        for (Company company : companyGroup.getCompanies()) {
            this.companies.add(company.getId());
        }
    }

    public CompanyGroupData(ArrayList<Integer> companies) {
        this.companies = companies;
    }

    public CompanyGroupData(String name) {
        this.name = name;
    }


    public CompanyGroupData() {

    }
}
