package ru.trade_manager.ObjectData;

import ru.trade_manager.products.ProductCatalog;
import ru.trade_manager.products.ProductCategory;

import java.util.HashSet;

public class ProductCatalogReturnData {
    public String name;
    public HashSet<ProductCategoryReturnData> categories = new HashSet<>();

    public ProductCatalogReturnData(ProductCatalog productCatalog) {
        this.name = productCatalog.getName();

        for (ProductCategory productCategory : productCatalog.getCategories()) {
            this.categories.add(new ProductCategoryReturnData(productCategory));
        }
    }

}
