package ru.trade_manager.API;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.trade_manager.ObjectData.*;


@Controller
public class Frontend {
    private final Network api = new Network();

    @GetMapping("/")
    public String indexForm(Model model) {
        return "index";
    }

    @GetMapping("/create_company")
    public String createCompanyForm(Model model) {
        model.addAttribute("company", new CompanyData());
        return "create_company";
    }

    @PostMapping("/create_company")
    public String createCompanySubmit(@ModelAttribute CompanyData company, Model model) {
        int resultID = api.createCompany(company);
        model.addAttribute("company", api.getCompany(resultID));
        return "success_company";
    }

    @GetMapping("/get_companies")
    public String getCompaniesForm(Model model) {
        model.addAttribute("companies", api.getCompanies());
        return "get_companies";
    }

    @GetMapping("/edit_company")
    public String editCompaniesForm(Model model) {
        return "edit_company";
    }

    @GetMapping("/update_company/{companyId}")
    public String updateCompaniesForm(@PathVariable int companyId, Model model) {
        model.addAttribute("company", api.getCompany(companyId));
        return "update_company";
    }

    @PostMapping("/update_company/{companyId}")
    public String updateCompaniesSubmit(@PathVariable int companyId, @ModelAttribute CompanyData company, Model model) {
        api.updateCompany(companyId, company);
        model.addAttribute("company", api.getCompany(companyId));
        return "success_company";
    }

    @GetMapping("/delete_company")
    public String deleteCompaniesForm(Model model) {
        return "delete_company";
    }

    @GetMapping("/delete_company/{companyId}")
    public String deleteCompaniesSubmit(@PathVariable int companyId, Model model) {
        api.deleteCompany(companyId);
        return "success_delete_company";
    }


    // group
    @GetMapping("/create_companygroup")
    public String createCompanyGroupForm(Model model) {
        model.addAttribute("company", new CompanyGroupData());
        return "create_companygroup";
    }

    @PostMapping("/create_companygroup")
    public String createCompanyGroupSubmit(@ModelAttribute CompanyGroupData company, Model model) {
        int resultID = api.createCompanyGroup(company);
        model.addAttribute("company", api.getCompanyGroup(resultID));
        return "success_companygroup";
    }

    @GetMapping("/get_companiesgroup")
    public String getCompaniesGroupForm(Model model) {
        model.addAttribute("companies", api.getCompanyGroups());
        return "get_companiesgroup";
    }

    @GetMapping("/edit_companygroup")
    public String editCompaniesGroupForm(Model model) {
        return "edit_companygroup";
    }

    @GetMapping("/update_companygroup/{companyId}")
    public String updateCompaniesGroupForm(@PathVariable int companyId, Model model) {
        model.addAttribute("company", api.getCompanyGroup(companyId));
        return "update_companygroup";
    }

    @PostMapping("/update_companygroup/{companyId}")
    public String updateCompaniesGroupSubmit(@PathVariable int companyId, @ModelAttribute CompanyGroupData company, Model model) {
        api.updateCompanyGroup(companyId, company);
        model.addAttribute("company", api.getCompanyGroup(companyId));
        return "success_companygroup";
    }

    @GetMapping("/delete_companygroup")
    public String deleteCompaniesGroupForm(Model model) {
        return "delete_companygroup";
    }

    @GetMapping("/delete_companygroup/{companyId}")
    public String deleteCompaniesGroupSubmit(@PathVariable int companyId, Model model) {
        api.deleteCompanyGroup(companyId);
        return "success_delete_companygroup";
    }

    // product
    @GetMapping("/create_product")
    public String createProductForm(Model model) {
        model.addAttribute("product", new ProductData());
        return "create_product";
    }

    @PostMapping("/create_product")
    public String createProductSubmit(@ModelAttribute ProductData product, Model model) {
        int resultID = api.createProduct(product);
        model.addAttribute("product", api.getProduct(resultID));
        return "success_product";
    }

    @GetMapping("/get_products")
    public String getProductForm(Model model) {
        model.addAttribute("products", api.getProducts());
        return "get_products";
    }

    @GetMapping("/edit_product")
    public String editProductForm(Model model) {
        return "edit_product";
    }

    @GetMapping("/update_product/{productID}")
    public String updateProductForm(@PathVariable int productID, Model model) {
        model.addAttribute("product", api.getProduct(productID));
        return "update_product";
    }

    @PostMapping("/update_product/{productID}")
    public String updateProductSubmit(@PathVariable int productID, @ModelAttribute ProductData product, Model model) {
        api.updateProduct(productID, product);
        model.addAttribute("product", api.getProduct(productID));
        return "success_product";
    }

    @GetMapping("/delete_product")
    public String deleteProductForm(Model model) {
        return "delete_product";
    }

    @GetMapping("/delete_product/{productID}")
    public String deleteProductSubmit(@PathVariable int productID, Model model) {
        api.deleteProduct(productID);
        return "success_delete_product";
    }

    // product category
    @GetMapping("/create_productcategory")
    public String createProductCatecoryForm(Model model) {
        model.addAttribute("product", new ProductCategoryData());
        return "create_productcategory";
    }

    @PostMapping("/create_productcategory")
    public String createProductCategorySubmit(@ModelAttribute ProductCategoryData productCategory, Model model) {
        int resultID = api.createProductCategory(productCategory);
        model.addAttribute("product", api.getProductCategory(resultID));
        return "success_productcategory";
    }

    @GetMapping("/get_productscategories")
    public String getProductCategoryForm(Model model) {
        model.addAttribute("products", api.getProductsCategories());
        return "get_productscategories";
    }

    @GetMapping("/edit_productcategory")
    public String editProductCategoryForm(Model model) {
        return "edit_productcategory";
    }

    @GetMapping("/update_productcategory/{productCategoryID}")
    public String updateProductCategoryForm(@PathVariable int productCategoryID, Model model) {
        model.addAttribute("product", api.getProductCategory(productCategoryID));
        return "update_productcategory";
    }

    @PostMapping("/update_productcategory/{productCategoryID}")
    public String updateCompaniesGroupSubmit(@PathVariable int productCategoryID, @ModelAttribute ProductCategoryData productCategoryData, Model model) {
        api.updateProductCategory(productCategoryID, productCategoryData);
        model.addAttribute("product", api.getProductCategory(productCategoryID));
        return "success_productcategory";
    }

    @GetMapping("/delete_productcategory")
    public String deleteProductCategoryForm(Model model) {
        return "delete_productcategory";
    }

    @GetMapping("/delete_productcategory/{productCategoryID}")
    public String deleteProductCategorySubmit(@PathVariable int productCategoryID, Model model) {
        api.deleteProductCategory(productCategoryID);
        return "success_delete_productcategory";
    }

    // product catalog
    @GetMapping("/create_productcatalog")
    public String createProductCatalogForm(Model model) {
        model.addAttribute("product", new ProductCatalogData());
        return "create_productcatalog";
    }

    @PostMapping("/create_productcatalog")
    public String createProductCatalogSubmit(@ModelAttribute ProductCatalogData productCatalogData, Model model) {
        int resultID = api.createProductCatalog(productCatalogData);
        model.addAttribute("product", api.getProductCatalog(resultID));
        return "success_productcatalog";
    }

    @GetMapping("/get_productcatalog")
    public String getProductCatalogForm(Model model) {
        model.addAttribute("products", api.getProductsCatalogs());
        return "get_productscatalog";
    }
}
