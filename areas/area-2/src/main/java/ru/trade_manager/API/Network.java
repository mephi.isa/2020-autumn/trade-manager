package ru.trade_manager.API;

import org.springframework.web.bind.annotation.*;
import ru.trade_manager.ObjectData.*;
import ru.trade_manager.UseCases.*;
import ru.trade_manager.companies.Company;
import ru.trade_manager.companies.CompanyGroup;
import ru.trade_manager.companies.LegalEntity;
import ru.trade_manager.companies.PriceList;
import ru.trade_manager.interfaces.NetworkI;
import ru.trade_manager.interfaces.UserAuthRepositoryI;
import ru.trade_manager.mock.Storage;
import ru.trade_manager.products.Product;
import ru.trade_manager.products.ProductCatalog;
import ru.trade_manager.products.ProductCategory;
import ru.trade_manager.user.UserRole;

import java.util.AbstractMap;
import java.util.List;


@RestController
public class Network implements NetworkI {
    private static class UserAuthRepository implements UserAuthRepositoryI {
        @Override
        public void assertRole(long id, UserRole... role) throws RuntimeException {
            if (id != 1) {
                throw new RuntimeException("Wrong role");
            }
        }
    }

    private final Storage storage = new Storage();
    private final UserAuthRepository userAuthRepository = new UserAuthRepository();

    @PostMapping("/createCompany")
    @Override
    public int createCompany(@RequestBody CompanyData company){
        CreateCompany usecase = new CreateCompany(storage, userAuthRepository);
        return usecase.run(1, company);
    }

    @GetMapping("/getCompany/{companyId}")
    @Override
    public Company getCompany(@PathVariable int companyId) {
        return storage.getCompany(companyId);
    }

    @GetMapping("/getCompanyData/{companyId}")
    @Override
    public CompanyData getCompanyData(@PathVariable int companyId) {
        UpdateCompany usecase = new UpdateCompany(storage, userAuthRepository);
        return usecase.getCompany(1, companyId);
    }

    @GetMapping("/getCompanies")
    @Override
    public List<CompanyReturnData> getCompanies() {
        GetAllCompanies usecase = new GetAllCompanies(this.storage);
        return usecase.run();
    }

    @PatchMapping("/updateCompany/{companyId}")
    @Override
    public void updateCompany(@PathVariable int companyId, @RequestBody CompanyData company) {
        UpdateCompany usecase = new UpdateCompany(this.storage, userAuthRepository);
        usecase.run(1, new AbstractMap.SimpleEntry<>(companyId, company));
    }

    @DeleteMapping("/deleteCompany/{companyId}")
    @Override
    public void deleteCompany(@PathVariable int companyId) {
        DeleteCompany usecase = new DeleteCompany(storage, userAuthRepository);
        usecase.run(1, companyId);
    }


    @PostMapping("/createCompanyGroup")
    @Override
    public int createCompanyGroup(@RequestBody CompanyGroupData company) {
        CreateCompanyGroup usecase = new CreateCompanyGroup(storage, userAuthRepository);
        return usecase.run(1, company);
    }

    @GetMapping("/getCompanyGroups/{companyGroupId}")
    @Override
    public CompanyGroup getCompanyGroup(@PathVariable int companyGroupId) {
        return storage.getCompanyGroups(companyGroupId);
    }

    @GetMapping("/getCompanyGroupData/{companyId}")
    @Override
    public CompanyGroupData getCompanyGroupData(@PathVariable int companyId) {
        UpdateCompanyGroup usecase = new UpdateCompanyGroup(storage, userAuthRepository);
        return usecase.getCompanyGroup(1, companyId);
    }

    @GetMapping("/getCompanyGroups")
    @Override
    public List<CompanyGroupReturnData> getCompanyGroups() {
        GetAllCompaniesGroups usecase = new GetAllCompaniesGroups(this.storage);
        return usecase.run();
    }

    @PatchMapping("/updateCompanyGroup/{companyId}")
    @Override
    public void updateCompanyGroup(@PathVariable int companyId, @RequestBody CompanyGroupData company) {
        UpdateCompanyGroup usecase = new UpdateCompanyGroup(this.storage, userAuthRepository);
        usecase.run(1, new AbstractMap.SimpleEntry<>(companyId, company));
    }

    @DeleteMapping("/deleteCompanyGroup/{company_id}")
    @Override
    public void deleteCompanyGroup(@PathVariable int company_id) {
        DeleteCompanyGroup usecase = new DeleteCompanyGroup(this.storage, this.userAuthRepository);
        usecase.run(1, company_id);
    }

    @PostMapping("/createProduct")
    @Override
    public int createProduct(@RequestBody ProductData product) {
        CreateProduct usecase = new CreateProduct(this.storage, this.userAuthRepository);
        return usecase.run(1, product);
    }

    @GetMapping("/getProduct/{productId}")
    @Override
    public Product getProduct(@PathVariable int productId) {
        return this.storage.getProduct(productId);
    }

    @GetMapping("/getProductData/{productId}")
    @Override
    public ProductData getProductData(@PathVariable int productId) {
        UpdateProduct usecase = new UpdateProduct(storage, userAuthRepository);
        return usecase.getProduct(1, productId);
    }

    @GetMapping("/getProducts")
    @Override
    public List<ProductReturnData> getProducts() {
        GetAllProducts usecase = new GetAllProducts(this.storage);
        return usecase.run();
    }

    @PatchMapping("/updateProduct/{productId}")
    @Override
    public void updateProduct(@PathVariable int productId, @RequestBody ProductData product) {
        UpdateProduct usecase = new UpdateProduct(this.storage, userAuthRepository);
        usecase.run(1, new AbstractMap.SimpleEntry<>(productId, product));
    }

    @DeleteMapping("/deleteProduct/{product_id}")
    @Override
    public void deleteProduct(@PathVariable int product_id) {
        DeleteProduct usecase = new DeleteProduct(this.storage, this.userAuthRepository);
        usecase.run(1, product_id);
    }

    @PostMapping("/createProductCategory")
    @Override
    public int createProductCategory(@RequestBody ProductCategoryData productCategory) {
        CreateProductCategory usecase = new CreateProductCategory(this.storage, this.userAuthRepository);
        return usecase.run(1, productCategory);
    }

    @GetMapping("/getProductCategory/{productCategoryId}")
    @Override
    public ProductCategory getProductCategory(@PathVariable int productCategoryId) {
        return this.storage.getProductCategory(productCategoryId);
    }

    @GetMapping("/getProductCategoryData/{productCategoryId}")
    @Override
    public ProductCategoryData getProductCategoryData(@PathVariable int productCategoryId) {
        UpdateProductCategory usecase = new UpdateProductCategory(storage, userAuthRepository);
        return usecase.getProductCategory(1, productCategoryId);
    }

    @GetMapping("/getProductsCategories")
    @Override
    public List<ProductCategoryReturnData> getProductsCategories() {
        GetAllProductCategory usecase = new GetAllProductCategory(this.storage);
        return usecase.run();
    }

    @PatchMapping("/updateProductCategory/{productCategoryId}")
    @Override
    public void updateProductCategory(@PathVariable int productCategoryId, @RequestBody ProductCategoryData productCategory) {
        UpdateProductCategory usecase = new UpdateProductCategory(this.storage, userAuthRepository);
        usecase.run(1, new AbstractMap.SimpleEntry<>(productCategoryId, productCategory));
    }

    @DeleteMapping("/deleteProductCategory/{productCategory_id}")
    @Override
    public void deleteProductCategory(@PathVariable int productCategory_id) {
        DeleteProductCategory usecase = new DeleteProductCategory(this.storage, this.userAuthRepository);
        usecase.run(1, productCategory_id);
    }

    @PostMapping("/createProductCatalog")
    @Override
    public int createProductCatalog(@RequestBody ProductCatalogData productCatalog) {
        CreateProductCatalog usecase = new CreateProductCatalog(this.storage, this.userAuthRepository);
        return usecase.run(1, productCatalog);
    }

    @GetMapping("/getProductCatalog/{productCategoryId}")
    @Override
    public ProductCatalog getProductCatalog(@PathVariable int productCategoryId) {
        return this.storage.getProductCatalog(productCategoryId);
    }

    @GetMapping("/getProductsCatalogs")
    @Override
    public List<ProductCatalogReturnData> getProductsCatalogs() {
        GetAllProductCatalog usecase = new GetAllProductCatalog(this.storage);
        return usecase.run();
    }

    @Override
    public PriceList getPriceList(int priceListId) {
        return this.storage.getPriceList(priceListId);
    }

    @Override
    public LegalEntity getLegalEntity(int legalEntityId) {
        return this.storage.getLegalEntity(legalEntityId);
    }

}

