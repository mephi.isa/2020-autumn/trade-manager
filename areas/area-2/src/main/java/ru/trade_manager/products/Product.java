package ru.trade_manager.products;

import java.util.Objects;

public class Product {
    private int id;
    private String name;
    private String description = "";
    private int productCode;
    private String barcode;

    public Product(int id, String name, int productCode, String barcode) {
        this.id = id;
        this.name = name;
        this.productCode = productCode;
        this.barcode = barcode;
    }

    public Product(int id, String name, String description, int productCode, String barcode) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.productCode = productCode;
        this.barcode = barcode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productCode == product.productCode &&
                Objects.equals(name, product.name) &&
                Objects.equals(barcode, product.barcode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, productCode, barcode);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
