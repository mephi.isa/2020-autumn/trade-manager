package ru.trade_manager.products;

import java.util.HashSet;
import java.util.Objects;

public class ProductCatalog {
    private int id;
    private String name;
    private HashSet<ProductCategory> categories = new HashSet<>();

    public ProductCatalog(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public ProductCatalog(int id, String name, HashSet<ProductCategory> categories) {
        this.id = id;
        this.name = name;
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductCatalog that = (ProductCatalog) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public HashSet<ProductCategory> getCategories() {
        return categories;
    }

    public void setCategories(HashSet<ProductCategory> categories) {
        this.categories = categories;
    }

    public void addProductCategory(ProductCategory productCategory) {
        categories.add(productCategory);
    }

    public void deleteProductCategory(ProductCategory productCategory) {
        categories.remove(productCategory);
    }
    public void clearCategories() {
        categories.clear();
    }

}
