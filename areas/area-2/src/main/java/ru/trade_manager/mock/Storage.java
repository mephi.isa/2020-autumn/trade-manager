package ru.trade_manager.mock;

import ru.trade_manager.companies.*;
import ru.trade_manager.interfaces.StorageI;
import ru.trade_manager.products.Product;
import ru.trade_manager.products.ProductCatalog;
import ru.trade_manager.products.ProductCategory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Storage implements StorageI {
    private final List<Company> companies = new ArrayList<>();
    private final List<CompanyGroup> companyGroups = new ArrayList<>();
    private final List<Product> products = new ArrayList<>();
    private final List<ProductCategory> productCategories = new ArrayList<>();
    private final List<ProductCatalog> productCatalogs = new ArrayList<>();
    private final List<PriceList> priceLists = new ArrayList<>();
    private final List<LegalEntity> legalEntities = new ArrayList<>();

    public Storage() {

        Company company = new Company(0, "name", "8800", "Moscow", "test@test.ru", "123", CompanyType.INDIVIDUAL_ENTREPRENEUR);
        this.createCompany(company);
        //        this.departmentID = this.storage.createDepartment((new Department("name", "address")));
        this.createLegalEntity(new LegalEntity(0, "name", "8800", "Moscow", "test@test.ru", "123", LegalEntityRole.CUSTOMER));
        this.createPriceList(new PriceList(0, "new", PriceListType.PROCUREMENT));
        Product firstProductData = new Product(0, "Beer1", "Baltika", 27, "");
        Product secondProductData = new Product(1, "Fish2", "Gum", 28, "");

        HashSet<Product> firstProducts = new HashSet<>();
        firstProducts.add(firstProductData);

        HashSet<Product> secondProducts = new HashSet<>();
        secondProducts.add(secondProductData);

        ProductCategory firstProductCategory = new ProductCategory(0, "FreshMeat", firstProducts);
        ProductCategory secondProductCategory = new ProductCategory(1, "FreshMilk", secondProducts);
        ProductCategory theardProductCategory = new ProductCategory(2, "FreshMil1k");
        this.createProductCategory(theardProductCategory);
        HashSet<ProductCategory> firstCatalog = new HashSet<>();
        firstCatalog.add(firstProductCategory);

        HashSet<ProductCategory> secondCatalog = new HashSet<>();
        secondCatalog.add(secondProductCategory);

        ProductCatalog firstProductCatalog = new ProductCatalog(0, "123", firstCatalog);
        ProductCatalog secondProductCatalog = new ProductCatalog(1, "1123", secondCatalog);
        this.createProductCatalog(firstProductCatalog);
        this.createProductCatalog(secondProductCatalog);

        CompanyGroup firstCompanyGroup = new CompanyGroup(0, "firstCompanyGroup");
        CompanyGroup secondCompanyGroup = new CompanyGroup(1, "secondCompanyGroup");
        this.createCompanyGroup(firstCompanyGroup);
        this.createCompanyGroup(secondCompanyGroup);

        Company firstCompany = new Company(1, "name1", "8800", "Moscow", "test@test.ru",
                "123456789123", CompanyType.INDIVIDUAL_ENTREPRENEUR);
        Company secondCompany = new Company(2, "1name_second", "8800", "Moscow", "test@test.ru",
                "123456789765", CompanyType.INDIVIDUAL_ENTREPRENEUR);
        this.createCompany(firstCompany);
        this.createCompany(secondCompany);

        Product thirdProductData = new Product(2, "Beer1", "Baltika", 29, "2");
        Product fourProductData = new Product(3, "Fish2", "Gum", 30, "3");

        HashSet<Product> thirdProducts = new HashSet<>();
        thirdProducts.add(thirdProductData);

        HashSet<Product> fourProducts = new HashSet<>();
        fourProducts.add(fourProductData);

        ProductCategory thirdProductCategory = new ProductCategory(3, "FreshMeatNew", thirdProducts);
        ProductCategory fourProductCategory = new ProductCategory(4, "FreshMilkNew", fourProducts);
        this.createProductCategory(thirdProductCategory);
        this.createProductCategory(fourProductCategory);
        this.createProduct(new Product(4, "BeerNewNew", "BaltikaNew", 31, ""));
        this.createProduct(new Product(5, "FishNewNew", "GumNew", 32, ""));

        this.createCompany(new Company(3, "name", "8800", "Moscow", "test@test.ru", "123", CompanyType.INDIVIDUAL_ENTREPRENEUR));
        this.createCompanyGroup(new CompanyGroup(2, "nametest"));
        this.createCompany(new Company(4, "name", "8800", "Moscow", "test@test.ru",
                "123456789000", CompanyType.INDIVIDUAL_ENTREPRENEUR));
        Product sixProductData = new Product(6, "Beer", "Baltika", 277, "");
        Product sevenProductData = new Product(7, "Fish", "Gum", 2823, "");
        this.createProduct(sevenProductData);

        HashSet<Product> sixProducts = new HashSet<>();
        sixProducts.add(sixProductData);
        this.createProductCategory(new ProductCategory(5, "Meat", sixProducts));
        this.createProduct(new Product(8, "Fish", "Gum", 28, ""));
    }

    @Override
    public int createCompany(Company company) {
        if (this.getCompany(company.getId()) != null) {
            throw new RuntimeException("Company this this id already exists");
        }
        this.companies.add(company);
        for (LegalEntity legalEntity : company.getLegalEntities()) {
            if (!this.legalEntities.contains(legalEntity)) {
                this.createLegalEntity(legalEntity);
            }
        }
        for (PriceList priceList : company.getPriceLists()) {
            if (!this.priceLists.contains(priceList)) {
                this.createPriceList(priceList);
            }
        }
        return company.getId();
    }

    @Override
    public Company getCompany(int companyId) {
        for (Company company : this.companies) {
            if (company.getId() == companyId) {
                return company;
            }
        }
        return null;
    }

    @Override
    public List<Company> getCompanies() {
        return this.companies;
    }

    @Override
    public void updateCompany(Company company) {
        for (int i = 0; i < this.companies.size(); i++) {
            if (this.companies.get(i).getId() == company.getId()) {
                this.companies.set(i, company);
                return;
            }
        }
    }

    @Override
    public void deleteCompany(Company company) {
        this.companies.remove(company);
    }

    @Override
    public int getInnCount(String inn) {
        int count = 0;
        for (Company company : this.companies) {
            if (company.getInn().equals(inn)) {
                count += 1;
            }
        }
        return count;
    }

    @Override
    public int createCompanyGroup(CompanyGroup companyGroup) {
        if (this.getCompanyGroups(companyGroup.getId()) != null) {
            throw new RuntimeException("companyGroup this this id already exists");
        }
        this.companyGroups.add(companyGroup);
        for (Company company : companyGroup.getCompanies()) {
            if (!this.companies.contains(company)) {
                this.companies.add(company);
            }
        }
        return companyGroup.getId();
    }

    @Override
    public CompanyGroup getCompanyGroups(int companyGroupId) {
        for (CompanyGroup companyGroup : this.companyGroups) {
            if (companyGroup.getId() == companyGroupId) {
                return companyGroup;
            }
        }
        return null;

    }

    @Override
    public List<CompanyGroup> getCompanyGroup() {
        return this.companyGroups;
    }

    @Override
    public void updateCompanyGroup(CompanyGroup companyGroup) {
        for (int i = 0; i < this.companyGroups.size(); i++) {
            if (this.companyGroups.get(i).getId() == companyGroup.getId()) {
                this.companyGroups.set(i, companyGroup);
                return;
            }
        }
    }

    @Override
    public void deleteCompanyGroup(CompanyGroup companyGroup) {
        this.companyGroups.remove(companyGroup);
    }

    @Override
    public int createProduct(Product product) {
        if (this.getProduct(product.getId()) != null) {
            throw new RuntimeException("Product this this id already exists");
        }
        this.products.add(product);
        return product.getId();
    }

    @Override
    public Product getProduct(int productId) {
        for (Product product : this.products) {
            if (product.getId() == productId) {
                return product;
            }
        }
        return null;

    }

    @Override
    public int getProductCodeCount(int productCode) {
        int count = 0;
        for (Product product : this.products) {
            if (product.getProductCode() == productCode) {
                count += 1;
            }
        }
        return count;
    }

    @Override
    public List<Product> getProducts() {
        return this.products;
    }

    @Override
    public void updateProduct(Product product) {
        for (int i = 0; i < this.products.size(); i++) {
            if (this.products.get(i).getId() == product.getId()) {
                this.products.set(i, product);
                return;
            }
        }
    }

    @Override
    public void deleteProduct(Product product) {
        this.products.remove(product);
    }

    @Override
    public int createProductCategory(ProductCategory productCategory) {
        if (this.getProductCategory(productCategory.getId()) != null) {
            throw new RuntimeException("productCategory this this id already exists");
        }
        this.productCategories.add(productCategory);
        for (Product product : productCategory.getProducts()) {
            if (!this.products.contains(product)) {
                this.createProduct(product);
            }
        }
        return productCategory.getId();
    }

    @Override
    public ProductCategory getProductCategory(int productCategoryId) {
        for (ProductCategory productCategory : this.productCategories) {
            if (productCategory.getId() == productCategoryId) {
                return productCategory;
            }
        }
        return null;

    }

    @Override
    public List<ProductCategory> getProductsCategories() {
        return this.productCategories;
    }

    @Override
    public void updateProductCategory(ProductCategory productCategory) {
        for (int i = 0; i < this.productCategories.size(); i++) {
            if (this.productCategories.get(i).getId() == productCategory.getId()) {
                this.productCategories.set(i, productCategory);
                return;
            }
        }
    }

    @Override
    public void deleteProductCategory(ProductCategory productCategory) {
        this.productCategories.remove(productCategory);
    }

    @Override
    public int createProductCatalog(ProductCatalog productCatalog) {
        if (this.getProductCatalog(productCatalog.getId()) != null) {
            throw new RuntimeException("productCatalog this this id already exists");
        }
        this.productCatalogs.add(productCatalog);
        for (ProductCategory productCategory : productCatalog.getCategories()) {
            if (!this.productCategories.contains(productCategory)) {
                this.createProductCategory(productCategory);
            }
        }
        return productCatalog.getId();
    }

    @Override
    public ProductCatalog getProductCatalog(int productCatalogId) {
        for (ProductCatalog productCatalog : this.productCatalogs) {
            if (productCatalog.getId() == productCatalogId) {
                return productCatalog;
            }
        }
        return null;

    }

    @Override
    public List<ProductCatalog> getProductsCatalogs() {
        return this.productCatalogs;
    }

    @Override
    public void updateProductCatalog(ProductCatalog productCatalog) {
        for (int i = 0; i < this.productCatalogs.size(); i++) {
            if (this.productCatalogs.get(i).getId() == productCatalog.getId()) {
                this.productCatalogs.set(i, productCatalog);
                return;
            }
        }
    }

    @Override
    public void deleteProductCatalog(ProductCatalog productCatalog) {
        this.productCatalogs.remove(productCatalog);
    }


    @Override
    public int createPriceList(PriceList priceList) {
        if (this.getPriceList(priceList.getId()) != null) {
            throw new RuntimeException("priceList this this id already exists");
        }
        this.priceLists.add(priceList);
        return priceList.getId();
    }

    @Override
    public PriceList getPriceList(int priceListId) {
        for (PriceList priceList : this.priceLists) {
            if (priceList.getId() == priceListId) {
                return priceList;
            }
        }
        return null;
    }

    @Override
    public int createLegalEntity(LegalEntity legalEntity) {
        if (this.getLegalEntity(legalEntity.getId()) != null) {
            throw new RuntimeException("legalEntity this this id already exists");
        }
        this.legalEntities.add(legalEntity);
        return legalEntity.getId();
    }

    @Override
    public LegalEntity getLegalEntity(int legalEntityId) {
        for (LegalEntity legalEntity : this.legalEntities) {
            if (legalEntity.getId() == legalEntityId) {
                return legalEntity;
            }
        }
        return null;

    }
}
