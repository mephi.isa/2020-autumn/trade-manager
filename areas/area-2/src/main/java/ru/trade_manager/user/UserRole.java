package ru.trade_manager.user;

public enum UserRole {
    Admin,
    Employee
}
