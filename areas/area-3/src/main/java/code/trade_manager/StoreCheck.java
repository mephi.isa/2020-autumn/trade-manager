package code.trade_manager;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.HashSet;

public class StoreCheck {
    private final int id;
    private HashSet<Product> products = new HashSet<>();
    private Cashbox cashbox;
    private final Timestamp time;

    public StoreCheck(int id, HashSet<Product> products, Cashbox cashbox, Timestamp time) {
        this.id = id;
        this.products = products;
        this.cashbox = cashbox;
        this.time = time;
    }

    public StoreCheck(int id, Cashbox cashbox, Timestamp time) {
        this.id = id;
        this.cashbox = cashbox;
        this.time = time;
    }

    public StoreCheck(int id, Timestamp time) {
        this.id = id;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public HashSet<Product> getProducts() {
        return products;
    }

    public Cashbox getCashbox() {
        return cashbox;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setProducts(HashSet<Product> products) {
        this.products = products;
    }

    public void setCashbox(Cashbox cashbox) {
        this.cashbox = cashbox;
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void deleteProduct(Product product) {
        products.remove(product);
    }

    public void clearProducts() {
        products.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StoreCheck that = (StoreCheck) o;
        return id == that.id &&
                Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time);
    }
}
