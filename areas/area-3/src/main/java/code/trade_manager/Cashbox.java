package code.trade_manager;

import java.util.Objects;
import java.util.HashSet;

public class Cashbox{
    private String serialNumberKKT;
    private final int id;
    private HashSet<StoreCheck> checks = new HashSet<>();
    private Employee employee;

    public Cashbox(String serialNumberKKT, int id, HashSet<StoreCheck> checks, Employee employee) {
        this.serialNumberKKT = serialNumberKKT;
        this.id = id;
        this.checks = checks;
        this.employee = employee;
    }

    public Cashbox(int id) {
        this.id = id;
    }

    public Cashbox(String serialNumberKKT, int id, Employee employee) {
        this.serialNumberKKT = serialNumberKKT;
        this.id = id;
        this.employee = employee;
    }

    public String getSerialNumberKKT() {
        return serialNumberKKT;
    }

    public HashSet<StoreCheck> getChecks() {
        return checks;
    }

    public Employee getEmployee() {
        return employee;
    }

    public int getId() {
        return id;
    }

    public void setSerialNumberKKT(String serialNumberKKT) {
        this.serialNumberKKT = serialNumberKKT;
    }

    public void setChecks(HashSet<StoreCheck> checks) {
        this.checks = checks;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void addCheck(StoreCheck check) {
        checks.add(check);
    }

    public void deleteCheck(StoreCheck check) {
        checks.remove(check);
    }

    public void clearChecks() {
        checks.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cashbox cashbox = (Cashbox) o;
        return id == cashbox.id &&
                Objects.equals(serialNumberKKT, cashbox.serialNumberKKT);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serialNumberKKT, id);
    }
}
