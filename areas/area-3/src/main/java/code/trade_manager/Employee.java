package code.trade_manager;

import java.util.Objects;

public class Employee{
    private String lastName;
    private String firstName;
    private String middleName;
    private String username;
    private String password;
    private EmployeeRole role;

    public Employee(String lastName, String firstName, EmployeeRole role) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.role = role;
    }

    public Employee(String lastName, String firstName, String middleName, EmployeeRole role) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.role = role;
    }

    public Employee(String lastName, String firstName, String middleName, String username, String password, EmployeeRole role) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public EmployeeRole getRole() {
        return role;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public void setRole(EmployeeRole role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(lastName, employee.lastName) &&
                Objects.equals(firstName, employee.firstName) &&
                role == employee.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, role);
    }
}
