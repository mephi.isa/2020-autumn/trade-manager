package code.trade_manager;

import java.util.Objects;

public class Product {

    private String name;
    private int productCode;
    private String barcode;
    private float price;

    public Product(String name, int productCode, String barcode) {
        this.name = name;
        this.productCode = productCode;
        this.barcode = barcode;
    }

    public Product(String name, float price, int productCode, String barcode) {
        this.name = name;
        this.price = price;
        this.productCode = productCode;
        this.barcode = barcode;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return productCode == product.productCode &&
                Objects.equals(name, product.name) &&
                Objects.equals(barcode, product.barcode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, productCode, barcode);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getProductCode() {
        return productCode;
    }

    public void setProductCode(int productCode) {
        this.productCode = productCode;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
