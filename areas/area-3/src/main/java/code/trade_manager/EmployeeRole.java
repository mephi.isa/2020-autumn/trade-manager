package code.trade_manager;

import java.util.Objects;

public enum  EmployeeRole{
    CASHIER,
    SENIOR_CASHIER
}
