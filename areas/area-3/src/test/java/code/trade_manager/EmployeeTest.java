package code.trade_manager;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class EmployeeTest {
    private String lastName = "Chechevili";
    private String firstName = "Abraaam";
    private String middleName = "Kemberbetovich";
    private String username = "sushka";
    private String password = "pushka";
    private Employee employee;

    @Before
    public void init() {
        employee = new Employee(lastName, firstName, middleName, username, password, EmployeeRole.SENIOR_CASHIER);
    }

    @Test
    public void getLastName() {
        assertEquals(lastName, employee.getLastName());
    }

    @Test
    public void getFirstName() {
        assertEquals(firstName, employee.getFirstName());
    }

    @Test
    public void getMiddleName() {
        assertEquals(middleName, employee.getMiddleName());
    }

    @Test
    public void getRole() {
        assertEquals(EmployeeRole.SENIOR_CASHIER, employee.getRole());
    }

    @Test
    public void getUsername() {
        assertEquals(username, employee.getUsername());
    }

    @Test
    public void getPassword() {
        assertEquals(password, employee.getPassword());
    }

    @Test
    public void setLastName() {
        String lastName1 = "abc";
        employee.setLastName(lastName1);
        assertEquals(lastName1, employee.getLastName());
    }

    @Test
    public void setFirstName() {
        String firstName1 = "bca";
        employee.setFirstName(firstName1);
        assertEquals(firstName1, employee.getFirstName());
    }

    @Test
    public void setMiddleName() {
        String middleName1 = "cheche";
        employee.setMiddleName(middleName1);
        assertEquals(middleName1, employee.getMiddleName());
    }

    @Test
    public void setRole() {
        employee.setRole(EmployeeRole.CASHIER);
        assertEquals(EmployeeRole.CASHIER, employee.getRole());
    }

    @Test
    public void setUsername() {
        String username1 = "balaklava";
        employee.setUsername(username1);
        assertEquals(username1, employee.getUsername());
    }

    @Test
    public void setPassword() {
        String password1 = "cherez_plecho";
        employee.setPassword(password1);
        assertEquals(password1, employee.getPassword());
    }

    @Test
    public void positiveEquals() {
        Employee employee1 = new Employee(lastName, firstName, middleName, EmployeeRole.SENIOR_CASHIER);
        Employee employee2 = new Employee(lastName, firstName, middleName, EmployeeRole.SENIOR_CASHIER);
        assertEquals(employee1, employee2);
    }

    @Test
    public void negativeEquals() {
        Employee employee1 = new Employee(lastName, firstName, EmployeeRole.CASHIER);
        Employee employee2 = new Employee(lastName, firstName, EmployeeRole.SENIOR_CASHIER);
        assertNotEquals(employee1, employee2);
    }

    @Test
    public void testHashCode() {
        Employee employee1 = new Employee(lastName, firstName, EmployeeRole.CASHIER);
        Employee employee2 = new Employee(lastName, firstName, EmployeeRole.CASHIER);
        assertEquals(employee1.hashCode(), employee2.hashCode());
    }
}
