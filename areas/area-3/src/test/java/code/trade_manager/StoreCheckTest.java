package code.trade_manager;

import org.junit.Before;
import org.junit.Test;

import javax.accessibility.AccessibleKeyBinding;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Date;

import static org.junit.Assert.*;

public class StoreCheckTest {
    private final int id = 123;
    private final Date date = new Date();
    private final Timestamp time = new Timestamp(date.getTime());
    private HashSet<Product> products = new HashSet<>();
    private Cashbox cashbox = new Cashbox(321);
    private StoreCheck check;

    @Before
    public void init() {
        Product newProduct = new Product("abc", 555, "shmel");
        products.add(newProduct);
        check = new StoreCheck(id, products, cashbox, time);
    }
    
    @Test
    public void getId() {
        assertEquals(id, check.getId());
    }

    @Test
    public void getProducts() {
        assertEquals(products, check.getProducts());
    }

    @Test
    public void getCashbox() {
        assertEquals(cashbox, check.getCashbox());
    }

    @Test
    public void getTime() {
        assertEquals(time, check.getTime());
    }
    
    @Test
    public void setProducts() {
        Product product1 = new Product("abc", 555, "shmel");
        HashSet<Product> products1 = new HashSet<>();
        products1.add(product1);
        check.setProducts(products1);
        assertEquals(products1, check.getProducts());
    }

    @Test
    public void setCashbox() {
        Cashbox cashbox1 = new Cashbox(777);
        check.setCashbox(cashbox1);
        assertEquals(cashbox1, check.getCashbox());
    }

    @Test
    public void addProduct() {
        Product product1 = new Product("abc", 555, "shmel");
        HashSet<Product> products1 = new HashSet<>();
        products1.add(product1);
        check.clearProducts();
        check.addProduct(product1);
        assertEquals(products1, check.getProducts());
    }

    @Test
    public void deleteProduct() {
        Product product1 = new Product("abc", 555, "shmel");
        Product product2 = new Product("penibagen_scvoz", 555, "djungli");
        HashSet<Product> products1 = new HashSet<>();
        products1.add(product1);
        check.clearProducts();
        check.addProduct(product1);
        check.addProduct(product2);
        check.deleteProduct(product2);
        assertEquals(products1, check.getProducts());
    }

    @Test
    public void clearProduct() {
        Product product1 = new Product("abc", 555, "shmel");
        check.addProduct(product1);
        check.clearProducts();
        assertTrue(check.getProducts().isEmpty());
    }

    @Test
    public void positiveEquals() {
        StoreCheck check1 = new StoreCheck(id, time);
        assertEquals(check1, check);
    }

    @Test
    public void negativeEquals() {
        StoreCheck check1 = new StoreCheck(777, time);
        assertNotEquals(check1, check);
    }

    @Test
    public void testHashCode() {
        StoreCheck check1 = new StoreCheck(id, cashbox, time);
        assertEquals(check1.hashCode(), check.hashCode());
    }
}
