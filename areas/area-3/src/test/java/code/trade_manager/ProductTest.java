package code.trade_manager;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProductTest {

    private static final String NAME = "name";
    private static final float PRICE = 123;
    private static final int PRODUCT_CODE = 1;
    private static final String BARCODE = "barcode";
    private Product product;

    @Before
    public void init() {
        product = new Product(NAME, PRICE, PRODUCT_CODE, BARCODE);
    }

    @Test
    public void getName() {
        assertEquals(NAME, product.getName());
    }

    @Test
    public void setName() {
        String newName = "newName";
        product.setName(newName);
        assertEquals(newName, product.getName());
    }

    @Test
    public void getPrice() {
        assertEquals(PRICE, product.getPrice(), 0);
    }

    @Test
    public void setPrice() {
        float newPrice = 321;
        product.setPrice(newPrice);
        assertEquals(newPrice, product.getPrice(), 0);
    }

    @Test
    public void getProductCode() {
        assertEquals(PRODUCT_CODE, product.getProductCode());
    }

    @Test
    public void setProductCode() {
        int newProductCode = 2;
        product.setProductCode(newProductCode);
        assertEquals(newProductCode, product.getProductCode());
    }

    @Test
    public void getBarcode() {
        assertEquals(BARCODE, product.getBarcode());
    }

    @Test
    public void setBarcode() {
        String newBarcode = "newBarcode";
        product.setBarcode(newBarcode);
        assertEquals(newBarcode, product.getBarcode());
    }

    @Test
    public void positiveEquals() {
        Product newProduct = new Product(NAME, PRODUCT_CODE, BARCODE);
        assertEquals(newProduct, product);
    }

    @Test
    public void negativeEquals() {
        String newName = "newName";
        Product newProduct = new Product(newName, PRODUCT_CODE, BARCODE);
        assertNotEquals(newProduct, product);
    }

    @Test
    public void testHashCode() {
        assertEquals(-1386389303, product.hashCode());
    }
}
