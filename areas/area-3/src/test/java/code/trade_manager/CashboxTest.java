package code.trade_manager;

import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;

import static org.junit.Assert.*;

public class CashboxTest {
    private final String serialNumberKKT = "abc";
    private final int id = 123;
    private final Date date = new Date();
    private final Timestamp time = new Timestamp(date.getTime());
    private HashSet<StoreCheck> checks = new HashSet<>();
    private Employee employee = new Employee("Buhanka", "Fedor", EmployeeRole.CASHIER);
    private StoreCheck check = new StoreCheck(321, time);
    private Cashbox cashbox;
    

    @Before
    public void init() {
        checks.add(check);
        cashbox = new Cashbox(serialNumberKKT, id, checks, employee);
    }
    
    @Test
    public void getSerialNumberKKT() {
        assertEquals(serialNumberKKT, cashbox.getSerialNumberKKT());
    }
    
    @Test
    public void getChecks() {
        assertEquals(checks, cashbox.getChecks());
    }
    
    @Test
    public void getEmployee() {
        assertEquals(employee, cashbox.getEmployee());
    }
    
    @Test
    public void getId() {
        assertEquals(id, cashbox.getId());
    }
    
    @Test 
    public void setSerialNumberKKT() {
        String serialNumber = "cba";
        cashbox.setSerialNumberKKT(serialNumber);
        assertEquals(serialNumber, cashbox.getSerialNumberKKT());
    }
    
    @Test
    public void setChecks() {
        HashSet<StoreCheck> checks1 = new HashSet<>();
        StoreCheck check1 = new StoreCheck(777, time);
        checks1.add(check1);
        cashbox.setChecks(checks1);
        assertEquals(checks1, cashbox.getChecks());
    }
    
    @Test
    public void setEmployee() {
        Employee employee1 = new Employee("Namana", "Kachaet", EmployeeRole.SENIOR_CASHIER);
        cashbox.setEmployee(employee1);
        assertEquals(employee1, cashbox.getEmployee());
    }

    @Test
    public void addCheck() {
        StoreCheck check1 = new StoreCheck(555, time);
        HashSet<StoreCheck> checks1 = new HashSet<>();
        checks1.add(check1);
        cashbox.clearChecks();
        cashbox.addCheck(check1);
        assertEquals(checks1, cashbox.getChecks());
    }

    @Test
    public void deleteCheck() {
        StoreCheck check1 = new StoreCheck(555, time);
        StoreCheck check2 = new StoreCheck(888, time);
        HashSet<StoreCheck> checks1 = new HashSet<>();
        checks1.add(check2);
        cashbox.clearChecks();
        cashbox.addCheck(check1);
        cashbox.addCheck(check2);
        cashbox.deleteCheck(check1);
        assertEquals(checks1, cashbox.getChecks());
    }

    @Test
    public void clearCheck() {
        StoreCheck check1 = new StoreCheck(555, time);
        cashbox.addCheck(check1);
        cashbox.clearChecks();
        assertTrue(cashbox.getChecks().isEmpty());
    }
    
    @Test
    public void testHashCode() {
        Cashbox cashbox1 = new Cashbox(serialNumberKKT, id, checks, employee);
        assertEquals(cashbox1.hashCode(), cashbox.hashCode());
    }

    @Test
    public void positiveEquals() {
        Cashbox cashbox1 = new Cashbox(serialNumberKKT, id, employee);
        Cashbox cashbox2 = new Cashbox(serialNumberKKT, id, employee);
        assertEquals(cashbox1, cashbox2);
    }

    @Test
    public void negativeEquals() {
        int newId = 111;
        Cashbox cashbox1 = new Cashbox(serialNumberKKT, newId, employee);
        assertNotEquals(cashbox1, cashbox);
    }
}
