package mephi.portal;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class ArrivalProductsTest {
    private static final String DEPARTMENT = "department";
    private static final boolean ISCOMING = true;
    private static final Float PRICE1 = (float)29.99;
    private static final Float PRICE2 = (float)999.99;

    LegalEntity legalEntity = new LegalEntity("name", "phone", "email", "address", "INN", RoleLegalEntity.SUPPLIER);
    PriceList priceList = new PriceList("name", TypePriceList.Procurement);

    Product product1 = new Product("name", 123, "barcode");
    Product product2 = new Product("name2", "lyaaaaaaa", 999, "barcode2");
    Product product3 = new Product("name3", 699, "barcode3");

    HashMap<Product, Integer> ordering;

    ArrivalProducts arrivalProducts;

    @Before
    public void init() {
        arrivalProducts = new ArrivalProducts(legalEntity, ISCOMING, DEPARTMENT);

        priceList.addProductPrice(product1, PRICE1);
        priceList.addProductPrice(product3, PRICE2);
        arrivalProducts.addPriceLists(priceList);
        arrivalProducts.AddCountForProduct(priceList, product1, 14);

        ordering = new HashMap<>();
        Integer count =  14;

        ordering.put(product1, count);
    }

    @Test
    public void getLegalEntity() {
        assertEquals(legalEntity, arrivalProducts.getLegalEnt());
    }

    @Test
    public void getIsComing() {
        assertEquals(ISCOMING, arrivalProducts.getIsComing());
    }

    @Test
    public void getDepartment() {
        assertEquals(DEPARTMENT, arrivalProducts.getDepartment());
    }

    @Test
    public void getOrdering() {
        assertEquals(ordering, arrivalProducts.getOrdering());
    }

    @Test
    public void setDepartment() {
        String newDepartment = "newDepartment";
        arrivalProducts.setDepartment(newDepartment);
        assertEquals(newDepartment, arrivalProducts.getDepartment());
    }

    @Test
    public void setLegalEntity() {
        LegalEntity newLegalEntity = new LegalEntity("name2", "phone2", "email2", "address2", "INN2", RoleLegalEntity.CUSTOMER);
        arrivalProducts.setLegalEnt(newLegalEntity);
        assertEquals(newLegalEntity, arrivalProducts.getLegalEnt());
    }

    @Test
    public void setIscoming() {
        boolean newIsComing = false;
        arrivalProducts.setIsComing(newIsComing);
        assertEquals(newIsComing, arrivalProducts.getIsComing());
    }

    @Test
    public void goodAddCountForGood() {
        assertTrue(arrivalProducts.AddCountForProduct(priceList, product3, 123));
    }

    @Test
    public void badAddCountForGood1() {
        assertFalse(arrivalProducts.AddCountForProduct(priceList, product2, 123));
    }

    @Test
    public void badAddCountForGood2() {
        assertFalse(arrivalProducts.AddCountForProduct(priceList, product3, -32));
    }
}