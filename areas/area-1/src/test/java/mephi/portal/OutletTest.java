package mephi.portal;

import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;

import static org.junit.Assert.*;

public class OutletTest {
    private static final String NAME = "name";
    private static final String ADDRESS = "address";
    private static final int ID = 543;
    private static final String SERIALNUMBER = "124325434";
    private static final int ID2 = 765;
    private static final String SERIALNUMBER2 = "87654326";
    private static final Float PRICE1 = (float)29.99;
    private static final Float PRICE2 = (float)999.99;
    private static final Float PRICE3 = (float)3434.99;
    private static final LocalDate DATE = LocalDate.now();
    private static final LocalTime TIME = LocalTime.now();

    HashMap<Product, Pair<Float, Integer>> products = new HashMap<>();
    HashMap<Product, Pair<Float, Integer>> products2 = new HashMap<>();
    Product product = new Product("name", 123, "barcode");
    Product product2 = new Product("name2", 345, "barcode2");
    Product product3 = new Product("name3", 678, "barcode3");
    Pair<Float, Integer> pair1 = new Pair<>(PRICE1, 14);
    Pair<Float, Integer> pair2 = new Pair<>(PRICE2, 45);
    Pair<Float, Integer> pair3 = new Pair<>(PRICE3, 67);

    Outlet outlet;

    @Before
    public void init() {
        outlet = new Outlet(NAME, ADDRESS);

        products.put(product, pair1);
        products.put(product2, pair2);
        products2.put(product3, pair3);

        outlet.addCash(ID, SERIALNUMBER);
        outlet.addCheque(ID, products, DATE, TIME, outlet.getCashes().get(0));
    }

    @Test
    public void getCash() {
        assertEquals(1, outlet.getCashes().size());
        assertEquals(ID, outlet.getCashes().get(0).getId());
        assertEquals(SERIALNUMBER, outlet.getCashes().get(0).getSerialNumber());
    }

    @Test
    public void getCheque() {
        assertEquals(1, outlet.getCheques().size());
        assertEquals(ID, outlet.getCheques().get(0).getId());
        assertEquals(products, outlet.getCheques().get(0).getProducts());
        assertEquals(DATE, outlet.getCheques().get(0).getDate());
        assertEquals(TIME, outlet.getCheques().get(0).getTime());
        assertEquals(outlet.getCashes().get(0), outlet.getCheques().get(0).getCash());
    }

    @Test
    public void addCash() {
        int was = outlet.getCashes().size();
        outlet.addCash(ID2, SERIALNUMBER2);
        assertEquals(was+1, outlet.getCashes().size());
    }

    @Test
    public void addCheque() {
        int was = outlet.getCheques().size();
        outlet.addCheque(ID2, products2, LocalDate.now(), LocalTime.now(), outlet.getCashes().get(0));
        assertEquals(was+1, outlet.getCheques().size());
    }

}
