package mephi.portal;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DepartmentTest {
    private static final String NAME = "name";
    private static final String ADDRESS = "address";
    private static final String NAMEPC = "prodCatalog";
    private static final Float PRICE1 = (float)29.99;
    private static final Float PRICE2 = (float)999.99;
    private static final Float PRICE3 = (float)3434.99;

    LegalEntity legalEntity = new LegalEntity("name", "phone", "email", "address", "INN", RoleLegalEntity.SUPPLIER);
    ArrivalProducts arrivalProducts = new ArrivalProducts(legalEntity, true, "department");
    ArrivalProducts arrivalProducts2 = new ArrivalProducts(legalEntity, false, "department2");

    PriceList priceList = new PriceList("name", TypePriceList.Procurement);
    PriceList priceList2 = new PriceList("name2", TypePriceList.Wholesale);

    Product product1 = new Product("name", 123, "barcode");
    Product product2 = new Product("name2", "lyaaaaaaa", 999, "barcode2");
    Product product3 = new Product("name3", 699, "barcode3");

    Department department;

    @Before
    public void init() {
        priceList.addProductPrice(product1, PRICE1);
        priceList.addProductPrice(product2, PRICE2);
        priceList2.addProductPrice(product3, PRICE3);
        arrivalProducts.addPriceLists(priceList);
        arrivalProducts.AddCountForProduct(priceList, product1, 15);
        arrivalProducts.AddCountForProduct(priceList, product2, 2);
        arrivalProducts2.addPriceLists(priceList);
        arrivalProducts2.addPriceLists(priceList2);
        arrivalProducts2.AddCountForProduct(priceList2, product1, 4);
        arrivalProducts2.AddCountForProduct(priceList, product2, 3);
        Document document1 = new Document(1, TypeDocument.Purchase, arrivalProducts);
        department = new Department(NAME, ADDRESS);
        department.addDocument(document1);

    }

    @Test
    public void getName() {
        assertEquals(NAME, department.getName());
    }

    @Test
    public void getAddress() {
        assertEquals(ADDRESS, department.getAddress());
    }

    @Test
    public void setName() {
        String newName = "newName";
        department.setName(newName);
        assertEquals(newName, department.getName());
    }

    @Test
    public void setAddress() {
        String newAddress = "newAddress";
        department.setAddress(newAddress);
        assertEquals(newAddress, department.getAddress());
    }

    @Test
    public void productCatalog() {
        department.setProductCatalog(NAMEPC);
        assertEquals(NAMEPC, department.getProductCatalog().getName());
    }

    @Test
    public void goodSetProduct() {
        assertTrue(department.setProducts(arrivalProducts));
    }

    @Test
    public void goodSetProduct2() {
        department.setProducts(arrivalProducts);
        assertTrue(department.setProducts(arrivalProducts));
    }

    @Test
    public void badSetProduct1() {
        department.setProducts(arrivalProducts);
        assertFalse(department.setProducts(arrivalProducts2));
    }

    @Test
    public void badSetProduct2() {
        arrivalProducts2.AddCountForProduct(priceList2, product3, 4);
        department.setProducts(arrivalProducts);
        assertFalse(department.setProducts(arrivalProducts2));
    }

    @Test
    public void getProduct() {
        department.setProducts(arrivalProducts);
        assertEquals(arrivalProducts.getOrdering().size(), department.getProducts().size());
    }
}