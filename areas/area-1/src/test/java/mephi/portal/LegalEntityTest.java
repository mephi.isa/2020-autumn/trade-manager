package mephi.portal;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LegalEntityTest {
    private static final String NAME = "name";
    private static final String PHONE = "phone";
    private static final String EMAIL = "email";
    private static final String ADDRESS = "address";
    private static final String INN = "inn";

    LegalEntity legEnt;

    @Before
    public void init() {
        legEnt = new LegalEntity(NAME, PHONE, EMAIL, ADDRESS, INN, RoleLegalEntity.CUSTOMER);
    }

    @Test
    public void getName() {
        assertEquals(NAME, legEnt.getName());
    }

    @Test
    public void getPhone() {
        assertEquals(PHONE, legEnt.getPhone());
    }

    @Test
    public void getEmail() {
        assertEquals(EMAIL, legEnt.getEmail());
    }

    @Test
    public void getAddress() {
        assertEquals(ADDRESS, legEnt.getAddress());
    }

    @Test
    public void getINN() {
        assertEquals(INN, legEnt.getINN());
    }

    @Test
    public void getRole() {
        assertEquals(RoleLegalEntity.CUSTOMER, legEnt.getRoleLegalEnt());
    }

    @Test
    public void setName() {
        String newName = "newName";
        legEnt.setName(newName);
        assertEquals(newName, legEnt.getName());
    }

    @Test
    public void setPhone() {
        String newPhone = "newPhone";
        legEnt.setPhone(newPhone);
        assertEquals(newPhone, legEnt.getPhone());
    }

    @Test
    public void setEmail() {
        String newEmail = "newEmail";
        legEnt.setEmail(newEmail);
        assertEquals(newEmail, legEnt.getEmail());
    }

    @Test
    public void setAddress() {
        String newAddress = "newAddress";
        legEnt.setAddress(newAddress);
        assertEquals(newAddress, legEnt.getAddress());
    }

    @Test
    public void setInn() {
        String newINN = "newINN";
        legEnt.setINN(newINN);
        assertEquals(newINN, legEnt.getINN());
    }

    @Test
    public void setLegEnt() {
        legEnt.setRoleLegalEnt(RoleLegalEntity.SUPPLIER);
        assertEquals(RoleLegalEntity.SUPPLIER, legEnt.getRoleLegalEnt());
    }

//    @Test
//    public void setPhone() {
//    }
//
//    @Test
//    public void setPhone() {
//    }
}
