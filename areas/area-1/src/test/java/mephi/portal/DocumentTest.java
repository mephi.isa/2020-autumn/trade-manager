package mephi.portal;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.*;

public class DocumentTest {
    private static final int NUMBER = 999;
    private static final LocalDate DATE = LocalDate.now();
    private static final LocalTime TIME = LocalTime.now();

    LegalEntity legalEntity = new LegalEntity("name", "phone", "email", "address", "INN", RoleLegalEntity.SUPPLIER);
    ArrivalProducts arrivalProducts = new ArrivalProducts(legalEntity, true, "5ochka");
    ArrivalProducts arrivalProducts2 = new ArrivalProducts(legalEntity, false, "Avos'ka");

    Document document1;
    Document document2;

    @Before
    public void init() {
        document1 = new Document(NUMBER, DATE, TIME, TypeDocument.Purchase, arrivalProducts);
        document2 = new Document(NUMBER, TypeDocument.Supply, arrivalProducts);
    }

    @Test
    public void constructorWithoutDateTime() {
        assertTrue(document2.getTime().isAfter(document1.getTime()));
        assertEquals(document1.getDate(), document2.getDate());
    }

    @Test
    public void getNumber() {
        assertEquals(NUMBER, document1.getNumber());
    }

    @Test
    public void getDate() {
        assertEquals(DATE, document1.getDate());
    }

    @Test
    public void getTime() {
        assertEquals(TIME, document1.getTime());
    }

    @Test
    public void getType() {
        assertEquals(TypeDocument.Purchase, document1.getType());
    }

    @Test
    public void getArrProducts() {
        assertEquals(arrivalProducts, document1.getArrProducts());
    }

    @Test
    public void setNumber() {
        int newNumber = 799;
        document1.setNumber(newNumber);
        assertEquals(newNumber, document1.getNumber());
    }

    @Test
    public void setDate() {
        LocalDate newDate = LocalDate.ofYearDay(1999, 285);
        document1.setDate(newDate);
        assertEquals(newDate, document1.getDate());
    }

    @Test
    public void setTime() {
        LocalTime newTime = LocalTime.of(9, 15);
        document1.setTime(newTime);
        assertEquals(newTime, document1.getTime());
    }

    @Test
    public void setType() {
        document1.setType(TypeDocument.Supply);
        assertEquals(TypeDocument.Supply, document1.getType());
    }

    @Test
    public void setArrivalProducts() {
        document1.setArrProducts(arrivalProducts2);
        assertEquals(arrivalProducts2, document1.getArrProducts());
    }
}
