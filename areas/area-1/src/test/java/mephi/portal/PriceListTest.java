package mephi.portal;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

public class PriceListTest {
    private static final String NAME = "name";
    HashMap<Product, Float> prices = new HashMap<>();

    Product product = new Product("name", 123, "barcode");
    Product product2 = new Product("name2", "description", 321, "barcode2");

    PriceList priceList;

    @Before
    public void init() {
        priceList = new PriceList(NAME, TypePriceList.Retail);

        Float price = (float) 2342.99;
        prices.put(product, price);
        priceList.addProductPrice(product, price);
    }

    @Test
    public void getName() {
        assertEquals(NAME, priceList.getName());
    }

    @Test
    public void getType() {
        assertEquals(TypePriceList.Retail, priceList.getType());
    }

    @Test
    public void getPrices() {
        assertEquals(prices, priceList.getPrices());
    }

    @Test
    public void setName() {
        String newName = "newName";
        priceList.setName(newName);
        assertEquals(newName, priceList.getName());
    }

    @Test
    public void setType() {
        TypePriceList newType = TypePriceList.Wholesale;
        priceList.setType(newType);
        assertEquals(newType, priceList.getType());
    }

    @Test
    public void goodAddProduct() {
        Float price = (float) 4365.79;
        assertTrue(priceList.addProductPrice(product2, price));
    }

    @Test
    public void badAddProduct() {
        Float price = (float) -353.7;
        assertFalse(priceList.addProductPrice(product2, price));
    }
}
