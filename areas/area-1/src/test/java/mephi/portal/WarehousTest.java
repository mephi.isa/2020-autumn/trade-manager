package mephi.portal;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WarehousTest {
    private static final String NAME = "name";
    private static final String ADDRESS = "address";

    Warehouse warehouse;

    @Before
    public void init() {
        warehouse = new Warehouse(NAME, ADDRESS);
    }

    @Test
    public void getName() {
        assertEquals(NAME, warehouse.getName());
    }

    @Test
    public void getAddress() {
        assertEquals(ADDRESS, warehouse.getAddress());
    }
}
