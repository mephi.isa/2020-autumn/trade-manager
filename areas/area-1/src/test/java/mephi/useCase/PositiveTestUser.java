package mephi.useCase;

import javafx.util.Pair;
import mephi.DAO.DepartmentDaoImpl;
import mephi.Dependencies.IRepositoryAutentification;
import mephi.Dependencies.IRepository;
import mephi.UserInputOutput.IUserInput;
import mephi.portal.*;
import mephi.structures.legalEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.ICheckFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PositiveTestUser {
    private Repository repository;
    private RepositoryAutentification auth;
    private UserInput userInput;
    private CheckFormat checkFormat;
    private addNewCustomer sutAddNewCustomer;
    private addNewSupplier sutAddNewSupplier;
    private updateCustomer sutUpdateCustomer;
    private updateSupplier sutUpdateSupplier;
    private deleteCustomer sutDeleteCustomer;
    private deleteSupplier sutDeleteSupplier;
    private showAllSuppliers sutShowAllSuppliers;
    private showAllCustomer sutShowAllCustomer;
    private deleteRetailPriceList sutDeleteRetailPriceList;
    private deleteProcurementPriceList sutDeleteProcurementPriceList;
    private deleteWholesalePriceList sutDeleteWholesalePriceList;
    class CheckFormat implements ICheckFormat{

        @Override
        public boolean checkLegalEntity(legalEntity le) throws Exception {

            if (le.Name.length() >30 || le.Name.length() < 2){
                return false;
            }

            if (!le.Phone.matches("(\\+*)\\d{11}")){
                return false;
            }

            if (!le.Email.matches("[A-Za-z0-9\\-\\.]*@[a-z]*\\.ru")){
                return false;
            }

            if (le.INN.length() >30 || le.INN.length() < 4){
                return false;
            }

            if (le.Address.length() >30 || le.Address.length() < 5){
                return false;
            }


            return true;

        }
    }

    private DepartmentDaoImpl Dao;
    class UserInput implements IUserInput{

        @Override
        public legalEntity getLegalEntity(String token) {
            legalEntity le=new legalEntity();
            le.Address = "Москва 10";
            le.INN = "15790dfdsf43";
            le.Email = "com@yandex.ru";
            le.Phone = "+12345678934";
            le.Name = "Ира";
            return le;
        }

        @Override
        public legalEntity getUpdateLegalEntity(String token, legalEntity le) {
            le.Address = "Москва 10";
            le.Email = "com@yandex.ru";
            le.Phone = "+12345678934";
            le.Name = "Ира";
            return le;
        }
    }
    class RepositoryAutentification implements IRepositoryAutentification{

        @Override
        public int check(Integer id) {
            if (id > 0 && id < 50){
                return 1;
            }
            if (id > 50 ){
                return 2;
            }
            return -1;
        }

        @Override
        public int check(String token, Integer i) throws Exception {
            if ((token=="AB" && i==1) || (token=="AC" && i==2) || (token=="AD" && i==3)){
                return 0;
            }
            throw new Exception("Error token");
        }
    }

    class Repository implements IRepository {


        @Override
        public LegalEntity getLegalEntityById(Integer id) throws Exception {

            return null;
        }

        @Override
        public Integer addNewUser(LegalEntity legalEntity) throws Exception {
            if(Dao.getLegalEntityByINN(legalEntity.getINN())!=null){
                return 0;
            }
            Dao.addNewUser(legalEntity);
            return 1;
        }

        @Override
        public Integer deleteSupplierAndPriceLists(String INN) throws Exception {
            Integer i=0;
            for (LegalEntity le : Dao.getAllLegalEntity()){
                if (le.getINN() == INN){
                    Dao.deleteLegalEntity(i);
                    break;
                }
                i++;
            }
            ArrayList<PriceList> priceListSupplier = Dao.getAllPriceListsSuppliers().get(INN);
            for(PriceList p: priceListSupplier){
                Dao.getAllPriceLists().remove(p);
            }

            Dao.getAllPriceListsSuppliers().remove(INN);
            return i+1;
        }

        @Override
        public ArrayList<PriceList> getPriceListsSupplierByINN(String INN) throws Exception {
            return Dao.getAllPriceListsSuppliers().get(INN);
        }

        @Override
        public HashMap<String, ArrayList<PriceList>> getAllPriceListsSupplier() throws Exception {
            return Dao.getAllPriceListsSuppliers();
        }

        @Override
        public HashMap<String, ArrayList<PriceList>> getAllPriceListsCustomers() throws Exception {
            return Dao.getAllPriceListsCustomers();
        }


        @Override
        public Integer updateLegalEntity(LegalEntity oldLegalEntity, LegalEntity newLegalEntity) {
            return null;
        }

        @Override
        public ArrayList<LegalEntity> getAllSuppliers() throws Exception {
            ArrayList<LegalEntity> legalEntitySupplier = new ArrayList<>();
            for (LegalEntity le : Dao.getAllLegalEntity()){
                if(le.getRoleLegalEnt() == RoleLegalEntity.SUPPLIER){
                    legalEntitySupplier.add(le);
                }
            }
            return legalEntitySupplier;
        }

        @Override
        public ArrayList<LegalEntity> getAllCustomers() throws Exception {
            ArrayList<LegalEntity> legalEntityCustomer = new ArrayList<>();
            for (LegalEntity le : Dao.getAllLegalEntity()){
                if(le.getRoleLegalEnt() == RoleLegalEntity.CUSTOMER){
                    legalEntityCustomer.add(le);
                }
            }
            return legalEntityCustomer;
        }

        @Override
        public RoleLegalEntity getRoleLegalByINN(String inn) throws Exception {
            List<LegalEntity> legalEntities = Dao.getAllLegalEntity();
            for (LegalEntity le :legalEntities){
                if (le.getINN() == inn){
                    return le.getRoleLegalEnt();
                }
            }
            return null;
        }
        @Override
        public LegalEntity getLegalEntityByINN(String inn) throws Exception {
            return Dao.getLegalEntityByINN(inn);
        }

        @Override
        public Integer deleteCustomerAndPriceLists(String INN) throws Exception {
            Integer i=0;
            for (LegalEntity le : Dao.getAllLegalEntity()){
                if (le.getINN() == INN){
                    Dao.deleteLegalEntity(i);
                    break;
                }
                i++;
            }

            ArrayList<PriceList> priceListCustomer = Dao.getAllPriceListsCustomers().get(INN);
            for(PriceList p: priceListCustomer){
                Dao.getAllPriceLists().remove(p);

            }
            Dao.getAllPriceListsCustomers().remove(INN);
            return i+1;
        }

        @Override
        public Integer updateLegalEntity(LegalEntity legEntityObject) {

            return null;
        }

        @Override
        public ArrayList<ArrivalProducts> getAllArrivalProducts() throws Exception {
            return Dao.getAllArrivalProducts();
        }

        @Override
        public ArrayList<Document> getAllDocuments() throws Exception {
            return Dao.getAllDocuments();
        }

        @Override
        public Integer deleteLegalEntity(LegalEntity legalEntityByINN) throws Exception {
            Dao.getAllLegalEntity().remove(legalEntityByINN);
            return 1;
        }

        @Override
        public Integer deletePriceListByName(String key) throws Exception {
            for(PriceList pl :Dao.getAllPriceLists()){
                if(pl.getName() == key){
                    Dao.getAllPriceLists().remove(pl);
                    pl = null;
                    return 1;
                }
            }
            return 0;
        }

        @Override
        public Integer getAllPriceLists() throws Exception {
            return null;
        }

        @Override
        public PriceList getPriceListByName(String key) throws Exception {
            return Dao.getPriceListByName(key);
        }

        @Override
        public Integer addDepartment(Department department) {
            return null;
        }

        @Override
        public Integer addNewPriceList(PriceList priceList) {
            return null;
        }

        @Override
        public HashMap<Product, Integer> getProductsInWarehouse() {
            return null;
        }

        @Override
        public String getProductNameByCode(int code) {
            return null;
        }

        @Override
        public String getProductDescriptionByCode(int code) {
            return null;
        }

        @Override
        public String getProductBarcodeByCode(int code) {
            return null;
        }
    }
    @BeforeEach
    public void init(){
        this.repository = new Repository();
        this.userInput = new UserInput();
        this.auth = new RepositoryAutentification();
        this.checkFormat = new CheckFormat();
        this.sutAddNewCustomer = new addNewCustomer(this.auth, this.repository, this.userInput, this.checkFormat);
        this.sutAddNewSupplier = new addNewSupplier(this.auth, this.repository, this.userInput, this.checkFormat);
        this.sutUpdateCustomer = new updateCustomer(this.auth, this.repository, this.userInput, this.checkFormat);
        this.sutUpdateSupplier = new updateSupplier(this.auth, this.repository, this.userInput, this.checkFormat);
        this.sutDeleteCustomer = new deleteCustomer(this.auth, this.repository);
        this.sutDeleteSupplier = new deleteSupplier(this.auth, this.repository);
        this.sutShowAllCustomer = new showAllCustomer(this.auth, this.repository);
        this.sutShowAllSuppliers = new showAllSuppliers(this.auth, this.repository);
        this.sutDeleteRetailPriceList = new deleteRetailPriceList(this.auth, this.repository);
        this.sutDeleteProcurementPriceList = new deleteProcurementPriceList(this.auth, this.repository);
        this.sutDeleteWholesalePriceList = new deleteWholesalePriceList(this.auth, this.repository);
        this.Dao = new DepartmentDaoImpl();
    }

    @Test
    public void addNewCustomerTest() throws Exception {
        legalEntity le2 = new legalEntity();
        le2.Email = "ds@yandex.ru";
        le2.Address = "Новосибирск";
        le2.INN = "21312423594493";
        le2.Phone = "+12345678454";
        le2.Name = "Клава";
        assertThrows(Exception.class, ()->this.sutAddNewCustomer.Run(new Pair<>(le2, "AC1")), "Error token");
        assertThrows(Exception.class, ()->this.sutAddNewCustomer.getAddCustomer("AB"), "Error token");
        legalEntity le1 = this.sutAddNewCustomer.getAddCustomer("AC");
        assertThrows(Exception.class, ()->sutAddNewCustomer.Run(new Pair<>(le1, "AC1")), "Error token");
        String tempINN = le1.INN;
        le1.INN="095843959354543";
        assertThrows(Exception.class, ()->sutAddNewCustomer.Run(new Pair<>(le1, "AC")), "Error contains");
        le1.INN = tempINN;
        le2.Address = le1.Address;
        le2.Email = le1.Email;
        le2.Name = le1.Name;
        le2.INN = le1.INN;
        le2.Phone = le1.Phone;
        assertThrows(Exception.class, ()->sutAddNewCustomer.Run(new Pair<>(le2, "AC")), "Error object");
        legalEntity le = this.sutAddNewCustomer.Run(new Pair<>(le1, "AC"));
        assertEquals(le.statusReturn, "SUCCESS");
        assertThrows(Exception.class, ()->sutAddNewCustomer.Run(new Pair<>(le1, "AC")), "Error. LegalEntity did not save");

    }

    @Test
    public void addNewSupplierTest() throws Exception {
        legalEntity le2 = new legalEntity();
        le2.Email = "ds@yandex.ru";
        le2.Address = "Новосибирск";
        le2.INN = "21312423594493";
        le2.Phone = "+12345678454";
        le2.Name = "Клава";
        assertThrows(Exception.class, ()->this.sutAddNewSupplier.Run(new Pair<>(le2, "AC1")), "Error token");
        assertThrows(Exception.class, ()->this.sutAddNewSupplier.getAddSupplier("AC"), "Error token");
        legalEntity le1 = this.sutAddNewSupplier.getAddSupplier("AB");
        assertThrows(Exception.class, ()->sutAddNewSupplier.Run(new Pair<>(le1, "AC1")), "Error token");
        String tempINN = le1.INN;
        le1.INN="095843959354543";
        assertThrows(Exception.class, ()->sutAddNewSupplier.Run(new Pair<>(le1, "AB")), "Error contains");
        le1.INN = tempINN;
        le2.Address = le1.Address;
        le2.Email = le1.Email;
        le2.Name = le1.Name;
        le2.INN = le1.INN;
        le2.Phone = le1.Phone;
        assertThrows(Exception.class, ()->sutAddNewSupplier.Run(new Pair<>(le2, "AB")), "Error object");
        legalEntity le = this.sutAddNewSupplier.Run(new Pair<>(le1, "AB"));
        assertEquals(le.statusReturn, "SUCCESS");
        assertThrows(Exception.class, ()->sutAddNewSupplier.Run(new Pair<>(le1, "AB")), "Error. LegalEntity did not save");

    }

    @Test
    public void updateCustomerTest() throws Exception {
        legalEntity le2 = new legalEntity();
        le2.Email = "ds@yandex.ru";
        le2.Address = "Новосибирск";
        le2.INN = "21312423594493";
        le2.Phone = "+12345678454";
        le2.Name = "Клава";
        assertThrows(Exception.class, ()->this.sutUpdateCustomer.Run(new Pair<>(le2, "AC")), "Error token");
        assertThrows(Exception.class, ()->sutUpdateCustomer.getUpdateCustomer("AB","085789"), "Error token");
        legalEntity le = this.sutUpdateCustomer.getUpdateCustomer("AC","085789");
        assertThrows(Exception.class, ()->this.sutUpdateCustomer.Run(new Pair<>(le2, "AC")), "Error object");
        assertEquals(this.sutUpdateCustomer.Run(new Pair<>(le, "AC")), le);
        assertEquals(le.statusReturn, "SUCCESS");

    }

    @Test
    public void updateSupplierTest() throws Exception {
        legalEntity le2 = new legalEntity();
        le2.Email = "ds@yandex.ru";
        le2.Address = "Новосибирск";
        le2.INN = "21312423594493";
        le2.Phone = "+12345678454";
        le2.Name = "Клава";
        assertThrows(Exception.class, ()->this.sutUpdateSupplier.Run(new Pair<>(le2, "AB")), "Error token");
        assertThrows(Exception.class, ()->sutUpdateSupplier.getUpdateSupplier("AC","085ролнн"), "Error token");
        legalEntity le = this.sutUpdateSupplier.getUpdateSupplier("AB","085ролнн");
        assertThrows(Exception.class, ()->this.sutUpdateSupplier.Run(new Pair<>(le2, "AB")), " Error object");
        assertEquals(this.sutUpdateSupplier.Run(new Pair<>(le, "AB")), le);
        assertEquals(le.statusReturn, "SUCCESS");

    }
    @Test
    public void deleteCustomerTest() throws Exception {
        assertThrows(Exception.class, ()->this.sutDeleteCustomer.Run(new Pair<>("085ролнн", "AC")), "Error. LegalEntity do not exist");
        assertThrows(Exception.class, ()->this.sutDeleteCustomer.Run(new Pair<>("085789", "AB")), "Error token");
        assertEquals(this.sutDeleteCustomer.Run(new Pair<>("085789", "AC")), 1);
        assertThrows(Exception.class, ()->this.sutDeleteCustomer.Run(new Pair<>("085789", "AC")), "Error. LegalEntity do not exist");
    }

    @Test
    public void deleteSupplierTest() throws Exception {
        assertThrows(Exception.class, ()->this.sutDeleteSupplier.Run(new Pair<>("085789", "AB")), "Error. LegalEntity do not exist");
        assertThrows(Exception.class, ()->this.sutDeleteSupplier.Run(new Pair<>("085ролнн", "AC")), "Error token");
        assertEquals(this.sutDeleteSupplier.Run(new Pair<>("085ролнн", "AB")), 1);
        assertThrows(Exception.class, ()->this.sutDeleteSupplier.Run(new Pair<>("085ролнн", "AB")), "Error. LegalEntity do not exist");
    }

    @Test
    public void showAllSupplierTest() throws Exception{
        ArrayList<legalEntity> le = this.sutShowAllSuppliers.Run("AB");
        assertEquals(le.size(), 2);
        assertThrows(Exception.class, ()->this.sutShowAllSuppliers.Run("AC"), "Error token");

    }

    @Test
    public void showAllCustomerTest() throws Exception{
        ArrayList<legalEntity> le = this.sutShowAllCustomer.Run("AC");
        assertEquals(le.size(), 3);
        assertThrows(Exception.class, ()->this.sutShowAllCustomer.Run("AB"), "Error token");

    }

    @Test
    public void deleteRetailPriceListTest() throws Exception {
        assertThrows(Exception.class, ()->this.sutDeleteRetailPriceList.Run(new Pair<>("name2", "AD")), "Error. PriceList do not exist");
        assertEquals(this.sutDeleteRetailPriceList.Run(new Pair<>("name1", "AD")), 1);
        assertThrows(Exception.class, ()->this.sutDeleteRetailPriceList.Run(new Pair<>("name1", "AD")), "Error. PriceList do not exist");
        assertThrows(Exception.class, ()->this.sutDeleteRetailPriceList.Run(new Pair<>("name1", "AB")), "Error token");
    }

    @Test
    public void deleteProcurementPriceListTest() throws Exception {
        assertThrows(Exception.class, ()->this.sutDeleteProcurementPriceList.Run(new Pair<>("name2", "AB")), "Error. PriceList do not exist");
        assertEquals(this.sutDeleteProcurementPriceList.Run(new Pair<>("name3", "AB")), 1);
        assertThrows(Exception.class, ()->this.sutDeleteProcurementPriceList.Run(new Pair<>("name3", "AB")), "Error. PriceList do not exist");
        assertThrows(Exception.class, ()->this.sutDeleteProcurementPriceList.Run(new Pair<>("name3", "AC")), "Error token");
    }

    @Test
    public void DeleteWholesalePriceListTest() throws Exception {
        assertThrows(Exception.class, ()->this.sutDeleteWholesalePriceList.Run(new Pair<>("name1", "AC")), "Error. PriceList do not exist");
        assertEquals(this.sutDeleteWholesalePriceList.Run(new Pair<>("name2", "AC")), 1);
        assertThrows(Exception.class, ()->this.sutDeleteWholesalePriceList.Run(new Pair<>("name2", "AC")), "Error. PriceList do not exist");
        assertThrows(Exception.class, ()->this.sutDeleteWholesalePriceList.Run(new Pair<>("name2", "AB")), "Error token");
    }

}