package mephi.useCase;

import javafx.util.Pair;
import mephi.DAO.DepartmentDaoImpl;
import mephi.Dependencies.IRepositoryAutentification;
import mephi.Dependencies.IRepository;
import mephi.UserInputOutput.IUserInput;
import mephi.portal.*;
import mephi.structures.legalEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.ICheckFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class NegativeTestUser {
    private Repository repository;
    private RepositoryAutentification auth;
    private UserInput userInput;
    private CheckFormat checkFormat;
    private addNewCustomer sutAddNewCustomer;
    private addNewSupplier sutAddNewSupplier;
    private updateCustomer sutUpdateCustomer;
    private updateSupplier sutUpdateSupplier;
    private deleteCustomer sutDeleteCustomer;
    private deleteSupplier sutDeleteSupplier;
    private showAllSuppliers sutShowAllSuppliers;
    private showAllCustomer sutShowAllCustomer;

    class CheckFormat implements ICheckFormat {

        @Override
        public boolean checkLegalEntity(legalEntity le) {

            if (le.Name.length() >30 || le.Name.length() < 2){
                return false;
            }

            if (!le.Phone.matches("(\\+*)\\d{11}")){
                return false;
            }

            if (!le.Email.matches("[A-Za-z0-9]*@yandex\\.ru")){
                return false;
            }

            if (le.INN.length() >30 || le.INN.length() < 10){
                return false;
            }

            if (le.Address.length() >30 || le.Address.length() < 5){
                return false;
            }


            return true;

        }
    }

    private DepartmentDaoImpl Dao;
    class UserInput implements IUserInput {

        @Override
        public legalEntity getLegalEntity(String token) {
            legalEntity le=new legalEntity();
            if (token == "A1") {
                le.Address = "в";
                le.INN = "15790dfdsf43";
                le.Email = "com@yandex.ru";
                le.Phone = "+12345678934";
                le.Name = "Ира";
                return le;
            }
            if (token == "A2") {
                le.Address = "Москвa 10";
                le.INN = "1579";
                le.Email = "com@yandex.ru";
                le.Phone = "+12345678934";
                le.Name = "Ира";
                return le;
            }
            if (token == "A3") {
                le.Address = "Москвa 10";
                le.INN = "157943545433";
                le.Email = "com@yandex";
                le.Phone = "+12345678934";
                le.Name = "Ира";
                return le;
            }
            if (token == "A4") {
                le.Address = "Москвa 10";
                le.INN = "157943545433";
                le.Email = "com@yandex";
                le.Phone = "+12345678934";
                le.Name = "Ира";
                return le;
            }
            if (token == "A5") {
                le.Address = "Москвa 10";
                le.INN = "157943545433";
                le.Email = "com@yandex.ru";
                le.Phone = "+12345678934";
                le.Name = "И";
                return le;
            }
            return le;
        }

        @Override
        public legalEntity getUpdateLegalEntity(String token, legalEntity le) {
            if (token == "A1") {
                le.Address = "в";
                le.Email = "com@yandex.ru";
                le.Phone = "+12345678934";
                le.Name = "Ира";
                return le;
            }
            if (token == "A3") {
                le.Address = "Москвa 10";
                le.Email = "com@yandex";
                le.Phone = "+12345678934";
                le.Name = "Ира";
                return le;
            }
            if (token == "A4") {
                le.Address = "Москвa 10";
                le.Email = "com@yandex";
                le.Phone = "+12345678934";
                le.Name = "Ира";
                return le;
            }
            if (token == "A5") {
                le.Address = "Москвa 10";
                le.Email = "com@yandex.ru";
                le.Phone = "+12345678934";
                le.Name = "И";
                return le;
            }
            return le;
        }
    }
    class RepositoryAutentification implements IRepositoryAutentification{

        @Override
        public int check(Integer id) {
            if (id > 0 && id < 50){
                return 1;
            }
            if (id > 50 ){
                return 2;
            }
            return -1;
        }

        @Override
        public int check(String token, Integer i) throws Exception {
            return 0;
        }
    }

    class Repository implements IRepository {


        @Override
        public LegalEntity getLegalEntityById(Integer id) throws Exception {

            return null;
        }

        @Override
        public Integer addNewUser(LegalEntity legalEntity) throws Exception {
            if(Dao.getLegalEntityByINN(legalEntity.getINN())!=null){
                return 0;
            }
            Dao.addNewUser(legalEntity);
            return 1;
        }

        @Override
        public Integer deleteSupplierAndPriceLists(String INN) throws Exception {
            Integer i=0;
            for (LegalEntity le : Dao.getAllLegalEntity()){
                if (le.getINN() == INN){
                    Dao.deleteLegalEntity(i);
                    break;
                }
                i++;
            }
            ArrayList<PriceList> priceListSupplier = Dao.getAllPriceListsSuppliers().get(INN);
            for(PriceList p: priceListSupplier){
                Dao.getAllPriceLists().remove(p);
            }

            Dao.getAllPriceListsSuppliers().remove(INN);
            return i+1;
        }

        @Override
        public ArrayList<PriceList> getPriceListsSupplierByINN(String INN) throws Exception {
            return Dao.getAllPriceListsSuppliers().get(INN);
        }

        @Override
        public HashMap<String, ArrayList<PriceList>> getAllPriceListsSupplier() throws Exception {
            return Dao.getAllPriceListsSuppliers();
        }

        @Override
        public HashMap<String, ArrayList<PriceList>> getAllPriceListsCustomers() throws Exception {
            return Dao.getAllPriceListsCustomers();
        }


        @Override
        public Integer updateLegalEntity(LegalEntity oldLegalEntity, LegalEntity newLegalEntity) {
            return null;
        }

        @Override
        public ArrayList<LegalEntity> getAllSuppliers() throws Exception {
            ArrayList<LegalEntity> legalEntitySupplier = new ArrayList<>();
            for (LegalEntity le : Dao.getAllLegalEntity()){
                if(le.getRoleLegalEnt() == RoleLegalEntity.SUPPLIER){
                    legalEntitySupplier.add(le);
                }
            }
            return legalEntitySupplier;
        }

        @Override
        public ArrayList<LegalEntity> getAllCustomers() throws Exception {
            ArrayList<LegalEntity> legalEntityCustomer = new ArrayList<>();
            for (LegalEntity le : Dao.getAllLegalEntity()){
                if(le.getRoleLegalEnt() == RoleLegalEntity.CUSTOMER){
                    legalEntityCustomer.add(le);
                }
            }
            return legalEntityCustomer;
        }

        @Override
        public RoleLegalEntity getRoleLegalByINN(String inn) throws Exception {
            List<LegalEntity> legalEntities = Dao.getAllLegalEntity();
            for (LegalEntity le :legalEntities){
                if (le.getINN() == inn){
                    return le.getRoleLegalEnt();
                }
            }
            return null;
        }
        @Override
        public LegalEntity getLegalEntityByINN(String inn) throws Exception {
            return Dao.getLegalEntityByINN(inn);
        }

        @Override
        public Integer deleteCustomerAndPriceLists(String INN) throws Exception {
            Integer i=0;
            for (LegalEntity le : Dao.getAllLegalEntity()){
                if (le.getINN() == INN){
                    Dao.deleteLegalEntity(i);
                    break;
                }
                i++;
            }

            ArrayList<PriceList> priceListCustomer = Dao.getAllPriceListsCustomers().get(INN);
            for(PriceList p: priceListCustomer){
                Dao.getAllPriceLists().remove(p);

            }
            Dao.getAllPriceListsCustomers().remove(INN);
            return i+1;
        }

        @Override
        public Integer updateLegalEntity(LegalEntity legEntityObject) {
            return null;
        }

        @Override
        public ArrayList<ArrivalProducts> getAllArrivalProducts() {
            return null;
        }

        @Override
        public ArrayList<Document> getAllDocuments() {
            return null;
        }

        @Override
        public Integer deleteLegalEntity(LegalEntity legalEntityByINN) {
            return null;
        }

        @Override
        public Integer deletePriceListByName(String key) throws Exception {
            return null;
        }

        @Override
        public Integer getAllPriceLists() throws Exception {
            return null;
        }

        @Override
        public PriceList getPriceListByName(String key) {
            return null;
        }

        @Override
        public Integer addDepartment(Department department) {
            return null;
        }

        @Override
        public Integer addNewPriceList(PriceList priceList) {
            return null;
        }

        @Override
        public HashMap<Product, Integer> getProductsInWarehouse() {
            return null;
        }

        @Override
        public String getProductNameByCode(int code) {
            return null;
        }

        @Override
        public String getProductDescriptionByCode(int code) {
            return null;
        }

        @Override
        public String getProductBarcodeByCode(int code) {
            return null;
        }
    }
    @BeforeEach
    public void init(){
        this.repository = new Repository();
        this.userInput = new UserInput();
        this.auth = new RepositoryAutentification();
        this.checkFormat = new CheckFormat();
        this.sutAddNewCustomer = new addNewCustomer(this.auth, this.repository, this.userInput, this.checkFormat);
        this.sutAddNewSupplier = new addNewSupplier(this.auth, this.repository, this.userInput, this.checkFormat);
        this.sutUpdateCustomer = new updateCustomer(this.auth, this.repository, this.userInput, this.checkFormat);
        this.sutUpdateSupplier = new updateSupplier(this.auth, this.repository, this.userInput, this.checkFormat);
        this.sutDeleteCustomer = new deleteCustomer(this.auth, this.repository);
        this.sutDeleteSupplier = new deleteSupplier(this.auth, this.repository);
        this.sutShowAllCustomer = new showAllCustomer(this.auth, this.repository);
        this.sutShowAllSuppliers = new showAllSuppliers(this.auth, this.repository);
        this.Dao = new DepartmentDaoImpl();
    }

    @Test
    public void addNewCustomerBadTest() throws Exception {
        legalEntity le1 = this.sutAddNewCustomer.getAddCustomer("A1");
        legalEntity le = this.sutAddNewCustomer.Run(new Pair<>(le1, "A1"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutAddNewCustomer.getAddCustomer("A2");
        le = this.sutAddNewCustomer.Run(new Pair<>(le1, "A2"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutAddNewCustomer.getAddCustomer("A3");
        le = this.sutAddNewCustomer.Run(new Pair<>(le1, "A3"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutAddNewCustomer.getAddCustomer("A4");
        le = this.sutAddNewCustomer.Run(new Pair<>(le1, "A4"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutAddNewCustomer.getAddCustomer("A5");
        le = this.sutAddNewCustomer.Run(new Pair<>(le1, "A5"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
    }

    @Test
    public void addNewSupplierBadTest() throws Exception {

        legalEntity le1 = this.sutAddNewSupplier.getAddSupplier("A1");
        legalEntity le = this.sutAddNewSupplier.Run(new Pair<>(le1, "A1"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutAddNewSupplier.getAddSupplier("A2");
        le = this.sutAddNewSupplier.Run(new Pair<>(le1, "A2"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutAddNewSupplier.getAddSupplier("A3");
        le = this.sutAddNewSupplier.Run(new Pair<>(le1, "A3"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutAddNewSupplier.getAddSupplier("A4");
        le = this.sutAddNewSupplier.Run(new Pair<>(le1, "A4"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutAddNewSupplier.getAddSupplier("A5");
        le = this.sutAddNewSupplier.Run(new Pair<>(le1, "A5"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
    }

    @Test
    public void updateNewCustomerBadTest() throws Exception {
        legalEntity le1 = this.sutUpdateCustomer.getUpdateCustomer("A1", "085789");
        legalEntity le = this.sutUpdateCustomer.Run(new Pair<>(le1, "A1"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutUpdateCustomer.getUpdateCustomer("A2", "085789");
        le = this.sutUpdateCustomer.Run(new Pair<>(le1, "A2"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutUpdateCustomer.getUpdateCustomer("A3", "085789");
        le = this.sutUpdateCustomer.Run(new Pair<>(le1, "A3"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutUpdateCustomer.getUpdateCustomer("A4", "085789");
        le = this.sutUpdateCustomer.Run(new Pair<>(le1, "A4"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutUpdateCustomer.getUpdateCustomer("A5", "085789");
        le = this.sutUpdateCustomer.Run(new Pair<>(le1, "A5"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
    }

    @Test
    public void updateNewSupplierBadTest() throws Exception {
        legalEntity le1 = this.sutUpdateCustomer.getUpdateCustomer("A1", "085ролнн");
        legalEntity le = this.sutUpdateCustomer.Run(new Pair<>(le1, "A1"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutUpdateCustomer.getUpdateCustomer("A2", "085ролнн");
        le = this.sutUpdateCustomer.Run(new Pair<>(le1, "A2"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutUpdateCustomer.getUpdateCustomer("A3", "085ролнн");
        le = this.sutUpdateCustomer.Run(new Pair<>(le1, "A3"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutUpdateCustomer.getUpdateCustomer("A4", "085ролнн");
        le = this.sutUpdateCustomer.Run(new Pair<>(le1, "A4"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
        le1 = this.sutUpdateCustomer.getUpdateCustomer("A5", "085ролнн");
        le = this.sutUpdateCustomer.Run(new Pair<>(le1, "A5"));
        assertEquals(le.statusReturn, "NOT SUCCESS");
    }
}