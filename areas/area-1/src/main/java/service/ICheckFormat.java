package service;

import mephi.structures.legalEntity;

public interface ICheckFormat {
    boolean checkLegalEntity(legalEntity le) throws Exception;

}
