package mephi.DAO;

import com.mongodb.client.MongoClient;
import mephi.portal.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.mongodb.*;
public class MongoImpl implements DepartmentDao {

    MongoClient client;
    public MongoImpl(){

    }
    @Override
    public void addDepartment(Department d) throws Exception {

    }

    @Override
    public void updateDepartment(String name, String newName) throws Exception {

    }

    @Override
    public List<Department> getAllDepartment() throws Exception {
        return null;
    }

    @Override
    public Department getDepartmentByName(String name) throws Exception {
        return null;
    }

    @Override
    public ArrayList<LegalEntity> getAllLegalEntity() throws Exception {
        return null;
    }

    @Override
    public Integer addNewUser(LegalEntity le) throws Exception {
        return null;
    }

    @Override
    public Integer deleteLegalEntity(Integer id) throws Exception {
        return null;
    }

    @Override
    public LegalEntity getLegalEntityByINN(String inn) throws Exception {
        return null;
    }

    @Override
    public ArrayList<PriceList> getAllPriceLists() throws Exception {
        return null;
    }

    @Override
    public PriceList getPriceListByName(String name) throws Exception {
        return null;
    }

    @Override
    public HashMap<String, ArrayList<PriceList>> getAllPriceListsSuppliers() throws Exception {
        return null;
    }

    @Override
    public HashMap<String, ArrayList<PriceList>> getAllPriceListsCustomers() throws Exception {
        return null;
    }

    @Override
    public ArrayList<ArrivalProducts> getAllArrivalProducts() throws Exception {
        return null;
    }

    @Override
    public ArrayList<Document> getAllDocuments() throws Exception {
        return null;
    }
}
