package mephi.DAO;

import mephi.portal.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface DepartmentDao {
   void addDepartment(Department d) throws Exception;
   void updateDepartment(String name, String newName) throws Exception;
   List<Department> getAllDepartment() throws Exception;
   Department getDepartmentByName(String name) throws Exception;
   ArrayList<LegalEntity> getAllLegalEntity() throws Exception;
   public Integer addNewUser(LegalEntity le) throws Exception;
   public Integer deleteLegalEntity(Integer id) throws Exception;
   LegalEntity getLegalEntityByINN(String inn) throws Exception;
   ArrayList<PriceList> getAllPriceLists() throws Exception;
   PriceList getPriceListByName(String name) throws Exception;

   HashMap<String, ArrayList<PriceList>> getAllPriceListsSuppliers() throws Exception;
   HashMap<String, ArrayList<PriceList>> getAllPriceListsCustomers() throws Exception;
   ArrayList <ArrivalProducts> getAllArrivalProducts() throws Exception;
   ArrayList <Document> getAllDocuments() throws Exception;

}
