package mephi.DAO;

import mephi.portal.*;

import java.util.*;

import static mephi.portal.TypePriceList.*;
import static mephi.portal.RoleLegalEntity.*;

public class DepartmentDaoImpl implements DepartmentDao {
    private List<Department> departments;
    private ArrayList<PriceList> priceLists;
    private ArrayList<LegalEntity> legalEntity;
    private ArrayList<ArrivalProducts> arrivalProduct;
    private ArrayList<Document> documents;

    public DepartmentDaoImpl(){
        this.departments = Arrays.asList(
                new Department("Дикси","Москва"),
                new Department("Пятерочка","Москва"),
                new Warehouse("Верный","Санкт-Петербург"),
                new Department("Фея","Видное"),
                new Warehouse("Магнит","Домодедово")
        );


        this.priceLists = new ArrayList<PriceList>();
        this.priceLists.add(new PriceList("name1", Retail));
        this.priceLists.add(new PriceList("name2", Wholesale));
        this.priceLists.add(new PriceList("name3", Procurement));
        this.priceLists.add(new PriceList("name5", Wholesale));
        this.priceLists.add(new PriceList("name6", Procurement));
        this.priceLists.add(new PriceList("name7", Procurement));
        this.priceLists.add(new PriceList("name8", Procurement));
        this.priceLists.add(new PriceList("name9", Procurement));
        this.priceLists.add(new PriceList("name10", Procurement));
        this.priceLists.add(new PriceList("name11", Wholesale));
        this.priceLists.add(new PriceList("name12", Wholesale));

          legalEntity = new ArrayList<LegalEntity>();
          legalEntity.add(new LegalEntity("Вася", "1111", "com", "Москва", "688", SUPPLIER));
          legalEntity.add(new LegalEntity("Петя", "+12345678454", "sdds@com.ru", "Питербург", "085789", CUSTOMER));
          legalEntity.add(new LegalEntity("Катя", "1135", "com", "Лондон", "085нн", CUSTOMER));
          legalEntity.add(new LegalEntity("Мила", "1168", "com", "Сочи", "085ролнн", SUPPLIER));
          legalEntity.add(new LegalEntity("Кеша", "1118798", "com", "Мытищи", "085ндждн", CUSTOMER));

          ArrayList<PriceList> PriceListProcurement1 = new ArrayList<PriceList>();
          PriceListProcurement1.add(this.priceLists.get(2));
          PriceListProcurement1.add(this.priceLists.get(4));
          ArrayList<PriceList> PriceListProcurement2 = new ArrayList<PriceList>();
          PriceListProcurement2.add(this.priceLists.get(5));
          PriceListProcurement2.add(this.priceLists.get(6));

          ArrayList<PriceList> PriceListWholesale1 = new ArrayList<PriceList>();
          PriceListWholesale1.add(this.priceLists.get(1));
          PriceListWholesale1.add(this.priceLists.get(3));
          arrivalProduct = new ArrayList<ArrivalProducts>();
          arrivalProduct.add(new ArrivalProducts (legalEntity.get(0), true, "Дикси"));
          arrivalProduct.add(new ArrivalProducts (legalEntity.get(2), true, "Пятерочка"));
          arrivalProduct.add(new ArrivalProducts (legalEntity.get(3), true, "Фея"));
          arrivalProduct.add(new ArrivalProducts (legalEntity.get(0), false, "Видное"));
          arrivalProduct.add(new ArrivalProducts (legalEntity.get(4), true, "Магнит"));
          arrivalProduct.add(new ArrivalProducts (legalEntity.get(4), true, "Пятерочка"));
          arrivalProduct.add(new ArrivalProducts (legalEntity.get(1), false, "Фея"));



        List<Product> products = Arrays.asList(
                new Product("Колбаса", 1, "12345"),
                new Product("Сыр", 2, "12346"),
                new Product("Хлеб", 3, "12347"),
                new Product("Йогурт", 4, "12348"),
                new Product("печенье", 5, "12349"),
                new Product("Курица", 6, "12310"),
                new Product("Вино", 7, "12311"),
                new Product("Торт", 8, "12312"),
                new Product("Масло", 9, "12313"),
                new Product("Молоко", 10, "12314"),
                new Product("Мука", 11, "12315"),
                new Product("Сыр", 12, "1245"),
                new Product("Фрукт", 13, "120987"),
                new Product("Овощ", 14, "12543")
                );
        Random rand = new Random();
        for(PriceList p: priceLists) {
            int c = rand.nextInt(7)+1;
            for (int i =0; i<c; i++) {
                p.addProductPrice(products.get(rand.nextInt(13)), rand.nextInt(100)+1 * rand.nextFloat());
            }
        }
        for(ArrivalProducts good: arrivalProduct) {
            int c = rand.nextInt(7)+1;
            for (int i =0; i<c; i++) {
                PriceList priceList = priceLists.get(rand.nextInt(5));
                HashMap<Product, Float> h = priceList.getPrices();
                ArrayList <Product> keys = new ArrayList<>(h.keySet());
                int size = keys.size();
                if (size == 1){
                    Product p = keys.get(0);
                    good.AddCountForProduct(priceList, p ,i+1);
                    continue;
                }
                Product p = keys.get(rand.nextInt(size-1));
                good.AddCountForProduct(priceList, p ,i+1);
            }
        }
        for(Department d : departments){
            d.setProductCatalog("catalog");
            int c = rand.nextInt(7)+1;
            for (int i =0; i<c; i++) {
                d.setProducts(arrivalProduct.get(rand.nextInt(6)));
            }
        }
        documents = new ArrayList<Document>();
        documents.add(new Document(1, TypeDocument.Purchase, arrivalProduct.get(0)));
        documents.add(new Document(2, TypeDocument.Purchase, arrivalProduct.get(1)));
        documents.add(new Document(3, TypeDocument.Purchase, arrivalProduct.get(2)));
        documents.add(new Document(4, TypeDocument.Supply, arrivalProduct.get(3)));
        documents.add(new Document(5, TypeDocument.Supply, arrivalProduct.get(4)));
        documents.add(new Document(6, TypeDocument.Supply, arrivalProduct.get(5)));
        documents.add(new Document(7, TypeDocument.Supply, arrivalProduct.get(6)));


    }
    @Override
    public void addDepartment(Department d) throws Exception {
        departments.add(d);
    }

    @Override
    public void updateDepartment(String name, String newName) throws Exception {

    }

    @Override
    public List<Department> getAllDepartment() throws Exception {
        return departments;
    }

    @Override
    public Department getDepartmentByName(String name) throws Exception {
        return departments.stream().filter(department -> department.getName().equals(name))
                .findAny().orElse(null);

    }
    public ArrayList<PriceList> getAllPriceLists() throws Exception {
        return priceLists;
    }

    @Override
    public PriceList getPriceListByName(String name) throws Exception {
        return priceLists.stream().filter(pl -> pl.getName().equals(name))
                .findAny().orElse(null);
    }

    @Override
    public HashMap<String, ArrayList<PriceList>> getAllPriceListsSuppliers() throws Exception {
        return null;
    }

    @Override
    public HashMap<String, ArrayList<PriceList>> getAllPriceListsCustomers() throws Exception {
        return null;
    }

    @Override
    public ArrayList<ArrivalProducts> getAllArrivalProducts() throws Exception {
        return arrivalProduct;
    }

    @Override
    public ArrayList<Document> getAllDocuments() throws Exception {
        return documents;
    }


    public ArrayList<LegalEntity> getAllLegalEntity() throws Exception {
        return legalEntity;
    }
    public Integer addNewUser(LegalEntity le) throws Exception{
        legalEntity.add(le);
        return 1;
    }

    @Override
    public Integer deleteLegalEntity(Integer id) throws Exception {
        legalEntity.remove(legalEntity.get(id));
        return 1;
    }

    public LegalEntity getLegalEntityByINN(String inn) throws Exception{
        return legalEntity.stream().filter(le -> le.getINN().equals(inn))
                .findAny().orElse(null);
    }
}
