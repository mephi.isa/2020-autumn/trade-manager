package mephi;

public interface IUseCase<TInput, TOutput> {
    TOutput Run( TInput input) throws Exception;
}
