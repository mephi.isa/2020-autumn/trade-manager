package mephi.UserInputOutput;

import mephi.structures.legalEntity;

public interface IUserInput {
    legalEntity getLegalEntity(String token);

    legalEntity getUpdateLegalEntity(String token, legalEntity legEntity);
}
