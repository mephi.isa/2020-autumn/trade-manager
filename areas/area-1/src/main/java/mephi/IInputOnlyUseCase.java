package mephi;

public interface IInputOnlyUseCase<TInput> {
    void Run(TInput input);
}
