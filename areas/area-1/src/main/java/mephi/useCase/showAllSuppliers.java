package mephi.useCase;

import mephi.Dependencies.IRepositoryAutentification;
import mephi.Dependencies.IRepository;
import mephi.IUseCase;
import mephi.portal.LegalEntity;
import mephi.structures.legalEntity;

import java.util.ArrayList;

public class showAllSuppliers<TInput, TOutput> implements IUseCase<String, ArrayList<legalEntity>> {

    private final IRepositoryAutentification auth;
    private final IRepository Storage;

    public showAllSuppliers(IRepositoryAutentification auth, IRepository userStorage){
        this.auth = auth;
        this.Storage = userStorage;

    }
    @Override
    public ArrayList<legalEntity> Run(String token) throws Exception {
        this.auth.check(token, 1);
        ArrayList<legalEntity> suppliers = new ArrayList<>();
        ArrayList<LegalEntity> legalEntities = this.Storage.getAllSuppliers();
        for (LegalEntity le : legalEntities){
            suppliers.add(createLegalEntity(le));
        }
        return suppliers;
    }
    legalEntity createLegalEntity(LegalEntity le){
        legalEntity structureLegalEntity = new legalEntity();
        structureLegalEntity.Address=le.getAddress();
        structureLegalEntity.Name=le.getName();
        structureLegalEntity.INN=le.getINN();
        structureLegalEntity.Email=le.getEmail();
        structureLegalEntity.Phone=le.getPhone();
        return structureLegalEntity;
    }

}
