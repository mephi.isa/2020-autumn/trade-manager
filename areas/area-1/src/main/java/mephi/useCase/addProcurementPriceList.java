package mephi.useCase;

import javafx.util.Pair;
import mephi.Dependencies.IRepository;
import mephi.Dependencies.IRepositoryAutentification;
import mephi.IUseCase;
import mephi.portal.PriceList;
import mephi.portal.Product;
import mephi.portal.TypePriceList;
import mephi.structures.product;

import java.util.HashMap;
import java.util.Map;

public class addProcurementPriceList<TInput, TOutput> implements
        IUseCase<Pair<Pair<String, HashMap<product,Float>>,Integer>, Integer> {

    private IRepositoryAutentification auth;
    private IRepository Storage;
    public addProcurementPriceList(IRepositoryAutentification auth, IRepository Storage){
        this.auth = auth;
        this.Storage = Storage;
    }
    @Override
    public Integer Run(Pair<Pair<String, HashMap<product, Float>>, Integer> p) throws Exception {
        if (auth.check(p.getValue()) == -1){
            return 0;
        }

        HashMap<product, Float> orders = p.getKey().getValue();
        PriceList priceList = new PriceList(p.getKey().getKey(), TypePriceList.Procurement);
        for (Map.Entry<product, Float> order : orders.entrySet()){

            Product prod = new Product(order.getKey().Name, order.getKey().Description,
                    order.getKey().Code, order.getKey().Barcode);
            priceList.addProductPrice(prod, order.getValue());

        }
        return this.Storage.addNewPriceList(priceList);
    }
}
