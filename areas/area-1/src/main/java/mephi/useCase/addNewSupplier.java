package mephi.useCase;

import javafx.util.Pair;
import mephi.Dependencies.IRepositoryAutentification;
import mephi.Dependencies.IRepository;
import mephi.IUseCase;
import mephi.UserInputOutput.IUserInput;
import mephi.portal.LegalEntity;
import mephi.portal.RoleLegalEntity;
import mephi.structures.legalEntity;
import service.ICheckFormat;

public class addNewSupplier<TInput, TOutput> implements IUseCase<Pair<legalEntity, String>, legalEntity> {
    private IRepositoryAutentification auth;
    private IRepository Storage;
    private ICheckFormat checkFormat;
    private IUserInput userInput;
    private String token;
    private String Name;
    private String Phone;
    private String Email;
    private String Address;
    private String INN;
    private legalEntity legEntity;
    public addNewSupplier(IRepositoryAutentification auth, IRepository Storage,
                          IUserInput userInput, ICheckFormat checkFormat) {
        this.auth = auth;
        this.Storage = Storage;
        this.userInput = userInput;
        this.checkFormat = checkFormat;
    }

/*
    public Integer Run(Pair<legalEntity, Integer> p) throws Exception {
        Integer check = auth.check(p.getValue());
        if (check == -1 || check != 2) {
            return -1;
        }

        legalEntity legE = p.getKey();
        if (this.userStorage.getRoleLegalByINN(legE.INN) == null) {
            return this.userStorage.addNewUser(new LegalEntity(legE.Name, legE.Phone, legE.Email,
                    legE.Address, legE.INN, RoleLegalEntity.SUPPLIER));
        }
        return 0;

    }

*/
    public legalEntity getAddSupplier(String token) throws Exception {
        // шаг первый проверка токина: право на добавление поставщика
        this.auth.check(token, 1);
        //если права нет, то ошибка иначе структура
        this.token = token;

        legEntity = this.userInput.getLegalEntity(token);
        this.Email = legEntity.Email;
        this.INN = legEntity.INN;
        this.Address = legEntity.Address;
        this.Name = legEntity.Name;
        this.Phone = legEntity.Phone;
        return legEntity;
    }
    @Override
    public legalEntity Run(Pair<legalEntity, String> p) throws Exception {

        //Шаг первый. Проверка того, что токен запускал метод getAddSupplier
        if (!p.getValue().equals(this.token)){
            throw new Exception("Error token");
        }

        // и что структура была создана
        if (!p.getKey().equals(this.legEntity)){
            throw new Exception("Error object");
        }
        if (!checkContains()){
            throw new Exception("Error contains");
        }
        // если не запускал, то ошибка иначе следующий шаг
        // шаг второй. Проверка введены данных. Если не пройдена, то возвращае заполн структуру
        if(!this.checkFormat.checkLegalEntity(p.getKey())){
            p.getKey().statusReturn = "NOT SUCCESS";
            return p.getKey();
        }
        // Если пройдена, то создаем объект поставщик
        LegalEntity legEntityObject = new LegalEntity(p.getKey().Name, p.getKey().Phone, p.getKey().Email,
                p.getKey().Address, p.getKey().INN, RoleLegalEntity.SUPPLIER);
        // Шаг третий. После создания записать поставщика(объект) в базу
        if (this.Storage.addNewUser(legEntityObject) == 0){
            legEntityObject = null;
            throw new Exception("Error. LegalEntity did not save");
        }
        //Если не успешна, то удаляем объект и возвращаем ошибку
        //Если успешно, то обновить объект данными из базы
        legEntityObject = this.Storage.getLegalEntityByINN(p.getKey().INN);
        //вернуть информацию об успешном добавлении
        p.getKey().statusReturn = "SUCCESS";
        return p.getKey();
    }
    private boolean checkContains(){
        if(legEntity.INN!=INN || legEntity.Phone!=Phone || legEntity.Name!=Name || legEntity.Email!=Email
            || legEntity.Address!=Address){
            return false;
        }
        return true;
    }
}


