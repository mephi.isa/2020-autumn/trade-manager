package mephi.useCase;

import javafx.util.Pair;
import mephi.Dependencies.*;
import mephi.IUseCase;
import mephi.UserInputOutput.IUserInput;
import mephi.portal.*;
import service.ICheckFormat;

import java.util.ArrayList;

public class deleteCustomer<TInput, TOutput> implements IUseCase<Pair<String, String>, Integer> {


    private final IRepositoryAutentification auth;
    private final IRepository Storage;

    public deleteCustomer(IRepositoryAutentification auth, IRepository Storage) {
        this.auth = auth;
        this.Storage = Storage;
    }

    @Override
    public Integer Run(Pair<String, String> p) throws Exception {
        // шаг первый проверка токина: право на добавление поставщика
        this.auth.check(p.getValue(), 2);
        //если права нет, то ошибка иначе структура
        LegalEntity legEnt = this.Storage.getLegalEntityByINN(p.getKey());
        if( legEnt == null || legEnt.getRoleLegalEnt() != RoleLegalEntity.CUSTOMER){
            throw new Exception("Error. LegalEntity do not exist");
        }
        this.Storage.deleteLegalEntity(this.Storage.getLegalEntityByINN(p.getKey()));
        ArrayList<Document> documentObj = this.Storage.getAllDocuments();
        for (Document doc : documentObj){
            LegalEntity le = doc.getArrProducts().getLegalEnt();
            if (le.getINN() == p.getKey()){
                doc.getArrProducts().setLegalEnt(null);
                for(PriceList pl : doc.getArrProducts().getPriceLists()){
                    doc.getArrProducts().deletePriceList(pl);
                }

            }
        }
        return 1;

    }
}
