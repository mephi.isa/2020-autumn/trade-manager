package mephi.useCase;

import javafx.util.Pair;
import mephi.Dependencies.IRepository;
import mephi.Dependencies.IRepositoryAutentification;
import mephi.IUseCase;
import mephi.portal.PriceList;
import mephi.portal.Product;
import mephi.portal.TypePriceList;
import mephi.structures.product;

import java.util.HashMap;
import java.util.Map;

public class addWholesalePriceList<TInput, TOutput> implements
        IUseCase<Pair<Pair<String, HashMap<product,Float>>,Integer>, Integer> {

    private IRepositoryAutentification auth;
    private IRepository Storage;
    public addWholesalePriceList(IRepositoryAutentification auth, IRepository Storage){
        this.auth = auth;
        this.Storage = Storage;

    }
    @Override
    public Integer Run(Pair<Pair<String, HashMap<product, Float>>, Integer> p) throws Exception {
        Integer check = auth.check(p.getValue());
        if (check == -1 || check != 1){
            return -1;
        }
        PriceList pl = this.Storage.getPriceListByName(p.getKey().getKey());

        if (pl != null && pl.getType() == TypePriceList.Wholesale){
            return 0;
        }

        HashMap<product, Float> orders = p.getKey().getValue();
        PriceList priceList = new PriceList(p.getKey().getKey(), TypePriceList.Wholesale);
        for (Map.Entry<product, Float> order : orders.entrySet()){

            Product prod = new Product(order.getKey().Name, order.getKey().Description,
                    order.getKey().Code, order.getKey().Barcode);
            priceList.addProductPrice(prod, order.getValue());

        }
        return this.Storage.addNewPriceList(priceList);
    }
}
