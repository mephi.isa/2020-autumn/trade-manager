package mephi.useCase;

import javafx.util.Pair;
import mephi.Dependencies.IRepositoryAutentification;
import mephi.Dependencies.IRepository;
import mephi.IUseCase;
import mephi.UserInputOutput.IUserInput;
import mephi.portal.*;
import mephi.structures.legalEntity;
import service.ICheckFormat;

public class updateCustomer<TInput, TOutput> implements IUseCase<Pair<legalEntity, String>, legalEntity> {
    private IRepositoryAutentification auth;
    private IRepository Storage;
    private ICheckFormat checkFormat;
    private IUserInput userInput;
    private String token;
    private String Name;
    private String Phone;
    private String Email;
    private String Address;
    private String INN;
    private legalEntity legEntity;
    private LegalEntity legEntityObject;
    public updateCustomer(IRepositoryAutentification auth, IRepository Storage,
                          IUserInput userInput, ICheckFormat checkFormat) {
        this.auth = auth;
        this.Storage = Storage;
        this.userInput = userInput;
        this.checkFormat = checkFormat;
        this.legEntity = new legalEntity();
    }

    public legalEntity getUpdateCustomer(String token, String INN) throws Exception {
        this.auth.check(token, 2);
        //если права нет, то ошибка иначе структура
        this.token = token;
        legEntityObject = this.Storage.getLegalEntityByINN(INN);
        legEntity.Phone = legEntityObject.getPhone();
        legEntity.INN = legEntityObject.getINN();
        legEntity.Name = legEntityObject.getName();
        legEntity.Address = legEntityObject.getAddress();
        legEntity.Email = legEntityObject.getEmail();

        legEntity = this.userInput.getUpdateLegalEntity(token, legEntity);
        this.Email = legEntity.Email;
        this.INN = INN;
        this.Address = legEntity.Address;
        this.Name = legEntity.Name;
        this.Phone = legEntity.Phone;
        return legEntity;
    }


    @Override
    public legalEntity Run(Pair<legalEntity, String> p) throws Exception {

        if (!p.getValue().equals(this.token)){
            throw new Exception("Error token");
        }

        // и что структура была создана
        if (!p.getKey().equals(this.legEntity)){
            throw new Exception("Error object");
        }

        // если не запускал, то ошибка иначе следующий шаг
        // шаг второй. Проверка введены данных. Если не пройдена, то возвращае заполн структуру
        if(!this.checkFormat.checkLegalEntity(p.getKey())){
            p.getKey().statusReturn = "NOT SUCCESS";
            return p.getKey();
        }
        // Если пройдена, то обновляем объект поставщик
        legEntityObject.setPhone(p.getKey().Phone);
        legEntityObject.setAddress(p.getKey().Address);
        legEntityObject.setEmail(p.getKey().Email);
        legEntityObject.setName(p.getKey().Name);

        this.Storage.updateLegalEntity(legEntityObject);


        legEntityObject = this.Storage.getLegalEntityByINN(p.getKey().INN);
        //вернуть информацию об успешном добавлении
        p.getKey().statusReturn = "SUCCESS";
        return p.getKey();
    }

    private boolean checkContains(){
        if(legEntity.INN!=INN || legEntity.Phone!=Phone || legEntity.Name!=Name || legEntity.Email!=Email
                || legEntity.Address!=Address){
            return false;
        }
        return true;
    }
}
