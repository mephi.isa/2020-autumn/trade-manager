package mephi.useCase;

import javafx.util.Pair;
import mephi.Dependencies.IRepository;
import mephi.Dependencies.IRepositoryAutentification;
import mephi.IUseCase;
import mephi.portal.PriceList;
import mephi.portal.TypePriceList;

public class deleteRetailPriceList<TInput, TOutput> implements IUseCase<Pair<String, String>,Integer> {
    private IRepositoryAutentification auth;
    private IRepository Storage;
    public deleteRetailPriceList(IRepositoryAutentification auth, IRepository Storage){
        this.auth = auth;
        this.Storage = Storage;
    }
    @Override
    public Integer Run(Pair<String, String> p) throws Exception {
        this.auth.check(p.getValue(), 3);
        PriceList pl = this.Storage.getPriceListByName(p.getKey());
        if (pl == null || pl.getType() != TypePriceList.Retail){
            throw new Exception("Error. PriceList do not exist");
        }
        return this.Storage.deletePriceListByName(p.getKey());
    }
}
