package mephi.useCase;

import mephi.Dependencies.*;

import mephi.IUseCase;
import mephi.portal.Product;
import mephi.structures.product;

import java.util.HashMap;
import java.util.Map;

public class ShowWarehouse implements IUseCase <Integer, HashMap<product, Integer>> {
    private IRepositoryAutentification auth;
    private IRepository Storage;
    public ShowWarehouse(IRepositoryAutentification auth, IRepository Storage){
        this.auth = auth;
        this.Storage = Storage;
    }
    @Override
    public HashMap<product, Integer>  Run(Integer id) {

        if (auth.check(id) == -1){
            return null;
        }
        HashMap<Product, Integer> productsOfWarehouse = this.Storage.getProductsInWarehouse();
        HashMap<product, Integer> products = new HashMap<>();
        for (Map.Entry<Product, Integer> prod: productsOfWarehouse.entrySet()){
            products.put(createNewProductStructureByCode(prod),prod.getValue());
        }
        return products;
    }
    product createNewProductStructureByCode(Map.Entry<Product, Integer> prod){
        product newProduct = new product();
        newProduct.Code = prod.getKey().getCode();
        newProduct.Name = this.Storage.getProductNameByCode(prod.getKey().getCode());
        newProduct.Description = this.Storage.getProductDescriptionByCode(prod.getKey().getCode());
        newProduct.Barcode = this.Storage.getProductBarcodeByCode(prod.getKey().getCode());
        return newProduct;
    }
}
