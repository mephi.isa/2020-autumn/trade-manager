package mephi.useCase;

import javafx.util.Pair;
import mephi.Dependencies.*;
import mephi.IUseCase;
import mephi.portal.Department;

public class addDepartmentOfCompany<TInput, TOutput> implements IUseCase<Pair<Pair<String, String>, Integer>, Integer> {
    private IRepositoryAutentification auth;
    private IRepository Storage;

    public addDepartmentOfCompany(IRepositoryAutentification auth,
                                  IRepository Storage){
        this.auth = auth;
        this.Storage = Storage;
    }
    @Override
    public Integer Run(Pair<Pair<String, String>, Integer> p) {

        if (auth.check(p.getValue()) == -1){
            return 0;
        }
        return this.Storage.addDepartment(new Department(p.getKey().getKey(), p.getKey().getValue()));
    }
}
