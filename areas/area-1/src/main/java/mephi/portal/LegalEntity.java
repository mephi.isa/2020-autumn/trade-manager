package mephi.portal;

public class LegalEntity {
    private String Name;
    private String Phone;
    private String Email;
    private String Address;
    private String INN;
    private RoleLegalEntity RoleLegalEnt;

    public LegalEntity(String name, String phone, String email, String address, String INN, RoleLegalEntity roleLegalEnt) {
        Name = name;
        Phone = phone;
        Email = email;
        Address = address;
        this.INN = INN;
        RoleLegalEnt = roleLegalEnt;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getINN() {
        return INN;
    }

    public void setINN(String INN) {
        this.INN = INN;
    }

    public RoleLegalEntity getRoleLegalEnt() {
        return RoleLegalEnt;
    }

    public void setRoleLegalEnt(RoleLegalEntity roleLegalEnt) {
        RoleLegalEnt = roleLegalEnt;
    }
}
