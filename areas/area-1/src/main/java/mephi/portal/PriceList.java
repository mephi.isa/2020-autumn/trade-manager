package mephi.portal;

import java.util.HashMap;

public class PriceList {
    private String Name;
    private HashMap<Product, Float> Prices;
    private TypePriceList Type;

    public PriceList(String name, TypePriceList type) {
        Name = name;
        Type = type;
        Prices = new HashMap<>();
    }

    public String getName() {
        return Name;
    }

    public HashMap<Product, Float> getPrices() {
        return Prices;
    }

    public TypePriceList getType() {
        return Type;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setType(TypePriceList type) {
        Type = type;
    }

    public boolean addProductPrice(Product product, Float price) {
        if (price >= 0) {
            Prices.put(product, price);
            return true;
        }
        return false;
    }
}
