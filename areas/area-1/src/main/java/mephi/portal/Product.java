package mephi.portal;

public class Product {
    private String Name;
    private String Description;
    private int Code;
    private String Barcode;

    public Product(String name, int code, String barcode) {
        Name = name;
        Code = code;
        Barcode = barcode;
    }

    public Product(String name, String description, int code, String barcode) {
        Name = name;
        Description = description;
        Code = code;
        Barcode = barcode;
    }

}
