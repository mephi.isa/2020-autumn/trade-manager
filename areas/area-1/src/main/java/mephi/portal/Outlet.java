package mephi.portal;

import javafx.util.Pair;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

public class Outlet extends Department {
    private ArrayList<Cash> Cashes;
    private ArrayList<Cheque> Cheques;

    class Cash {
        private int Id;
        private String SerialNumber;

        private Cash(int id, String serialNumber) {
            Id = id;
            SerialNumber = serialNumber;
        }

        public int getId() {
            return Id;
        }

        public String getSerialNumber() {
            return SerialNumber;
        }
    }

    class Cheque {
        private int Id;
        private HashMap<Product, Pair<Float, Integer>> Products;
        private LocalDate Date;
        private LocalTime Time;
        private Cash Cash;

        private Cheque(int id, HashMap<Product, Pair<Float, Integer>> products, LocalDate date, LocalTime time, Cash cash) {
            Id = id;
            Products = products;
            Date = date;
            Time = time;
            Cash = cash;
        }

        public int getId() {
            return Id;
        }

        public HashMap<Product, Pair<Float, Integer>> getProducts() {
            return Products;
        }

        public LocalDate getDate() {
            return Date;
        }

        public LocalTime getTime() {
            return Time;
        }

        public Cash getCash() {
            return Cash;
        }
    }

    public Outlet(String name, String address) {
        super(name, address);
        Cashes = new ArrayList<>();
        Cheques = new ArrayList<>();
    }

    public ArrayList<Cash> getCashes() {
        return Cashes;
    }

    public ArrayList<Cheque> getCheques() {
        return Cheques;
    }

    public void addCash(int id, String serialNumber) {
        Cashes.add(new Cash(id, serialNumber));
    }

    public void addCheque(int id, HashMap<Product, Pair<Float, Integer>> products, LocalDate date, LocalTime time, Cash cash) {
        Cheques.add(new Cheque(id, products, date, time, cash));
    }
}
