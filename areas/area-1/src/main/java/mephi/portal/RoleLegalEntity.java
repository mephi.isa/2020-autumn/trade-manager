package mephi.portal;

public enum RoleLegalEntity {
    SUPPLIER,
    CUSTOMER
}
