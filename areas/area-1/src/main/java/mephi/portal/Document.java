package mephi.portal;

import java.time.LocalDate;
import java.time.LocalTime;

public class Document {
    private int Number;
    private LocalDate Date;
    private LocalTime Time;
    private TypeDocument Type;
    private ArrivalProducts ArrProducts;

    public Document(int number, LocalDate date, LocalTime time, TypeDocument type, ArrivalProducts arrProducts) {
        Number = number;
        Date = date;
        Time = time;
        Type = type;
        ArrProducts = arrProducts;
    }

    public Document(int number, TypeDocument type, ArrivalProducts arrProducts) {
        Number = number;
        Type = type;
        ArrProducts = arrProducts;
        Date = LocalDate.now();
        Time = LocalTime.now();
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    public LocalDate getDate() {
        return Date;
    }

    public void setDate(LocalDate date) {
        Date = date;
    }

    public LocalTime getTime() {
        return Time;
    }

    public void setTime(LocalTime time) {
        Time = time;
    }

    public TypeDocument getType() {
        return Type;
    }

    public void setType(TypeDocument type) {
        Type = type;
    }

    public ArrivalProducts getArrProducts() {
        return ArrProducts;
    }

    public void setArrProducts(ArrivalProducts arrProducts) {
        ArrProducts = arrProducts;
    }
}
