package mephi.portal;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;

public class ArrivalProducts {
    private LegalEntity LegalEnt;
    private boolean IsComing;
    private String Department;
    private ArrayList<PriceList> priceLists;
    private HashMap<Product, Integer> Ordering;

    public ArrivalProducts(LegalEntity legalEnt, boolean isComing, String department) {
        LegalEnt = legalEnt;
        IsComing = isComing;
        Department = department;
        Ordering = new HashMap<>();
        priceLists = new ArrayList<>();
    }

    public LegalEntity getLegalEnt() {
        return LegalEnt;
    }

    public void setLegalEnt(LegalEntity legalEnt) {
        LegalEnt = legalEnt;
    }

    public boolean getIsComing() {
        return IsComing;
    }

    public void setIsComing(boolean isComing) {
        IsComing = isComing;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public HashMap<Product, Integer> getOrdering() {
        return Ordering;
    }

    public boolean AddCountForProduct(PriceList priceList, Product product, Integer count) {
        if (this.priceLists.contains(priceList) && priceList.getPrices().containsKey(product) && count > 0) {
            Ordering.put(product, count);
            return true;
        }
        return false;
    }

    public void addPriceLists(PriceList priceList) {
        if ((this.LegalEnt.getRoleLegalEnt() == RoleLegalEntity.CUSTOMER && priceList.getType() == TypePriceList.Wholesale) ||
                (this.LegalEnt.getRoleLegalEnt() == RoleLegalEntity.SUPPLIER && priceList.getType() == TypePriceList.Procurement))
            this.priceLists.add(priceList);
    }

    public ArrayList<PriceList> getPriceLists() {
        return priceLists;
    }
    public void deletePriceList(PriceList priceList){
        this.priceLists.remove(priceList);
    }

}