package mephi.portal;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Department {
    private String Name;
    private String Address;
    private HashMap<Product, Integer> Products;
    private ArrayList<Document> documents;
    private ProductCatalog ProductCatalog;

    class ProductCatalog {

        private final String Name;

        private ProductCatalog(String name) {
            Name = name;
        }

        public String getName() {
            return Name;
        }
    }

    public Department(String name, String address) {
        Name = name;
        Address = address;
        Products = new HashMap<>();
        documents = new ArrayList<>();
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public ProductCatalog getProductCatalog() {
        return ProductCatalog;
    }

    public void setProductCatalog(String name) {
        ProductCatalog = new ProductCatalog(name);
    }

    public boolean setProducts(ArrivalProducts goods) {
        int i=0;
        for(Document doc : documents){
            if(doc.getArrProducts().equals(goods)){
                break;
            }

            i++;
        }
        if (i == documents.size()){
            return false;
        }
        int k;
        if (goods.getIsComing()) {
            k = 1;
        }
        else {
            k = -1;
        }
        for (Map.Entry<Product, Integer> entry: goods.getOrdering().entrySet()) {
            boolean contains = Products.containsKey(entry.getKey());
            if (!contains && k == -1) {
                return false;
            }
            if (contains) {
                int newCount = Products.get(entry.getKey()) + k * entry.getValue();
                if (newCount < 0) {
                    return false;
                }
                else {
                    Products.put(entry.getKey(), newCount);
                }
            }
            else {
                Products.put(entry.getKey(), entry.getValue());
            }
        }
        return true;
    }

    public HashMap<Product, Integer> getProducts() {
        return this.Products;
    }

    public void addDocument(Document document){
        this.documents.add(document);
    }
    public ArrayList<Document> getDocuments(){
        return this.documents;
    }


}