package mephi.portal;

public enum TypePriceList {
    Retail,
    Wholesale,
    Procurement
}
