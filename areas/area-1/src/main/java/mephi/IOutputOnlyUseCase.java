package mephi;

public interface IOutputOnlyUseCase<TOutput> {

    TOutput Run();
}
