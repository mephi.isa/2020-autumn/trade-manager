package mephi.Dependencies;

public interface IRepositoryAutentification {
    int check(Integer id);
    int check(String token, Integer i) throws Exception;
}
