package mephi.Dependencies;

import mephi.portal.*;

import java.util.ArrayList;
import java.util.HashMap;

public interface IRepository {
    LegalEntity getLegalEntityById(Integer id) throws Exception;
    Integer addNewUser(LegalEntity legalEntity) throws Exception;
    Integer deleteSupplierAndPriceLists(String INN) throws Exception;
    ArrayList<PriceList> getPriceListsSupplierByINN(String INN) throws Exception;
    HashMap<String, ArrayList<PriceList>> getAllPriceListsSupplier() throws Exception;
    HashMap<String, ArrayList<PriceList>> getAllPriceListsCustomers() throws Exception;
    Integer updateLegalEntity(LegalEntity oldLegalEntity, LegalEntity newLegalEntity);

    ArrayList<LegalEntity> getAllSuppliers() throws Exception;

    ArrayList<LegalEntity> getAllCustomers() throws Exception;

    RoleLegalEntity getRoleLegalByINN(String inn) throws Exception;
    LegalEntity getLegalEntityByINN(String inn) throws Exception;

    Integer deleteCustomerAndPriceLists(String key) throws Exception;

    Integer updateLegalEntity(LegalEntity legEntityObject);

    ArrayList<ArrivalProducts> getAllArrivalProducts() throws Exception;

    ArrayList<Document> getAllDocuments() throws Exception;

    Integer deleteLegalEntity(LegalEntity legalEntityByINN) throws Exception;

    Integer deletePriceListByName(String key) throws Exception;
    Integer getAllPriceLists() throws Exception;

    PriceList getPriceListByName(String key) throws Exception;

    Integer addDepartment(Department department);

    Integer addNewPriceList(PriceList priceList);

    HashMap<Product, Integer> getProductsInWarehouse();

    String getProductNameByCode(int code);

    String getProductDescriptionByCode(int code);

    String getProductBarcodeByCode(int code);
}
